﻿using UnityEngine;
using System.Collections;

public class boomFollowPlayer : MonoBehaviour {
	GameObject Player;
	void Start()
	{
		Player = GameObject.FindGameObjectWithTag ("Player");
	}
	// Update is called once per frame
	void Update () {
		if (Player != null) {
			transform.position = (Player.transform.position + new Vector3 (-0.8f, -0.8f, 1));
			Destroy (gameObject, 0.1f);
		} else {
			Destroy (gameObject);
		}
	}
}
