﻿using UnityEngine;
using System.Collections;

public class EnBulletFly4a : MonoBehaviour {

	Transform _trans;
	Vector3 _StartPos;
	float moveSpeed;
	Vector3 moveDirection;
	//Object and parametars for global speed variables
	GameObject go;
	Parameters parameters;

	// Use this for initialization
	void Start () {
		//initialize and find Parameters script
		go = GameObject.Find ("EnemySpawner");
		parameters = go.GetComponent<Parameters> ();
		moveSpeed = parameters.enemyBulletSpeed;

		_trans = GetComponent<Transform> ();
		_StartPos = _trans.position;
	}

	// Update is called once per frame
	void Update () {
		_trans.position =new Vector3 (_StartPos.x += moveSpeed*Time.deltaTime*2, _StartPos.y -= moveSpeed*Time.deltaTime, _StartPos.z);

	}



	void OnTriggerEnter2D(Collider2D target){
		if(gameObject.CompareTag("EnemyBullet") && target.CompareTag("Player")|| gameObject.CompareTag("EnemyBullet") && target.CompareTag("Shield")){
			Destroy (gameObject);
		}
	}
}
