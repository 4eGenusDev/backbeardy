﻿using UnityEngine;
using System.Collections;

public class EnShoot4 : MonoBehaviour {

	public float moveSpeed=5f;
	Vector3 _desiredPos;
	Vector3 _transPos;
	//Object and parametars for global speed variables
	GameObject go;
	Parameters parameters;

	// Use this for initialization
	void Start () {
		//initialize and find Parameters script
		go = GameObject.Find ("EnemySpawner");
		parameters = go.GetComponent<Parameters> ();
	}

	void OnEnable()
	{
		Invoke("Shoot",1f);
	}

	// Update is called once per frame
	void Update () {
		//set the Shooting speed from global
		moveSpeed = parameters.enemyShootSpeed;
		//make corrections for starting point for bullet
		_transPos = transform.position;
		_desiredPos = new Vector3 (_transPos.x+1f, _transPos.y - 0.5f, _transPos.z);
	}



	void Shoot() {
		if (gameObject.active) {
			Instantiate (Resources.Load ("Enemy_Bullets/bullet_4", typeof(Object)), _desiredPos, Quaternion.identity);
			Instantiate (Resources.Load ("Enemy_Bullets/bullet_4a", typeof(Object)), _desiredPos, Quaternion.identity);
			GameObject boom = Instantiate (Resources.Load ("boom_enemy", typeof(Object)), _desiredPos, Quaternion.identity) as GameObject;
			Destroy (boom, 0.1f);
			Invoke ("Shoot", moveSpeed);
		}
	}
}
