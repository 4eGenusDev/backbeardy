﻿using UnityEngine;
using System.Collections;

public class BulletFly : MonoBehaviour {

	Transform _trans;
	Vector3 _StartPos;
	public float moveSpeed;
	Vector3 moveDirection;
	GameObject go;
	//PowerUpMove powerupmove;

	// Use this for initialization
	void Start () {
		//Get PowerUp parameters
//		go = GameObject.Find ("coinPowerup");
//		PowerUpMove powerupmove = go.GetComponent<PowerUpMove>();
//		//TODO if get powerUp get SpeedUp
		_trans = GetComponent<Transform> ();
		_StartPos = _trans.position;
		moveSpeed = 5f;
	}
	
	// Update is called once per frame
	void Update () {
		
		_trans.position =new Vector3 (_StartPos.x -= moveSpeed*Time.unscaledDeltaTime*2, _StartPos.y, _StartPos.z);

	}



	void OnTriggerEnter2D(Collider2D target){
		if(gameObject.CompareTag("PlayerBullet") && target.CompareTag("Enemy")){
			Destroy (gameObject);
	}
	}
}