﻿using UnityEngine;
using System.Collections;

public class PetsFollow : MonoBehaviour {
	//Movement variables
	Transform _trans;
	Vector3 _StartPos;
	float moveSpeed;
	int pearlNum;
	bool canCome;
	//You may consider adding a rigid body to the pet for accurate physics simulation
	private GameObject wayPoint;
	private Vector3 wayPointPos;


	//This will be the pet's speed. Adjust as necessary.
	private float speed = 10.0f;
	float randomPos;

	void Start ()
	{
		
		 randomPos = Random.Range (1, 7);
		//At the start of the game, the pets will find the gameobject called wayPoint.
		wayPoint = GameObject.FindWithTag("Player");

		//Movement and take speed from parameters class
		_trans = GetComponent<Transform> ();
		_StartPos = _trans.position;

		//Get move speed from Parameters class]

		moveSpeed = GameObject.Find ("EnemySpawner").GetComponent<Parameters> ().enemySpeed;
	}

	void Update ()
	{
		

		if (wayPoint!=null) {
			pearlNum = wayPoint.GetComponent<Player_Movement> ().pearlNum;
			canCome = wayPoint.GetComponent<Player_Movement> ().canCome;
		}
		//if the player is alive
		if (pearlNum>0&&wayPoint!=null) {
			//if u have a pearl be friendly
			float distance = Vector3.Distance (wayPoint.transform.position, transform.position);
			//if player comes near come to his side
			if (distance < 20&&gameObject.CompareTag("SeaHorse")) {
					wayPointPos = new Vector3 (wayPoint.transform.position.x - 2, wayPoint.transform.position.y + Mathf.PingPong (Time.time, randomPos), wayPoint.transform.position.z);
					//Here, the pet's will follow the player
					transform.position = Vector3.MoveTowards (transform.position, wayPointPos, speed * Time.deltaTime);
					//we flip the pet and allow him to shoot
					gameObject.GetComponent<SpriteRenderer> ().flipX = true;
					gameObject.GetComponent<PShoot> ().enabled = true;
			} 
			} 
		else {
				_trans.position = new Vector3 (_StartPos.x += moveSpeed * Time.deltaTime / 2, _StartPos.y, _StartPos.z);
			}
		}
	}


