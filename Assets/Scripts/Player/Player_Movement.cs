﻿using UnityEngine;
using System.Collections;

public class Player_Movement : MonoBehaviour {
	GameObject player;
	GameObject bullet;

	public GameObject pearl1, pearl2, pearl3;
	public int pearlNum= 0;
	//////////////////////////////////////////////// mobile move variables
	public float MoveSpeedBeardy;   //Movement reaction on touch
	public bool isDrunk = false;
	public bool isChrono = false;
	public bool isManiac = false;
	public bool canCome;

	//public bool isPearl = false;
	//Get component variables
	FrostEffect frostEffect;


	void Start(){
		MoveSpeedBeardy = PlayerPrefs.GetFloat ("moveSpeedBeardy");
		frostEffect = GameObject.Find ("Main Camera").GetComponent<FrostEffect> ();
		if(Application.loadedLevel == 2) {
		gameObject.GetComponent<Animator> ().SetBool ("isSea", true);
		}
		pearl1.SetActive (false);
		pearl2.SetActive (false);
		pearl3.SetActive (false);
		canCome = true;

	}

	// Update is called once per frame
	void Update () 
	{
		//if Beardy is not drunk move normal   =============MOVEMENT======================
		if (isDrunk == false) 
		{
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) 
			{
				// Get movement of the finger since last frame
				Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;

				// Move object across XY plane
				transform.Translate (touchDeltaPosition.x * (MoveSpeedBeardy * Time.unscaledDeltaTime), touchDeltaPosition.y * (MoveSpeedBeardy * Time.unscaledDeltaTime), 0);
			} 
		}

		//If Beardy drunk move confused 
		if (isDrunk == true) 
		{
			//initiate drunk timer
			Invoke ("timerDrunk", 10f);

	
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) 
			{
				// Get movement of the finger since last frame
				Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;

				// Move object across XY plane
				transform.Translate (-touchDeltaPosition.x * (MoveSpeedBeardy * Time.unscaledDeltaTime), -touchDeltaPosition.y * (MoveSpeedBeardy * Time.unscaledDeltaTime), 0);
			}
		}

			//Initiate the Maniac timer
		if (isManiac==true)
		{
			Invoke ("timerManiac", 10f);
			isManiac = false;
		}

		//Initiate the Chrono timer
		if(isChrono == true)
		{
			Invoke ("timerChrono", 10f);
			isChrono = false;
		}
	}

	void timerManiac()
	{
		Debug.Log ("maniac cancelling");
		frostEffect.enabled = false;
		GameObject.Find ("ManiacDeathField").GetComponent<BoxCollider2D> ().enabled = false;
		gameObject.GetComponent<Animator> ().SetBool ("isManiac", false);
	}

	void timerDrunk()
	{
		isDrunk = false;
		frostEffect.FrostAmount = 0.32f;
		frostEffect.seethroughness = 0.1f;
		frostEffect.distortion = 0.02f;
		frostEffect.enabled = false;
	
	}

	void timerChrono(){
		Time.timeScale = 1;
	}

	void OnTriggerEnter2D(Collider2D target)
	{
		//if pearl is taken set the pearl to show on beardy
		if (target.CompareTag("Pearl"))
			{
			pearlNum++;
			if (pearlNum >= 3) {
				pearlNum = 3;
			}
			switch (pearlNum) {
			case 1: 
				pearl1.SetActive (true);
				break;
			case 2:
				pearl2.SetActive (true);
				break;
			case 3: 			
				pearl3.SetActive (true);
				break;
			default:
				break;
			}
			Destroy (target.gameObject);

			}
	}
}