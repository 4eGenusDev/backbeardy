﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OnCollisionDie : MonoBehaviour {

	private Vector3 pos;
	private Vector3	behindRocks;
	GameObject go;
	Parameters parameters;
	GameObject Sphere;
	List <GameObject> enemiesPool;
	List <GameObject> explosionPool;

	//put current level in parameters script
	int currentLevel = 1;
	//TODO Load resources for exploding and coffins



	void Start(){
		
		go = GameObject.Find ("EnemySpawner");
		parameters = go.GetComponent<Parameters> ();
		Sphere = GameObject.Find ("Sphere");
		enemiesPool = go.GetComponent<SpawnEnemies> ().enemiesPool;
		explosionPool = go.GetComponent<SpawnEnemies> ().explosionsPool;

	}

	void Update(){
		pos = transform.position;
		behindRocks = transform.position + new Vector3 (0, 0, 4);
		
	}


	//Logic for on Collision die
	void OnTriggerEnter2D(Collider2D target){
		//If The enemy is hit by Player bullet or the ShockWave powerup
		if (gameObject.CompareTag ("Enemy") && target.CompareTag ("PlayerBullet") || (gameObject.CompareTag ("Enemy") && target.CompareTag ("Shockwave")) || (gameObject.CompareTag("Enemy") && target.CompareTag("Shield"))|| (gameObject.CompareTag("Enemy") && target.CompareTag("ManiacDeathField"))) {
			//Spawn feathers when killed
			if (gameObject.name == "Enemy_1" || gameObject.name == "Enemy_1(Clone)") {
				GameObject feather = (GameObject)Instantiate (Resources.Load ("Particles/Feathers_poof/poof_1", typeof(GameObject)), pos, Quaternion.identity);
				Destroy (feather, 3f);
			} else if (gameObject.name == "Enemy_2" || gameObject.name == "Enemy_2(Clone)") {
				GameObject feather2 = (GameObject)Instantiate (Resources.Load ("Particles/Feathers_poof/poof_2", typeof(GameObject)), pos, Quaternion.identity);
				Destroy (feather2, 3f);
			} else if (gameObject.name == "Enemy_3" || gameObject.name == "Enemy_3(Clone)") {
				GameObject feather3 = (GameObject)Instantiate (Resources.Load ("Particles/Feathers_poof/poof_3", typeof(GameObject)), pos, Quaternion.identity);
				Destroy (feather3, 3f);
			} else if (gameObject.name == "Enemy_4" || gameObject.name == "Enemy_4(Clone)") {
				GameObject feather4 = (GameObject)Instantiate (Resources.Load ("Particles/Feathers_poof/poof_4", typeof(GameObject)), pos, Quaternion.identity);
				Destroy (feather4, 3f);
			}
			//if shockwave is triggered destroy it 
			if (target.CompareTag ("Shockwave")) {
				Destroy (target, 1f);
			}
			//Count How many enemies are destroyed, then destroy enemy
			if(parameters){ 
			parameters.Enemies += 1; //Death Field error, unknown
			}
			//disable gameobject and return it to pool
			gameObject.SetActive (false);
			//random for the coffin different spawn
			int rand = Random.Range (1, 4);

			explosionPool [0].transform.position = gameObject.transform.position;
			explosionPool [0].GetComponent<ParticleSystem> ().Play ();
			explosionPool [0].SetActive (true);
			//TODO explosion pooling


			//GameObject go = (GameObject)Instantiate (Resources.Load ("Particles/Toon_Explode/Explode_1", typeof(GameObject)), pos, Quaternion.identity);
			GameObject go1 = (GameObject)Instantiate (Resources.Load ("Particles/Coffin/coffin_" + rand, typeof(GameObject)), behindRocks, Quaternion.identity);

			//Destroy (go, 1f);
			Destroy (go1, 3f);
		} 
		//If the Player is hit in an enemy or is hit by enemy bullet,trap,seashell
		else if (gameObject.CompareTag ("Player") && target.CompareTag ("Enemy") || gameObject.CompareTag ("Player") && target.CompareTag ("EnemyBullet") || gameObject.CompareTag ("Player") && target.CompareTag ("Trap") || gameObject.CompareTag ("Player") && target.CompareTag ("SeaShell")|| gameObject.CompareTag ("Player") && target.CompareTag ("SeaSpikes")) {
			Destroy (gameObject);
			GameObject go = (GameObject)Instantiate (Resources.Load ("Particles/Toon_Explode/Explode_1", typeof(GameObject)), pos, Quaternion.identity);
			Destroy (go, 1f);
		} else if (gameObject.CompareTag ("Shield") && target.CompareTag ("Enemy") || gameObject.CompareTag ("Shield") && target.CompareTag ("EnemyBullet")) {
			Destroy (target);
			Sphere.GetComponent<MeshRenderer> ().enabled = false;
			Sphere.GetComponent<CircleCollider2D> ().enabled = false;

		}

		else if (gameObject.CompareTag("Player") && target.CompareTag("House")){
			currentLevel +=1;
		}


}
	//IF Player is hit by SeaSpikes from SeaSpikies
	void OnParticleCollision(GameObject target)
	{
		if (gameObject.CompareTag ("Player") && target.CompareTag ("SeaSpikes")) {
			Destroy (gameObject);
			GameObject go = (GameObject)Instantiate (Resources.Load ("Particles/Toon_Explode/Explode_1", typeof(GameObject)), pos, Quaternion.identity);
			Destroy (go, 1f);
		}
	}
}
