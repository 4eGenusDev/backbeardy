﻿using UnityEngine;
using System.Collections;

public class PShoot : MonoBehaviour {

	[SerializeField]
    float moveSpeed;
	//Get the Parameters globals
	GameObject go;
	Parameters parameters;
	int pearlNum;
	//// /////////////////////////

	// Use this for initialization
	void Start () {
		//find object which contains the Parameters script
		go = GameObject.Find ("EnemySpawner");
		parameters = go.GetComponent<Parameters> ();
		if (gameObject.CompareTag ("SeaHorse")) {
			pearlNum = GameObject.Find ("Beardy").GetComponent<Player_Movement> ().pearlNum;
		}
		//find object which contains the PowerUp trigger
		Invoke ("Shoot", 1f);
	}
	
	// Update is called once per frame

	void Update () {
		moveSpeed = parameters.playerShootSpeed;
	}




	 void Shoot(){
		
		if (gameObject.CompareTag ("SeaHorse")) {
			if (pearlNum == 1) {
				Instantiate (Resources.Load ("Enemy_Bullets/sniperBullet", typeof(Object)), transform.position + new Vector3 (-0.8f, 1, 1), Quaternion.identity);			}
			else if (pearlNum == 2) {
				Instantiate (Resources.Load ("Enemy_Bullets/sniperBullet", typeof(Object)), transform.position + new Vector3 (-0.8f, -2, 1), Quaternion.identity);
				Instantiate (Resources.Load ("Enemy_Bullets/sniperBullet", typeof(Object)), transform.position + new Vector3 (-0.8f, 3f, 1), Quaternion.identity);
			}
			else if (pearlNum == 3) {
				Instantiate (Resources.Load ("Enemy_Bullets/sniperBullet", typeof(Object)), transform.position + new Vector3 (-0.8f, -2, 1), Quaternion.identity);
				Instantiate (Resources.Load ("Enemy_Bullets/sniperBullet", typeof(Object)), transform.position + new Vector3 (-0.8f, 1f, 1), Quaternion.identity);
				Instantiate (Resources.Load ("Enemy_Bullets/sniperBullet", typeof(Object)), transform.position + new Vector3 (-0.8f, 3f, 1), Quaternion.identity);

			}
			} else {
			//made to shoot the same if the time is slowed down
			Instantiate (Resources.Load ("Enemy_Bullets/sniperBullet", typeof(Object)), transform.position + new Vector3 (-0.8f, -0.8f, 1), Quaternion.identity);
			Instantiate (Resources.Load ("boom", typeof(Object)), transform.position, Quaternion.identity);
		}
		Invoke ("Shoot", moveSpeed * Time.timeScale);
	}
}