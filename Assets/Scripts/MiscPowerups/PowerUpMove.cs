﻿using UnityEngine;
using System.Collections;

public class PowerUpMove : MonoBehaviour {

	Vector3 _startingPos;
	Transform _trans;
	float moveSpeed;
	float randomPos;
	Vector3 myScale;
	Vector3 centerOfScreen;

	GameObject go;
	Parameters parameters;


	// Use this for initialization
	void Start () {
		_trans = GetComponent<Transform>();
		_startingPos = _trans.position;
		//Get parameters from Global parameters
		go = GameObject.Find ("EnemySpawner");
		parameters = go.GetComponent<Parameters> ();
		randomPos = Random.Range (1f, 4f);
		moveSpeed = 3f;
		centerOfScreen = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height/2,10));
	}

	// Update is called once per frame
	void Update () {
		//Move
		_trans.position = new Vector3(_startingPos.x+=moveSpeed*Time.deltaTime*2, _startingPos.y + Mathf.PingPong(Time.time, randomPos), _startingPos.z);
		myScale.x = 7f;


	}




	void OnTriggerEnter2D(Collider2D target){
		//On collision with player destroy and "give" powerup
		if (target.CompareTag ("Player") && gameObject.name == "ShockwaveUp(Clone)" && !target.CompareTag ("PlayerBullet")) 
		{
			Destroy (gameObject);
			GameObject go = (GameObject)Instantiate (Resources.Load ("Powerups/Shockwave/Shockwave", typeof(GameObject)), _trans.position, Quaternion.identity);
			Destroy (go, 1f);
		} 
		else if ((target.CompareTag ("Player") && gameObject.name == "Flurry(Clone)" || gameObject.name == "Flurry") && !target.CompareTag ("PlayerBullet")) 
		{
			Destroy (gameObject);
			GameObject go = (GameObject)Instantiate (Resources.Load ("Powerups/BulletStorm", typeof(GameObject)), centerOfScreen, Quaternion.identity);
			Destroy (go, 1f);
			parameters.playerShootSpeed = 1;
		} 
		else if ((target.CompareTag ("Player") && gameObject.name == "Shield(Clone)" || gameObject.name == "Shield") && !target.CompareTag ("PlayerBullet")) 
		{
			GameObject.Find ("Sphere").GetComponent<MeshRenderer> ().enabled = true;
			GameObject.Find ("Sphere").GetComponent<CircleCollider2D> ().enabled = true;
			Destroy (gameObject);
			GameObject go = (GameObject)Instantiate (Resources.Load ("Powerups/ForceShield", typeof(GameObject)), centerOfScreen, Quaternion.identity);
			Destroy (go, 1f);
		} 
		else if ((target.CompareTag ("Player") && gameObject.name == "coinManiac(Clone)" || gameObject.name == "coinManiac") && !target.CompareTag ("PlayerBullet")) 
		{
			
			GameObject go = (GameObject)Instantiate (Resources.Load ("Powerups/Maniac", typeof(GameObject)), centerOfScreen, Quaternion.identity);
			Destroy (go, 1f);
			GameObject.Find ("Beardy").GetComponent<Player_Movement> ().isManiac = true;
			GameObject.Find ("Beardy").GetComponent<Animator> ().SetBool ("isManiac", true);
			GameObject.Find ("Main Camera").GetComponent<FrostEffect> ().enabled = true;
			GameObject.Find ("ManiacDeathField").GetComponent<BoxCollider2D> ().enabled = true;
			Destroy (gameObject);
			//Maniac effect is cancelled in Player_Movement script because this is destroyed
		} 
		else if ((target.CompareTag ("Player") && gameObject.name == "Bottle(Clone)" || gameObject.name == "Bottle") && !target.CompareTag ("PlayerBullet")) 
		{
			Destroy (gameObject);
			GameObject go = (GameObject)Instantiate (Resources.Load ("Powerups/bottleDrunk", typeof(GameObject)), centerOfScreen, Quaternion.identity);
			Destroy (go, 1f);
			GameObject.Find ("Main Camera").GetComponent<FrostEffect> ().FrostAmount = 0.5f;
			GameObject.Find ("Main Camera").GetComponent<FrostEffect> ().seethroughness = 0.7f;
			GameObject.Find ("Main Camera").GetComponent<FrostEffect> ().distortion = 0.08f;
			GameObject.Find ("Main Camera").GetComponent<FrostEffect> ().enabled = true;
			GameObject.Find ("Beardy").GetComponent<Player_Movement> ().isDrunk = true;
		} 
		else if ((target.CompareTag ("Player") && gameObject.name == "coinChrono(Clone)" || gameObject.name == "coinChrono") && !target.CompareTag ("PlayerBullet")) 
		{
			GameObject go = (GameObject)Instantiate (Resources.Load ("Powerups/ChronoPowerUp", typeof(GameObject)), centerOfScreen, Quaternion.identity);
			Destroy (go, 0.5f);
			GameObject.Find ("Beardy").GetComponent<Player_Movement> ().isChrono = true;
			Destroy (gameObject);
			Time.timeScale = 0.5f;
		}

}
}