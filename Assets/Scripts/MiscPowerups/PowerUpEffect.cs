﻿using UnityEngine;
using System.Collections;

public class PowerUpEffect : MonoBehaviour {
	SpriteRenderer changeEffect;
	Color FadeColor;
	GameObject Shine;
	// Use this for initialization
	void Start () {
		Shine = GameObject.Find ("Shine");
		changeEffect = Shine.GetComponent<SpriteRenderer> ();
	    FadeColor = new Color (1, 1, 1, 0);
		gameObject.layer = 3;

	}
	
	// Update is called once per frame
	void Update () {
		
		//Invoke ("changeColor", 2f);
		Color lerpedColor = Color.Lerp (Color.white, FadeColor, Mathf.PingPong(Time.time, 0.5f));
		changeEffect.color = lerpedColor;
		if (gameObject.name == "bottleDrunk" || gameObject.name == "bottleDrunk(Clone)") {
			transform.localScale += new Vector3 (0.03f, 0.03f, 0);
			Destroy (gameObject, 0.8f);
		} 
		else {
			transform.localScale += new Vector3 (0.1f, 0.1f, 0);
			Destroy (gameObject, 0.8f);
		}
	}
		
}
