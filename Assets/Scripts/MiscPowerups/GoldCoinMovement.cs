﻿using UnityEngine;
using System.Collections;

public class GoldCoinMovement : MonoBehaviour {

	Vector3 _startingPos;
	Transform _trans;
	float moveSpeed;
	GameObject go;
	Parameters parameters;
	GameObject sparkles;

	// Use this for initialization
	void Start () {
		//find Parameters for coins count
		go = GameObject.Find ("EnemySpawner");
		parameters = go.GetComponent<Parameters> ();
		//Load the sparkles from the coin
		sparkles = Resources.Load("Powerups/Sparkles") as GameObject;
		_trans = GetComponent<Transform>();
		_startingPos = _trans.position;
		moveSpeed = 4f;
	}
	
	// Update is called once per frame
	void Update () {
		//Move


		_trans.position = new Vector3(_startingPos.x+=moveSpeed*Time.deltaTime/2, _startingPos.y + Mathf.PingPong(Time.time, 2), _startingPos.z);
	}

	void OnTriggerEnter2D(Collider2D target){
		if(target.CompareTag("Player")){
			GameObject newSparkles = (GameObject) Instantiate (sparkles, _trans.position, Quaternion.identity);
			parameters.Coins += 1;
			Destroy (gameObject, 0.1f);
			Destroy (newSparkles, 1f);
	

		}
	}
}
