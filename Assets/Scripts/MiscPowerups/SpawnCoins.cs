﻿using UnityEngine;
using System.Collections;

public class SpawnCoins : MonoBehaviour {

	BoxCollider2D myBox;
	Vector3 pos;
	int randPowerup;
	float randTime;
	float x1;
	float x2;

	// Use this for initialization
	void Start () {
		myBox = GetComponent<BoxCollider2D> ();
		x1 = myBox.transform.position.y - myBox.bounds.size.y / 2;
		x2 = myBox.transform.position.y + myBox.bounds.size.y / 2;
		Invoke ("SpawnThem", Random.Range(1f,2f));
		Invoke ("SpawnPowerup", Random.Range(10f,20f));
	}
	
	// Update is called once per frame
	void Update () {

	}

	void SpawnThem(){

	pos = new Vector2 (transform.position.x,Random.Range (x1, x2));
	//random time between invoking SpawnThem
	randTime = Random.Range (2.5f, 5.5f);
	//which powerup is chosen
	randPowerup = Random.Range (1, 7);
	Instantiate (Resources.Load ("Powerups/coinGold", typeof(GameObject)), pos, Quaternion.identity);
	Invoke ("SpawnThem",randTime);
	
}
	void SpawnPowerup(){
		switch (randPowerup) {
		case 1:
			{
				Invoke ("SpawnShockwave",1f);
				break;
			}
		case 2:
			{
				Invoke ("SpawnFlurry",1f);
				break;
			}
		case 3:
			{
				Invoke ("SpawnManiac", 1f);
				break;
			}
		case 4:
			{
				Invoke ("SpawnBottle", 1f);
				break;
			}
		case 5:
			{
				Invoke ("SpawnShield", 1f);
				break;
			}
		case 6:
			{
				Invoke ("SpawnChrono", 1f);
				break;
			}

		default:
			break;
		}
		Invoke ("SpawnPowerup", randTime *7);
	}
	void SpawnShockwave(){
		Instantiate (Resources.Load ("Powerups/ShockwaveUp", typeof(GameObject)), pos, Quaternion.identity);

	}
	void SpawnFlurry(){
		Instantiate (Resources.Load ("Powerups/Flurry", typeof(GameObject)), pos, Quaternion.identity);

	}
	void SpawnManiac(){
		Instantiate (Resources.Load ("Powerups/coinManiac", typeof(GameObject)), pos, Quaternion.identity);
	}
	void SpawnBottle(){
		Instantiate (Resources.Load ("Powerups/Bottle", typeof(GameObject)), pos, Quaternion.identity);
	}
	void SpawnShield(){
		Instantiate (Resources.Load ("Powerups/Shield", typeof(GameObject)), pos, Quaternion.identity);
	}

	void SpawnChrono()
	{
		 Instantiate (Resources.Load ("Powerups/coinChrono", typeof(GameObject)), pos, Quaternion.identity);

	}
}
