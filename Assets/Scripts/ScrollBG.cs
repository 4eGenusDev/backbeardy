﻿using UnityEngine;
using System.Collections;

public class ScrollBG : MonoBehaviour {

	void Update () {
		MeshRenderer mr = GetComponent<MeshRenderer>();
		Material mat = mr.material;
		Vector2 offset = mat.mainTextureOffset;
		offset.x += Time.deltaTime / 120f;
		mat.mainTextureOffset = offset;
	}
}






