﻿using UnityEngine;
using System.Collections;

public class yyo : MonoBehaviour {

	float lerpTime = 2f;
	float currentLerpTime;

	float moveDistance = 15f;

	Vector3 startPos;
	Vector3 endPos;
	Vector3 endPosOut;
	Animator myAni;
	// // // //
	public Transform tran1;
	public Transform tran2;
	float yoyo;
	float you;
	float perc;
	bool onlyOnceCheckpoint = true;
	AudioSource playSound;

	// Get the Parameters script//
	GameObject go;
	Parameters parameters;
	GameObject house;

	protected void Start() {
		house = Resources.Load ("HouseEmpty") as GameObject;
		playSound = gameObject.GetComponent<AudioSource> ();
		startPos = transform.position;
		endPos = transform.position + (transform.right) * moveDistance;
		endPosOut = endPos + (transform.up) * (moveDistance+1);
		myAni = GetComponent<Animator> ();
		//Parameters for checkpoint
		go = GameObject.Find("EnemySpawner");
		parameters = go.GetComponent<Parameters> ();
	}

     protected void Update() {
		//How much does the checkpoint last
		Invoke ("MoveToSight", 60f);

	}

		void OnTriggerEnter2D(Collider2D trigger){
		if (gameObject.CompareTag ("House") && trigger.CompareTag ("Player")) {
			myAni.SetBool ("isInside", true);
			playSound.Play ();
			if (onlyOnceCheckpoint) {
				//Set checkpoint +1
			    parameters.currentCheckPoint += 1;
				Instantiate (house, startPos, Quaternion.identity);
				onlyOnceCheckpoint = false;
			}
		}
	}

	void MoveToSight(){
		currentLerpTime += Time.deltaTime;
		if (currentLerpTime > lerpTime) {
			currentLerpTime = lerpTime;
		}

		//lerp!
		perc = currentLerpTime / lerpTime;
		if (myAni.GetBool ("isInside") == false) { 
			transform.position = Vector3.Lerp (startPos, endPos, perc);
		} else {
			Invoke ("MoveOutOfSight", 2f);
		}
	}
	void MoveOutOfSight(){
		you += Time.deltaTime/20;
		if (you > lerpTime) {
			you = lerpTime;
		}

		transform.position = Vector3.Lerp (transform.position,endPosOut,you);
		Destroy (gameObject,2f);

	}
}