﻿using UnityEngine;
using System.Collections;

public class DestroyOutOFBounds : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		Invoke ("DieOutOfBounds", 3f);

	}


	void DieOutOfBounds(){
		// Die by being off screen
		Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		if (gameObject.CompareTag("SeaHorse"))
		{
			if (screenPosition.x > Screen.width+10 || screenPosition.x <-10)
			{
				Destroy (gameObject);
			}
		}
		else if (screenPosition.y > Screen.height+10 || screenPosition.y < -10 || screenPosition.x > Screen.width+10 || screenPosition.x <-10)
		{
			Destroy (gameObject);
		}
	}
}
