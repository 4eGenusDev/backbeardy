﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {

	Image meshR;
	float lerp;
	bool isLerped;
	GameObject[] ButtonsFade;
	CanvasRenderer[] allRend;
	// Update is called once per frame
	void Start ()
	{ 
	
		ButtonsFade = GameObject.FindGameObjectsWithTag ("FadeButton");
		 allRend = new CanvasRenderer[ButtonsFade.Length];

		isLerped = false;
		meshR = gameObject.GetComponent<Image> ();
		for (int i=0;i<ButtonsFade.Length;i++) 
		{
			allRend [i] = ButtonsFade [i].GetComponent<CanvasRenderer> ();
		}
		foreach (CanvasRenderer can in allRend) 
		{
			can.SetAlpha (0);
		}
			
	}


	void Update () {
		if (Application.isShowingSplashScreen !=true){
		Invoke ("doBlend", 0.2f);
			if (isLerped == true) {
				CancelInvoke ();
			}
		}

}

	void doBlend()
	{
		//do a lerp between the two materials for background effect
		if (lerp < 1&&isLerped==false) {
			lerp += 1*Time.deltaTime;
			meshR.material.SetFloat ("_Blend", lerp);
			foreach (CanvasRenderer can in allRend) 
			{
				can.SetAlpha (lerp);
			}
		} 
		else 
		{
			isLerped = true;
		}
	}
}