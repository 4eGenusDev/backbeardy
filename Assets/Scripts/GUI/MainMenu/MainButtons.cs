﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainButtons : MonoBehaviour {

	//buttons in main menu
	public GameObject Play, Settings, Store, Share, Back;
	public AudioSource audioPlay;
	AudioClip buttonPlay;
	AudioClip buttonCancel;
	//Text for settins ON/OFF
	public Text soundText;
	public Text notificationsText;
	//animations for share menu slide
    public Animator shareAni;
	//animations for main menu buttons
	public Animator mainAni;
	//in what menu are you currently
	int menuCounter = 1;
	//if the player plays first time
	int hasPlayed;
	//Slider variables
	public float sensitivity;
	public Slider Slider;
	float sliderValue;
	//Toggels for buttons
	public Toggle toggleSounds;


	void Start () {
		hasPlayed = PlayerPrefs.GetInt( "HasPlayed");
		if (hasPlayed == 0) {
			PlayerPrefs.SetInt ("HasPlayed", 1);
			PlayerPrefs.SetFloat ("moveSpeedBeardy", 3.5f);
			PlayerPrefs.SetInt ("sound", 1);
		} 
		// // // // // SET SOUND ON/OFF // // // // // // 
		//Set the player sound preferences on start
		if (PlayerPrefs.GetInt ("sound") == 1) {
			AudioListener.pause = false;
			AudioListener.volume = 1;
			toggleSounds.isOn = true;
			soundText.text = "    ENABLE SOUND:   ON";
		} 
		else 
		{
			AudioListener.pause = true;
			AudioListener.volume = 0;
			toggleSounds.isOn = false;
			soundText.text = "    ENABLE SOUND:   OFF";
		}
		// // // // // SET SOUND ON/OFF // // // // // // // // // // // // // // // /  

		//set the sensitivity slider on player preferences
		Slider.value = PlayerPrefs.GetFloat ("moveSpeedBeardy");

		//get animation component for the Share slide menu
		shareAni = Share.GetComponent<Animator> ();
		//get audio component for the background song
		audioPlay = gameObject.GetComponent<AudioSource> ();
	//Load Audio Clips
		buttonPlay = Resources.Load ("Sounds/MainMenu/playButton") as AudioClip;
		buttonCancel = Resources.Load ("Sounds/MainMenu/cancelButton") as AudioClip;
	}
		
	//play game button
	public void OnPlay()
	{
		audioPlay.clip = buttonPlay;
		audioPlay.Play ();
	    SceneManager.LoadSceneAsync ("Game");
	}
	//play share slide animation forth and back
	public void OnShare()
	{
		
		if (shareAni.GetBool ("slideIn") == true) {
			shareAni.SetBool ("slideIn", false);
		} else {
			shareAni.SetBool ("slideIn", true);
		}
	}
	//when click on settings, open settings menu
	public void onSettings()
	{
		mainAni.SetBool ("isFadeOut", true);
		menuCounter++;

	}
	//set back button
	public void onBack()
	{
		if (menuCounter > 1) {
			mainAni.SetBool ("isFadeOut", false);
			PlayerPrefs.Save ();
		}
	}
	//adjust the sensitivity slider
	public void AdjustSpeed(Slider slider )
	{
		sensitivity = slider.value;
		PlayerPrefs.SetFloat ("moveSpeedBeardy",sensitivity);

	}
	//toggle sound on/off
	public void toggleSound(Toggle toggleSounds)
	{
		if (toggleSounds.isOn) {
			AudioListener.pause = false;
			AudioListener.volume = 1;
			soundText.text = "    ENABLE SOUND:   ON";
			PlayerPrefs.SetInt ("sound", 1);
		} 
		else 
		{
			AudioListener.pause = true;
			AudioListener.volume = 0;
			soundText.text = "    ENABLE SOUND:   OFF";
			PlayerPrefs.SetInt ("sound", 0);
		}

			
	}

		

}
