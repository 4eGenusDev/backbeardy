﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class PauseButton : MonoBehaviour {

	public bool paused;
	public GameObject pauseButton, pausePanel, exitButton, tryAgainButton, darkGO, scoreBoard;
	GUITexture darkenScreen;
	[SerializeField]
	AudioClip pauseSound;
	AudioSource playSound;

	// Use this for initialization
	void Start () {
		darkenScreen = darkGO.GetComponent<GUITexture> ();
		playSound = GameObject.Find ("PauseButton").GetComponent<AudioSource> ();

		paused = false;
		pausePanel.SetActive (false);
		exitButton.SetActive (false);
		tryAgainButton.SetActive (false);
	}

	public void OnPause() {
			Time.timeScale = 0;
			paused = true;
		pausePanel.SetActive (true);
		exitButton.SetActive (true); 
		tryAgainButton.SetActive (true);
		darkenScreen.enabled = true;
		scoreBoard.SetActive (false);
		playSound.clip = pauseSound;
		playSound.Play ();
		}

	public void OnResume() {
		Time.timeScale = 1;
		paused = false;
		pausePanel.SetActive (false);
		exitButton.SetActive (false);
		tryAgainButton.SetActive (false);
		darkenScreen.enabled = false;
		scoreBoard.SetActive (true);
	}

	public void TryAgain(){
		SceneManager.LoadScene (Application.loadedLevel);
		Time.timeScale = 1;
	}

	public void GoToMenu()
	{
		SceneManager.LoadScene (Application.loadedLevel + 1);
		Time.timeScale = 1;
	}


}
