﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Parameters : MonoBehaviour {

	GameObject GoCoins;
	GameObject GoEnemies;
	public Text scoreText;
	public Text enemiesText;

	//Bullets
	public float enemyBulletSpeed;
	public float PlayerBulletSpeed;
	//Speed
	public float enemySpeed;
	public float playerSpeed;
	//Shooting intervals
	public float playerShootSpeed;
	public float enemyShootSpeed;
	//Spawning Times
	//Coins,Powerups
	public float goldCoinSpeed;
	//SCOREBOARD // // // // // // / / / / / / / / / / / / / / /
	public int Coins;
	public int Enemies;
	//CHECKPOINT COUNT// // // / / / / / / / / / / / / / / / / /
	public int currentCheckPoint;
	//SPAWN COEFICIENT // / /  / / / / /  // / /  //  // /  // /
	public float spawnCoeficient;

	// Use this for initialization
	void Start () {
		GoCoins = GameObject.Find ("Coins");
		scoreText = GoCoins.GetComponent<Text> ();
		GoEnemies = GameObject.Find ("Enemies");
		enemiesText = GoEnemies.GetComponent<Text> ();

		//Shooting intervals
		playerShootSpeed = 2f;
		enemyShootSpeed = 2f;
		//Bullets
		enemyBulletSpeed=3f;
		PlayerBulletSpeed = 2f;
		//Speed
		playerSpeed=5f;
		enemySpeed = 5f;
		//Coins,Powerups
		goldCoinSpeed = 5f;
		// / / / / / // / / / / / / / / / / / / / / / /
		//Coins read
		Coins = 0;
		Enemies = 0;
		// Checkpoint read
		currentCheckPoint = 0;
		//Spawn read
		spawnCoeficient = 1f;
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = Coins.ToString ()+ " C" +currentCheckPoint;
		enemiesText.text = Enemies.ToString ();

		switch (currentCheckPoint) {
		case 1:
			{
				enemyBulletSpeed = 4f;
				enemySpeed = 6f;
				enemyShootSpeed = 2.7f;
				spawnCoeficient = 0.8f;
				break;
			}
		case 2:
			{
				enemyBulletSpeed = 5f;
				enemySpeed = 7f;
				enemyShootSpeed = 2.2f;
				spawnCoeficient = 0.7f;
				break;
			}
		case 3:
			{
				enemyBulletSpeed = 6f;
				enemySpeed = 8f;
				enemyShootSpeed = 1.7f;
				spawnCoeficient = 0.6f;
				break;
			}
		case 4:
			{
				enemyBulletSpeed = 7f;
				enemySpeed = 8.5f;
				enemyShootSpeed = 1.3f;
				spawnCoeficient = 0.5f;
				break;
			}
		case 5:
			{
				enemyBulletSpeed = 7.5f;
				enemySpeed = 9f;
				enemyShootSpeed = 1.1f;
				spawnCoeficient = 0.4f;
				Invoke ("goToNextScene", 3f);
				break;

			}
		default:
			break;
		}
		}

	void goToNextScene()
	{
		SceneManager.LoadScene (Application.loadedLevel + 1);
	}
	}
