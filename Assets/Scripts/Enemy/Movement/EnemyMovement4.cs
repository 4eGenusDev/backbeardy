﻿using UnityEngine;
using System.Collections;

public class EnemyMovement4 : MonoBehaviour {

	float moveSpeed;
	Transform _trans;
	Vector3 StartingPos;
	float randomPos;

	// Use this for initialization
	void Start () {
		_trans = GetComponent<Transform>();
		StartingPos = transform.position;
		randomPos = Random.Range (1, 7);

		//Get move speed from Parameters class]
		GameObject go = GameObject.Find ("EnemySpawner");
		Parameters parameters = go.GetComponent<Parameters> ();
		moveSpeed = parameters.enemySpeed;

	}
	
	// Update is called once per frame
	void Update () {
		_trans.position = new Vector3(StartingPos.x += moveSpeed * Time.deltaTime/2,StartingPos.y + Mathf.PingPong(Time.time, randomPos),StartingPos.z);
	}
}
