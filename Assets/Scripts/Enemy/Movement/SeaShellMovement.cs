﻿using UnityEngine;
using System.Collections;

public class SeaShellMovement : MonoBehaviour {

	Transform _trans;
	Vector3 _StartPos;
	float moveSpeed;
	SpriteRenderer spriteName;
	CircleCollider2D collider;
	Animator myAni;
	// Use this for initialization
	void Start () {
		
		myAni = GetComponent<Animator> ();
		//find the pearl as child object
		_trans = GetComponent<Transform> ();
		_StartPos = _trans.position;

		//Get move speed from Parameters class]
		moveSpeed = GameObject.Find ("EnemySpawner").GetComponent<Parameters> ().enemySpeed;
		//Get the name and collider of SeaShell
		spriteName= gameObject.GetComponent<SpriteRenderer> ();
		collider = gameObject.GetComponent<CircleCollider2D> ();

	}

	// Update is called once per frame
	void Update () {
		//shell movement
		_trans.position = new Vector3 (_StartPos.x += moveSpeed * Time.deltaTime/8, _StartPos.y, _StartPos.z);

		//Open and close case of shell, enable and disable collider based on animation changes
		if (spriteName.sprite.name == "gadgets-05") 
		{
			collider.enabled = true;
		}
		else {
			collider.enabled = false;
		}
	}

}

