﻿using UnityEngine;
using System.Collections;

public class KeepEnemyBounded : MonoBehaviour {

	Vector3 bottomLeft;
	Vector3 topRight;
	// Use this for initialization
	void Start () {

		bottomLeft = Camera.main.ScreenToWorldPoint(Vector3.zero);
		topRight = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight));
	}

	// Update is called once per frame
	void Update () {


		Rect cameraRect = new Rect(
			bottomLeft.x,
			bottomLeft.y,
			topRight.x - bottomLeft.x,
			topRight.y - bottomLeft.y);

		transform.position = new Vector3(
			Mathf.Clamp(transform.position.x, cameraRect.xMin, cameraRect .xMax),
			Mathf.Clamp(transform.position.y, cameraRect.yMin, cameraRect .yMax),
			transform.position.z);
	}
}
