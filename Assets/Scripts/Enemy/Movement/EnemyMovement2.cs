﻿using UnityEngine;
using System.Collections;

public class EnemyMovement2 : MonoBehaviour {

	Transform _trans;
	Vector3 _StartPos;
	float moveSpeed;

	// Use this for initialization
	void Start () {
	
		_trans = GetComponent<Transform> ();
		_StartPos = _trans.position;

		//Get move speed from Parameters class]
		GameObject go = GameObject.Find ("EnemySpawner");
		Parameters parameters = go.GetComponent<Parameters> ();
		moveSpeed = parameters.enemySpeed;
	}
	
	// Update is called once per frame
	void Update () {
	
		_trans.position = new Vector3 (_StartPos.x += moveSpeed * Time.deltaTime/2, _StartPos.y, _StartPos.z);
	}
}
