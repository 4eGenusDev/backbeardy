﻿using UnityEngine;
using System.Collections;

public class EnemyMovement5a : MonoBehaviour {

	Transform _trans;
	Vector3 _StartPos;
	float moveSpeed;
	float randMove;

	void Start () {
		_trans = GetComponent<Transform> ();
		_StartPos = _trans.position;
		randMove = Random.Range (0.5f, 3);

		//Get move speed from Parameters class]
		GameObject go = GameObject.Find ("EnemySpawner");
		Parameters parameters = go.GetComponent<Parameters> ();
		moveSpeed = parameters.enemySpeed;
	}
	
	// Update is called once per frame
	void Update () {
		_trans.position = new Vector3 (_StartPos.x += moveSpeed * randMove * Time.deltaTime/2, _StartPos.y += moveSpeed * Time.deltaTime/2, _StartPos.z);
	}
}
