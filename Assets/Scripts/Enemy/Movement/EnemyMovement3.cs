﻿using UnityEngine;
using System.Collections;


public class EnemyMovement3 : MonoBehaviour
{
 
	Transform _trans;
	Vector3 _StartPos;
	float moveSpeed;
	private GameObject player;
	Animator ani;
	public Object go;
	bool explodeOnce = true;

	// Use this for initialization
	void Start ()
	{
		
		_trans = GetComponent<Transform> ();
		_StartPos = _trans.position;

		//set speed for enemy animation
		player = GameObject.FindGameObjectWithTag ("Player");
		ani = GetComponent<Animator> ();
		ani.speed = 0.3f;

		//Get move speed from Parameters class
		GameObject go = GameObject.Find ("EnemySpawner");
		Parameters parameters = go.GetComponent<Parameters> ();
		moveSpeed = parameters.enemySpeed;
	}
	
	// Update is called once per frame
	void Update ()
	{
		//if player is dead just move forward
		if (!player) {
			_StartPos = transform.position;
			_trans.position = new Vector3 (_StartPos.x += moveSpeed * Time.deltaTime * 1.5f, _StartPos.y, _StartPos.z);
		} else {

			var distance = Vector3.Distance (player.transform.position, transform.position);
			//if can see the player, move towards him
			if (distance < 100 /*&& distance > 5 && cont == true*/) {  
				//Debug.Log ("player is close");
				var delta = player.transform.position - transform.position;
				delta.Normalize ();
				transform.position = transform.position + (delta * moveSpeed * Time.deltaTime * 1.3f);


				if (distance < 10 && explodeOnce == true) {
					ani.SetBool ("explode", true);
					explodeOnce = false;
					Invoke ("destroyMe", 2.5f);
				}
			}
		
		}
	}

	void destroyMe(){
		Destroy (gameObject);
		GameObject go = (GameObject)Instantiate (Resources.Load ("Explode_suicide", typeof(GameObject)), _trans.position, Quaternion.identity);
		Destroy (go, 1f);

	}
}
	
