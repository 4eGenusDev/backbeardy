﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	Vector3 _startingPos;
	Transform _trans;
	public float moveSpeed;



	// Use this for initialization
	void Start () {
		_trans = GetComponent<Transform>();
		_startingPos = _trans.position;
		GameObject go = GameObject.Find ("EnemySpawner");
		Parameters parameters = go.GetComponent<Parameters> ();
		moveSpeed = parameters.enemySpeed;

	}
	
	// Update is called once per frame
	void Update () {
	//Move
		_trans.position = new Vector3(_startingPos.x+=moveSpeed*Time.deltaTime/2, _startingPos.y + Mathf.PingPong(Time.time, 2), _startingPos.z);
	
	}
}
