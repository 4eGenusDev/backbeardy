﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class SpawnEnemies : MonoBehaviour
{
	BoxCollider2D myBox;
	Vector3 pos;
	int rand;
	float randTime;
	float x1;
	float x2;
	Vector3 bottomLeft;
	int wave;
	public List <GameObject> enemiesPool;
	public List <GameObject> explosionsPool;
	int numberOfEnemies= 100;
	float spawnCoeficient;

	void Start ()
	{
		
		spawnCoeficient = gameObject.GetComponent<Parameters> ().spawnCoeficient;
		//set boundaries of the Enemy Spawner
		myBox = GetComponent<BoxCollider2D> ();
		x1 = myBox.transform.position.y - myBox.bounds.size.y / 2;
		x2 = myBox.transform.position.y + myBox.bounds.size.y / 2;

		CreatePool ();
		Invoke ("SpawnThem", Random.Range(3f,6f));

	}

	void SpawnThem(){
		rand = Random.Range (0, 90);
		pos = new Vector2 (transform.position.x, Random.Range (x1, x2));
		bottomLeft = new Vector2 (transform.position.x, x1);

		//calculate the randTime spawning time with the spawnCoeficient
		randTime = Random.Range (2.5f * spawnCoeficient, 5.5f * spawnCoeficient);

		if (randTime > 4) {
			//a chance to instantiate the diagonally flying bird
			Instantiate (Resources.Load ("enemies/Enemy_5a", typeof(GameObject)), bottomLeft, Quaternion.identity);
		}
		if (enemiesPool[rand].activeSelf == false){
		enemiesPool [rand].transform.position = pos;
		enemiesPool [rand].SetActive (true);
		//enemiesPool.RemoveAt (rand);
		}
		//ON LEVEL 2 == SEA, ADD THE SeaShell,SeaHorse,Spike and SpikeTurtle
		if (Application.loadedLevelName == "Sea") {
			//spawn SeaShell every 10th wave
			if (wave % 14 == 0) {
				Instantiate (Resources.Load ("enemies/Sea/SeaShell", typeof(GameObject)), bottomLeft, Quaternion.identity);
			} 
			//spawh SeaHorse every 17th wave
			else if (wave % 17 == 0) {
				Instantiate (Resources.Load ("enemies/Sea/SeaHorse", typeof(GameObject)), pos, Quaternion.identity);
			} 
			else if (wave % 12 == 0) {
				Instantiate (Resources.Load ("enemies/Sea/SeaSpiky", typeof(GameObject)), pos, Quaternion.identity);
			} 
			else if (wave % 9 == 0) 
			{
				Instantiate (Resources.Load ("enemies/Sea/SeaTurtle", typeof(GameObject)), pos, Quaternion.identity);
			}

			Debug.Log (Application.loadedLevelName + randTime + " " + wave);
			wave++;
		}
		Invoke ("SpawnThem", randTime);
	}


	void CreatePool()
	{

		for (int i = 0; i < numberOfEnemies; i++) {
			int rand = Random.Range (1, 5);
			GameObject newObject = Instantiate (Resources.Load ("enemies/Enemy_" + rand, typeof(GameObject)), pos, Quaternion.identity) as GameObject;
			newObject.SetActive (false);
			enemiesPool.Add (newObject);
			newObject.transform.parent = gameObject.transform;
		}

		//Create explosions pool
		for (int i = 0; i < 20; i++) {
			GameObject newObject = Instantiate (Resources.Load ("Particles/Toon_Explode/Explode_1", typeof(GameObject)), gameObject.transform.position, Quaternion.identity) as GameObject;
			newObject.SetActive (false);
			explosionsPool.Add (newObject);
		}

	}



	}

