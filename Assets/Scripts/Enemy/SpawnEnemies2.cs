﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SpawnEnemies2 : MonoBehaviour {

//	float moveSpeed=gameObject.GetComponent<EnShoot>().moveSpeed;
	BoxCollider2D myBox;
	Vector3 pos;
	float randTime;
	float x1;
	float x2;
	Vector3 bottomLeft;
	int wave;
	public List <GameObject> enemiesPool;
	public List <GameObject> explosionsPool;
	int numberOfEnemies= 30;


	void Start ()
	{
		myBox = GetComponent<BoxCollider2D> ();
		x1 = myBox.transform.position.y - myBox.bounds.size.y / 2;
		x2 = myBox.transform.position.y + myBox.bounds.size.y / 2;

		CreatePool ();
		Invoke ("SpawnThem", Random.Range(6f,10f));
	}


	void Update ()
	{
		randTime = Random.Range (0.5f, 5.5f);
		bottomLeft = new Vector2 (transform.position.x, x1);

	}

	void SpawnThem(){
		pos = new Vector2 (transform.position.x, Random.Range (x1, x2));
		enemiesPool [0].transform.position = pos;
		enemiesPool [0].SetActive (true);
		enemiesPool.RemoveAt (0);
		Invoke ("SpawnThem", 2);
	}


	void CreatePool()
	{
		//Create standard enemies pool
		for (int i = 0; i < numberOfEnemies; i++) {
			int rand = Random.Range (1, 5);
			GameObject newObject = Instantiate (Resources.Load ("enemies/Enemy_" + rand, typeof(GameObject)), gameObject.transform.position, Quaternion.identity) as GameObject;
			newObject.SetActive (false);
			enemiesPool.Add (newObject);
			newObject.transform.parent = gameObject.transform;
		}

		//Create explosions pool
		for (int i = 0; i < 20; i++) {
			GameObject newObject = Instantiate (Resources.Load ("Particles/Toon_Explode/Explode_1", typeof(GameObject)), gameObject.transform.position, Quaternion.identity) as GameObject;
			newObject.SetActive (false);
			enemiesPool.Add (newObject);
			newObject.transform.parent = gameObject.transform;
		}

	}

}
