﻿using UnityEngine;
using System.Collections;

public class rotatingSphere : MonoBehaviour {
	Transform sphere;
	float rotationSpeed = 5f;


	void Start()
	{
		sphere = this.transform;
	}

	void Update () {
		
		sphere.Rotate(0, Time.deltaTime*rotationSpeed, 0, Space.World);
	}
}
