﻿using UnityEngine;
using System.Collections;

public class Parallaxing : MonoBehaviour {

	public Transform[] backgrounds;			// Array (list) of all the back- and foregrounds to be parallaxed

	public float[] speed = { 0.3f, 0.6f, 1f };
	float parameters;
	void Start()
	{
		parameters = gameObject.GetComponent<Parameters> ().spawnCoeficient;
	}



	// Update is called once per frame
	void Update () {
		
		// for each background offset with the speed, the speed changes every checkpoint with the spawn coeficient;
		for (int i = 0; i < backgrounds.Length; i++) {
			speed [i] = speed [i] * parameters;
			        MeshRenderer mr = backgrounds[i].GetComponent<MeshRenderer> ();
					Material mat = mr.material;
					Vector2 offset = mat.mainTextureOffset;
			        offset.x -= (Time.deltaTime/20) * speed[i];
			        offset.x = (offset.x) % -3;
					mat.mainTextureOffset = offset;

	}
}
}
