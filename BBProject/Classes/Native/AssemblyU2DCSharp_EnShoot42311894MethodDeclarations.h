﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnShoot
struct EnShoot_t42311894;

#include "codegen/il2cpp-codegen.h"

// System.Void EnShoot::.ctor()
extern "C"  void EnShoot__ctor_m3512475477 (EnShoot_t42311894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot::Start()
extern "C"  void EnShoot_Start_m2459613269 (EnShoot_t42311894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot::Update()
extern "C"  void EnShoot_Update_m3239419480 (EnShoot_t42311894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot::Shoot()
extern "C"  void EnShoot_Shoot_m2128903378 (EnShoot_t42311894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
