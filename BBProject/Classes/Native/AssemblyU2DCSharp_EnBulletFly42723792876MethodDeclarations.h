﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnBulletFly4
struct EnBulletFly4_t2723792876;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void EnBulletFly4::.ctor()
extern "C"  void EnBulletFly4__ctor_m3291632687 (EnBulletFly4_t2723792876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly4::Start()
extern "C"  void EnBulletFly4_Start_m2238770479 (EnBulletFly4_t2723792876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly4::Update()
extern "C"  void EnBulletFly4_Update_m688260286 (EnBulletFly4_t2723792876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly4::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void EnBulletFly4_OnTriggerEnter2D_m759319849 (EnBulletFly4_t2723792876 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
