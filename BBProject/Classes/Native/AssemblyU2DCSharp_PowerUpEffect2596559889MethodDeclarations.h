﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PowerUpEffect
struct PowerUpEffect_t2596559889;

#include "codegen/il2cpp-codegen.h"

// System.Void PowerUpEffect::.ctor()
extern "C"  void PowerUpEffect__ctor_m3321759866 (PowerUpEffect_t2596559889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PowerUpEffect::Start()
extern "C"  void PowerUpEffect_Start_m2268897658 (PowerUpEffect_t2596559889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PowerUpEffect::Update()
extern "C"  void PowerUpEffect_Update_m1622202835 (PowerUpEffect_t2596559889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
