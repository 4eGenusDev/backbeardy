﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t2509538522;
// UnityEngine.Shader
struct Shader_t3998140498;
// UnityEngine.Material
struct Material_t1886596500;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FrostEffect
struct  FrostEffect_t3874425173  : public MonoBehaviour_t3012272455
{
public:
	// System.Single FrostEffect::FrostAmount
	float ___FrostAmount_2;
	// System.Single FrostEffect::EdgeSharpness
	float ___EdgeSharpness_3;
	// System.Single FrostEffect::minFrost
	float ___minFrost_4;
	// System.Single FrostEffect::maxFrost
	float ___maxFrost_5;
	// System.Single FrostEffect::seethroughness
	float ___seethroughness_6;
	// System.Single FrostEffect::distortion
	float ___distortion_7;
	// UnityEngine.Texture2D FrostEffect::Frost
	Texture2D_t2509538522 * ___Frost_8;
	// UnityEngine.Texture2D FrostEffect::FrostNormals
	Texture2D_t2509538522 * ___FrostNormals_9;
	// UnityEngine.Shader FrostEffect::Shader
	Shader_t3998140498 * ___Shader_10;
	// UnityEngine.Material FrostEffect::material
	Material_t1886596500 * ___material_11;

public:
	inline static int32_t get_offset_of_FrostAmount_2() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___FrostAmount_2)); }
	inline float get_FrostAmount_2() const { return ___FrostAmount_2; }
	inline float* get_address_of_FrostAmount_2() { return &___FrostAmount_2; }
	inline void set_FrostAmount_2(float value)
	{
		___FrostAmount_2 = value;
	}

	inline static int32_t get_offset_of_EdgeSharpness_3() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___EdgeSharpness_3)); }
	inline float get_EdgeSharpness_3() const { return ___EdgeSharpness_3; }
	inline float* get_address_of_EdgeSharpness_3() { return &___EdgeSharpness_3; }
	inline void set_EdgeSharpness_3(float value)
	{
		___EdgeSharpness_3 = value;
	}

	inline static int32_t get_offset_of_minFrost_4() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___minFrost_4)); }
	inline float get_minFrost_4() const { return ___minFrost_4; }
	inline float* get_address_of_minFrost_4() { return &___minFrost_4; }
	inline void set_minFrost_4(float value)
	{
		___minFrost_4 = value;
	}

	inline static int32_t get_offset_of_maxFrost_5() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___maxFrost_5)); }
	inline float get_maxFrost_5() const { return ___maxFrost_5; }
	inline float* get_address_of_maxFrost_5() { return &___maxFrost_5; }
	inline void set_maxFrost_5(float value)
	{
		___maxFrost_5 = value;
	}

	inline static int32_t get_offset_of_seethroughness_6() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___seethroughness_6)); }
	inline float get_seethroughness_6() const { return ___seethroughness_6; }
	inline float* get_address_of_seethroughness_6() { return &___seethroughness_6; }
	inline void set_seethroughness_6(float value)
	{
		___seethroughness_6 = value;
	}

	inline static int32_t get_offset_of_distortion_7() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___distortion_7)); }
	inline float get_distortion_7() const { return ___distortion_7; }
	inline float* get_address_of_distortion_7() { return &___distortion_7; }
	inline void set_distortion_7(float value)
	{
		___distortion_7 = value;
	}

	inline static int32_t get_offset_of_Frost_8() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___Frost_8)); }
	inline Texture2D_t2509538522 * get_Frost_8() const { return ___Frost_8; }
	inline Texture2D_t2509538522 ** get_address_of_Frost_8() { return &___Frost_8; }
	inline void set_Frost_8(Texture2D_t2509538522 * value)
	{
		___Frost_8 = value;
		Il2CppCodeGenWriteBarrier(&___Frost_8, value);
	}

	inline static int32_t get_offset_of_FrostNormals_9() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___FrostNormals_9)); }
	inline Texture2D_t2509538522 * get_FrostNormals_9() const { return ___FrostNormals_9; }
	inline Texture2D_t2509538522 ** get_address_of_FrostNormals_9() { return &___FrostNormals_9; }
	inline void set_FrostNormals_9(Texture2D_t2509538522 * value)
	{
		___FrostNormals_9 = value;
		Il2CppCodeGenWriteBarrier(&___FrostNormals_9, value);
	}

	inline static int32_t get_offset_of_Shader_10() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___Shader_10)); }
	inline Shader_t3998140498 * get_Shader_10() const { return ___Shader_10; }
	inline Shader_t3998140498 ** get_address_of_Shader_10() { return &___Shader_10; }
	inline void set_Shader_10(Shader_t3998140498 * value)
	{
		___Shader_10 = value;
		Il2CppCodeGenWriteBarrier(&___Shader_10, value);
	}

	inline static int32_t get_offset_of_material_11() { return static_cast<int32_t>(offsetof(FrostEffect_t3874425173, ___material_11)); }
	inline Material_t1886596500 * get_material_11() const { return ___material_11; }
	inline Material_t1886596500 ** get_address_of_material_11() { return &___material_11; }
	inline void set_material_11(Material_t1886596500 * value)
	{
		___material_11 = value;
		Il2CppCodeGenWriteBarrier(&___material_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
