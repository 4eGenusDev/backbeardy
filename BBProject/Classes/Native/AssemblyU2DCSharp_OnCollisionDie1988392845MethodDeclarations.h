﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnCollisionDie
struct OnCollisionDie_t1988392845;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void OnCollisionDie::.ctor()
extern "C"  void OnCollisionDie__ctor_m3021636526 (OnCollisionDie_t1988392845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnCollisionDie::Start()
extern "C"  void OnCollisionDie_Start_m1968774318 (OnCollisionDie_t1988392845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnCollisionDie::Update()
extern "C"  void OnCollisionDie_Update_m908313887 (OnCollisionDie_t1988392845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnCollisionDie::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void OnCollisionDie_OnTriggerEnter2D_m35539338 (OnCollisionDie_t1988392845 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnCollisionDie::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void OnCollisionDie_OnParticleCollision_m1081715761 (OnCollisionDie_t1988392845 * __this, GameObject_t4012695102 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
