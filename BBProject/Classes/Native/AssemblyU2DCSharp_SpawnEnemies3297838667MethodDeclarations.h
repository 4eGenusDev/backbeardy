﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpawnEnemies
struct SpawnEnemies_t3297838667;

#include "codegen/il2cpp-codegen.h"

// System.Void SpawnEnemies::.ctor()
extern "C"  void SpawnEnemies__ctor_m2692862128 (SpawnEnemies_t3297838667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnEnemies::Start()
extern "C"  void SpawnEnemies_Start_m1639999920 (SpawnEnemies_t3297838667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnEnemies::SpawnThem()
extern "C"  void SpawnEnemies_SpawnThem_m2594919877 (SpawnEnemies_t3297838667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
