﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldUvAnimation
struct ShieldUvAnimation_t644239002;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldUvAnimation::.ctor()
extern "C"  void ShieldUvAnimation__ctor_m2563976209 (ShieldUvAnimation_t644239002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldUvAnimation::Start()
extern "C"  void ShieldUvAnimation_Start_m1511114001 (ShieldUvAnimation_t644239002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldUvAnimation::Update()
extern "C"  void ShieldUvAnimation_Update_m3900713244 (ShieldUvAnimation_t644239002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
