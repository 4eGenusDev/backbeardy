﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.GUITexture
struct GUITexture_t63494093;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseButton
struct  PauseButton_t3671462056  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean PauseButton::paused
	bool ___paused_2;
	// UnityEngine.GameObject PauseButton::pauseButton
	GameObject_t4012695102 * ___pauseButton_3;
	// UnityEngine.GameObject PauseButton::pausePanel
	GameObject_t4012695102 * ___pausePanel_4;
	// UnityEngine.GameObject PauseButton::exitButton
	GameObject_t4012695102 * ___exitButton_5;
	// UnityEngine.GameObject PauseButton::tryAgainButton
	GameObject_t4012695102 * ___tryAgainButton_6;
	// UnityEngine.GameObject PauseButton::darkGO
	GameObject_t4012695102 * ___darkGO_7;
	// UnityEngine.GameObject PauseButton::scoreBoard
	GameObject_t4012695102 * ___scoreBoard_8;
	// UnityEngine.GUITexture PauseButton::darkenScreen
	GUITexture_t63494093 * ___darkenScreen_9;
	// UnityEngine.AudioClip PauseButton::pauseSound
	AudioClip_t3714538611 * ___pauseSound_10;
	// UnityEngine.AudioSource PauseButton::playSound
	AudioSource_t3628549054 * ___playSound_11;

public:
	inline static int32_t get_offset_of_paused_2() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___paused_2)); }
	inline bool get_paused_2() const { return ___paused_2; }
	inline bool* get_address_of_paused_2() { return &___paused_2; }
	inline void set_paused_2(bool value)
	{
		___paused_2 = value;
	}

	inline static int32_t get_offset_of_pauseButton_3() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___pauseButton_3)); }
	inline GameObject_t4012695102 * get_pauseButton_3() const { return ___pauseButton_3; }
	inline GameObject_t4012695102 ** get_address_of_pauseButton_3() { return &___pauseButton_3; }
	inline void set_pauseButton_3(GameObject_t4012695102 * value)
	{
		___pauseButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___pauseButton_3, value);
	}

	inline static int32_t get_offset_of_pausePanel_4() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___pausePanel_4)); }
	inline GameObject_t4012695102 * get_pausePanel_4() const { return ___pausePanel_4; }
	inline GameObject_t4012695102 ** get_address_of_pausePanel_4() { return &___pausePanel_4; }
	inline void set_pausePanel_4(GameObject_t4012695102 * value)
	{
		___pausePanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___pausePanel_4, value);
	}

	inline static int32_t get_offset_of_exitButton_5() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___exitButton_5)); }
	inline GameObject_t4012695102 * get_exitButton_5() const { return ___exitButton_5; }
	inline GameObject_t4012695102 ** get_address_of_exitButton_5() { return &___exitButton_5; }
	inline void set_exitButton_5(GameObject_t4012695102 * value)
	{
		___exitButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___exitButton_5, value);
	}

	inline static int32_t get_offset_of_tryAgainButton_6() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___tryAgainButton_6)); }
	inline GameObject_t4012695102 * get_tryAgainButton_6() const { return ___tryAgainButton_6; }
	inline GameObject_t4012695102 ** get_address_of_tryAgainButton_6() { return &___tryAgainButton_6; }
	inline void set_tryAgainButton_6(GameObject_t4012695102 * value)
	{
		___tryAgainButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___tryAgainButton_6, value);
	}

	inline static int32_t get_offset_of_darkGO_7() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___darkGO_7)); }
	inline GameObject_t4012695102 * get_darkGO_7() const { return ___darkGO_7; }
	inline GameObject_t4012695102 ** get_address_of_darkGO_7() { return &___darkGO_7; }
	inline void set_darkGO_7(GameObject_t4012695102 * value)
	{
		___darkGO_7 = value;
		Il2CppCodeGenWriteBarrier(&___darkGO_7, value);
	}

	inline static int32_t get_offset_of_scoreBoard_8() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___scoreBoard_8)); }
	inline GameObject_t4012695102 * get_scoreBoard_8() const { return ___scoreBoard_8; }
	inline GameObject_t4012695102 ** get_address_of_scoreBoard_8() { return &___scoreBoard_8; }
	inline void set_scoreBoard_8(GameObject_t4012695102 * value)
	{
		___scoreBoard_8 = value;
		Il2CppCodeGenWriteBarrier(&___scoreBoard_8, value);
	}

	inline static int32_t get_offset_of_darkenScreen_9() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___darkenScreen_9)); }
	inline GUITexture_t63494093 * get_darkenScreen_9() const { return ___darkenScreen_9; }
	inline GUITexture_t63494093 ** get_address_of_darkenScreen_9() { return &___darkenScreen_9; }
	inline void set_darkenScreen_9(GUITexture_t63494093 * value)
	{
		___darkenScreen_9 = value;
		Il2CppCodeGenWriteBarrier(&___darkenScreen_9, value);
	}

	inline static int32_t get_offset_of_pauseSound_10() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___pauseSound_10)); }
	inline AudioClip_t3714538611 * get_pauseSound_10() const { return ___pauseSound_10; }
	inline AudioClip_t3714538611 ** get_address_of_pauseSound_10() { return &___pauseSound_10; }
	inline void set_pauseSound_10(AudioClip_t3714538611 * value)
	{
		___pauseSound_10 = value;
		Il2CppCodeGenWriteBarrier(&___pauseSound_10, value);
	}

	inline static int32_t get_offset_of_playSound_11() { return static_cast<int32_t>(offsetof(PauseButton_t3671462056, ___playSound_11)); }
	inline AudioSource_t3628549054 * get_playSound_11() const { return ___playSound_11; }
	inline AudioSource_t3628549054 ** get_address_of_playSound_11() { return &___playSound_11; }
	inline void set_playSound_11(AudioSource_t3628549054 * value)
	{
		___playSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___playSound_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
