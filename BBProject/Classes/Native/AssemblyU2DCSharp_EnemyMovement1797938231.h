﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyMovement
struct  EnemyMovement_t1797938231  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Vector3 EnemyMovement::_startingPos
	Vector3_t3525329789  ____startingPos_2;
	// UnityEngine.Transform EnemyMovement::_trans
	Transform_t284553113 * ____trans_3;
	// System.Single EnemyMovement::moveSpeed
	float ___moveSpeed_4;

public:
	inline static int32_t get_offset_of__startingPos_2() { return static_cast<int32_t>(offsetof(EnemyMovement_t1797938231, ____startingPos_2)); }
	inline Vector3_t3525329789  get__startingPos_2() const { return ____startingPos_2; }
	inline Vector3_t3525329789 * get_address_of__startingPos_2() { return &____startingPos_2; }
	inline void set__startingPos_2(Vector3_t3525329789  value)
	{
		____startingPos_2 = value;
	}

	inline static int32_t get_offset_of__trans_3() { return static_cast<int32_t>(offsetof(EnemyMovement_t1797938231, ____trans_3)); }
	inline Transform_t284553113 * get__trans_3() const { return ____trans_3; }
	inline Transform_t284553113 ** get_address_of__trans_3() { return &____trans_3; }
	inline void set__trans_3(Transform_t284553113 * value)
	{
		____trans_3 = value;
		Il2CppCodeGenWriteBarrier(&____trans_3, value);
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(EnemyMovement_t1797938231, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
