﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyMovement5a
struct EnemyMovement5a_t1241788739;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyMovement5a::.ctor()
extern "C"  void EnemyMovement5a__ctor_m2323861896 (EnemyMovement5a_t1241788739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement5a::Start()
extern "C"  void EnemyMovement5a_Start_m1270999688 (EnemyMovement5a_t1241788739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement5a::Update()
extern "C"  void EnemyMovement5a_Update_m752136837 (EnemyMovement5a_t1241788739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
