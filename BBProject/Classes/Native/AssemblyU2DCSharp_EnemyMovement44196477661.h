﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyMovement4
struct  EnemyMovement4_t4196477661  : public MonoBehaviour_t3012272455
{
public:
	// System.Single EnemyMovement4::moveSpeed
	float ___moveSpeed_2;
	// UnityEngine.Transform EnemyMovement4::_trans
	Transform_t284553113 * ____trans_3;
	// UnityEngine.Vector3 EnemyMovement4::StartingPos
	Vector3_t3525329789  ___StartingPos_4;
	// System.Single EnemyMovement4::randomPos
	float ___randomPos_5;

public:
	inline static int32_t get_offset_of_moveSpeed_2() { return static_cast<int32_t>(offsetof(EnemyMovement4_t4196477661, ___moveSpeed_2)); }
	inline float get_moveSpeed_2() const { return ___moveSpeed_2; }
	inline float* get_address_of_moveSpeed_2() { return &___moveSpeed_2; }
	inline void set_moveSpeed_2(float value)
	{
		___moveSpeed_2 = value;
	}

	inline static int32_t get_offset_of__trans_3() { return static_cast<int32_t>(offsetof(EnemyMovement4_t4196477661, ____trans_3)); }
	inline Transform_t284553113 * get__trans_3() const { return ____trans_3; }
	inline Transform_t284553113 ** get_address_of__trans_3() { return &____trans_3; }
	inline void set__trans_3(Transform_t284553113 * value)
	{
		____trans_3 = value;
		Il2CppCodeGenWriteBarrier(&____trans_3, value);
	}

	inline static int32_t get_offset_of_StartingPos_4() { return static_cast<int32_t>(offsetof(EnemyMovement4_t4196477661, ___StartingPos_4)); }
	inline Vector3_t3525329789  get_StartingPos_4() const { return ___StartingPos_4; }
	inline Vector3_t3525329789 * get_address_of_StartingPos_4() { return &___StartingPos_4; }
	inline void set_StartingPos_4(Vector3_t3525329789  value)
	{
		___StartingPos_4 = value;
	}

	inline static int32_t get_offset_of_randomPos_5() { return static_cast<int32_t>(offsetof(EnemyMovement4_t4196477661, ___randomPos_5)); }
	inline float get_randomPos_5() const { return ___randomPos_5; }
	inline float* get_address_of_randomPos_5() { return &___randomPos_5; }
	inline void set_randomPos_5(float value)
	{
		___randomPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
