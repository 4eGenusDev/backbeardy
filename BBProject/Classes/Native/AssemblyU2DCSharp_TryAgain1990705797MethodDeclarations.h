﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TryAgain
struct TryAgain_t1990705797;

#include "codegen/il2cpp-codegen.h"

// System.Void TryAgain::.ctor()
extern "C"  void TryAgain__ctor_m2946171318 (TryAgain_t1990705797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TryAgain::Start()
extern "C"  void TryAgain_Start_m1893309110 (TryAgain_t1990705797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TryAgain::Update()
extern "C"  void TryAgain_Update_m2863859735 (TryAgain_t1990705797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
