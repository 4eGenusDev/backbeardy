﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnBulletFly4a
struct EnBulletFly4a_t2833200629;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void EnBulletFly4a::.ctor()
extern "C"  void EnBulletFly4a__ctor_m3346009878 (EnBulletFly4a_t2833200629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly4a::Start()
extern "C"  void EnBulletFly4a_Start_m2293147670 (EnBulletFly4a_t2833200629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly4a::Update()
extern "C"  void EnBulletFly4a_Update_m2373953207 (EnBulletFly4a_t2833200629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly4a::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void EnBulletFly4a_OnTriggerEnter2D_m2177747746 (EnBulletFly4a_t2833200629 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
