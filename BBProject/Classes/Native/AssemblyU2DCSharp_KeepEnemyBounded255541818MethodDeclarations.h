﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KeepEnemyBounded
struct KeepEnemyBounded_t255541818;

#include "codegen/il2cpp-codegen.h"

// System.Void KeepEnemyBounded::.ctor()
extern "C"  void KeepEnemyBounded__ctor_m323470753 (KeepEnemyBounded_t255541818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KeepEnemyBounded::Start()
extern "C"  void KeepEnemyBounded_Start_m3565575841 (KeepEnemyBounded_t255541818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KeepEnemyBounded::Update()
extern "C"  void KeepEnemyBounded_Update_m3164520844 (KeepEnemyBounded_t255541818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
