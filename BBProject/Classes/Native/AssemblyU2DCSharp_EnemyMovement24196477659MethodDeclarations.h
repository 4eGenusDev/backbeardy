﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyMovement2
struct EnemyMovement2_t4196477659;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyMovement2::.ctor()
extern "C"  void EnemyMovement2__ctor_m1215801376 (EnemyMovement2_t4196477659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement2::Start()
extern "C"  void EnemyMovement2_Start_m162939168 (EnemyMovement2_t4196477659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement2::Update()
extern "C"  void EnemyMovement2_Update_m761999085 (EnemyMovement2_t4196477659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
