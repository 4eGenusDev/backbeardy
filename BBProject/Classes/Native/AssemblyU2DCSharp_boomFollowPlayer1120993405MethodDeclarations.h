﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// boomFollowPlayer
struct boomFollowPlayer_t1120993405;

#include "codegen/il2cpp-codegen.h"

// System.Void boomFollowPlayer::.ctor()
extern "C"  void boomFollowPlayer__ctor_m2290115262 (boomFollowPlayer_t1120993405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void boomFollowPlayer::Start()
extern "C"  void boomFollowPlayer_Start_m1237253054 (boomFollowPlayer_t1120993405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void boomFollowPlayer::Update()
extern "C"  void boomFollowPlayer_Update_m4000958479 (boomFollowPlayer_t1120993405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
