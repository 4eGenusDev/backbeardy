﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpawnEnemies2
struct SpawnEnemies2_t3448750919;

#include "codegen/il2cpp-codegen.h"

// System.Void SpawnEnemies2::.ctor()
extern "C"  void SpawnEnemies2__ctor_m2610191876 (SpawnEnemies2_t3448750919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnEnemies2::Start()
extern "C"  void SpawnEnemies2_Start_m1557329668 (SpawnEnemies2_t3448750919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnEnemies2::Update()
extern "C"  void SpawnEnemies2_Update_m1038431625 (SpawnEnemies2_t3448750919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnEnemies2::SpawnThem()
extern "C"  void SpawnEnemies2_SpawnThem_m2219776281 (SpawnEnemies2_t3448750919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
