﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PowerUpMove
struct PowerUpMove_t2161606225;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void PowerUpMove::.ctor()
extern "C"  void PowerUpMove__ctor_m1159628858 (PowerUpMove_t2161606225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PowerUpMove::Start()
extern "C"  void PowerUpMove_Start_m106766650 (PowerUpMove_t2161606225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PowerUpMove::Update()
extern "C"  void PowerUpMove_Update_m3315618323 (PowerUpMove_t2161606225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PowerUpMove::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void PowerUpMove_OnTriggerEnter2D_m2383021694 (PowerUpMove_t2161606225 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
