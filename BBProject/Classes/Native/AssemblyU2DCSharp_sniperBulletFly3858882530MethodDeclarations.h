﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// sniperBulletFly
struct sniperBulletFly_t3858882530;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void sniperBulletFly::.ctor()
extern "C"  void sniperBulletFly__ctor_m3215369673 (sniperBulletFly_t3858882530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void sniperBulletFly::Start()
extern "C"  void sniperBulletFly_Start_m2162507465 (sniperBulletFly_t3858882530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void sniperBulletFly::Update()
extern "C"  void sniperBulletFly_Update_m2619074148 (sniperBulletFly_t3858882530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void sniperBulletFly::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void sniperBulletFly_OnTriggerEnter2D_m1115166415 (sniperBulletFly_t3858882530 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
