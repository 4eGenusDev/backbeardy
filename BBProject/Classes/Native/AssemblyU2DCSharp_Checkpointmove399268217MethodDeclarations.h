﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Checkpointmove
struct Checkpointmove_t399268217;

#include "codegen/il2cpp-codegen.h"

// System.Void Checkpointmove::.ctor()
extern "C"  void Checkpointmove__ctor_m1389628226 (Checkpointmove_t399268217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkpointmove::Start()
extern "C"  void Checkpointmove_Start_m336766018 (Checkpointmove_t399268217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkpointmove::Update()
extern "C"  void Checkpointmove_Update_m1855664139 (Checkpointmove_t399268217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
