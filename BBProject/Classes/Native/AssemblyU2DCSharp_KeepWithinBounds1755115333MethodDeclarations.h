﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KeepWithinBounds
struct KeepWithinBounds_t1755115333;

#include "codegen/il2cpp-codegen.h"

// System.Void KeepWithinBounds::.ctor()
extern "C"  void KeepWithinBounds__ctor_m3617625334 (KeepWithinBounds_t1755115333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KeepWithinBounds::Start()
extern "C"  void KeepWithinBounds_Start_m2564763126 (KeepWithinBounds_t1755115333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KeepWithinBounds::Update()
extern "C"  void KeepWithinBounds_Update_m2204097751 (KeepWithinBounds_t1755115333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
