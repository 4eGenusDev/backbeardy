﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyMovement3
struct EnemyMovement3_t4196477660;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyMovement3::.ctor()
extern "C"  void EnemyMovement3__ctor_m1019287871 (EnemyMovement3_t4196477660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement3::Start()
extern "C"  void EnemyMovement3_Start_m4261392959 (EnemyMovement3_t4196477660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement3::Update()
extern "C"  void EnemyMovement3_Update_m3260015022 (EnemyMovement3_t4196477660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement3::destroyMe()
extern "C"  void EnemyMovement3_destroyMe_m2578077487 (EnemyMovement3_t4196477660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
