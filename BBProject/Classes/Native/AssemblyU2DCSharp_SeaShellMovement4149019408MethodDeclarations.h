﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SeaShellMovement
struct SeaShellMovement_t4149019408;

#include "codegen/il2cpp-codegen.h"

// System.Void SeaShellMovement::.ctor()
extern "C"  void SeaShellMovement__ctor_m4170356107 (SeaShellMovement_t4149019408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SeaShellMovement::Start()
extern "C"  void SeaShellMovement_Start_m3117493899 (SeaShellMovement_t4149019408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SeaShellMovement::Update()
extern "C"  void SeaShellMovement_Update_m2158882530 (SeaShellMovement_t4149019408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
