﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player_Movement
struct Player_Movement_t3938410573;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void Player_Movement::.ctor()
extern "C"  void Player_Movement__ctor_m2462805438 (Player_Movement_t3938410573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Movement::Start()
extern "C"  void Player_Movement_Start_m1409943230 (Player_Movement_t3938410573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Movement::Update()
extern "C"  void Player_Movement_Update_m764419343 (Player_Movement_t3938410573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Movement::timerManiac()
extern "C"  void Player_Movement_timerManiac_m2031532946 (Player_Movement_t3938410573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Movement::timerDrunk()
extern "C"  void Player_Movement_timerDrunk_m607062405 (Player_Movement_t3938410573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Movement::timerChrono()
extern "C"  void Player_Movement_timerChrono_m3821313796 (Player_Movement_t3938410573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Movement::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Player_Movement_OnTriggerEnter2D_m2494488954 (Player_Movement_t3938410573 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
