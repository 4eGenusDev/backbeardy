﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// Parameters
struct Parameters_t2452200970;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnShoot
struct  EnShoot_t42311894  : public MonoBehaviour_t3012272455
{
public:
	// System.Single EnShoot::moveSpeed
	float ___moveSpeed_2;
	// UnityEngine.Vector3 EnShoot::_desiredPos
	Vector3_t3525329789  ____desiredPos_3;
	// UnityEngine.Vector3 EnShoot::_transPos
	Vector3_t3525329789  ____transPos_4;
	// UnityEngine.GameObject EnShoot::go
	GameObject_t4012695102 * ___go_5;
	// Parameters EnShoot::parameters
	Parameters_t2452200970 * ___parameters_6;

public:
	inline static int32_t get_offset_of_moveSpeed_2() { return static_cast<int32_t>(offsetof(EnShoot_t42311894, ___moveSpeed_2)); }
	inline float get_moveSpeed_2() const { return ___moveSpeed_2; }
	inline float* get_address_of_moveSpeed_2() { return &___moveSpeed_2; }
	inline void set_moveSpeed_2(float value)
	{
		___moveSpeed_2 = value;
	}

	inline static int32_t get_offset_of__desiredPos_3() { return static_cast<int32_t>(offsetof(EnShoot_t42311894, ____desiredPos_3)); }
	inline Vector3_t3525329789  get__desiredPos_3() const { return ____desiredPos_3; }
	inline Vector3_t3525329789 * get_address_of__desiredPos_3() { return &____desiredPos_3; }
	inline void set__desiredPos_3(Vector3_t3525329789  value)
	{
		____desiredPos_3 = value;
	}

	inline static int32_t get_offset_of__transPos_4() { return static_cast<int32_t>(offsetof(EnShoot_t42311894, ____transPos_4)); }
	inline Vector3_t3525329789  get__transPos_4() const { return ____transPos_4; }
	inline Vector3_t3525329789 * get_address_of__transPos_4() { return &____transPos_4; }
	inline void set__transPos_4(Vector3_t3525329789  value)
	{
		____transPos_4 = value;
	}

	inline static int32_t get_offset_of_go_5() { return static_cast<int32_t>(offsetof(EnShoot_t42311894, ___go_5)); }
	inline GameObject_t4012695102 * get_go_5() const { return ___go_5; }
	inline GameObject_t4012695102 ** get_address_of_go_5() { return &___go_5; }
	inline void set_go_5(GameObject_t4012695102 * value)
	{
		___go_5 = value;
		Il2CppCodeGenWriteBarrier(&___go_5, value);
	}

	inline static int32_t get_offset_of_parameters_6() { return static_cast<int32_t>(offsetof(EnShoot_t42311894, ___parameters_6)); }
	inline Parameters_t2452200970 * get_parameters_6() const { return ___parameters_6; }
	inline Parameters_t2452200970 ** get_address_of_parameters_6() { return &___parameters_6; }
	inline void set_parameters_6(Parameters_t2452200970 * value)
	{
		___parameters_6 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
