﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// boomFollowEnemy
struct boomFollowEnemy_t3351201388;

#include "codegen/il2cpp-codegen.h"

// System.Void boomFollowEnemy::.ctor()
extern "C"  void boomFollowEnemy__ctor_m1966275199 (boomFollowEnemy_t3351201388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void boomFollowEnemy::Update()
extern "C"  void boomFollowEnemy_Update_m2551851118 (boomFollowEnemy_t3351201388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
