﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Parallaxing
struct Parallaxing_t2299696843;

#include "codegen/il2cpp-codegen.h"

// System.Void Parallaxing::.ctor()
extern "C"  void Parallaxing__ctor_m2631817472 (Parallaxing_t2299696843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Parallaxing::Start()
extern "C"  void Parallaxing_Start_m1578955264 (Parallaxing_t2299696843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Parallaxing::Update()
extern "C"  void Parallaxing_Update_m1708825101 (Parallaxing_t2299696843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
