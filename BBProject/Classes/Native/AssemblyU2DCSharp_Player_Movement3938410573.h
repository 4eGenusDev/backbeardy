﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// FrostEffect
struct FrostEffect_t3874425173;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player_Movement
struct  Player_Movement_t3938410573  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject Player_Movement::player
	GameObject_t4012695102 * ___player_2;
	// UnityEngine.GameObject Player_Movement::bullet
	GameObject_t4012695102 * ___bullet_3;
	// System.Single Player_Movement::MoveSpeedBeardy
	float ___MoveSpeedBeardy_4;
	// System.Boolean Player_Movement::isDrunk
	bool ___isDrunk_5;
	// System.Boolean Player_Movement::isChrono
	bool ___isChrono_6;
	// System.Boolean Player_Movement::isManiac
	bool ___isManiac_7;
	// System.Boolean Player_Movement::isPearl
	bool ___isPearl_8;
	// FrostEffect Player_Movement::frostEffect
	FrostEffect_t3874425173 * ___frostEffect_9;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(Player_Movement_t3938410573, ___player_2)); }
	inline GameObject_t4012695102 * get_player_2() const { return ___player_2; }
	inline GameObject_t4012695102 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(GameObject_t4012695102 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier(&___player_2, value);
	}

	inline static int32_t get_offset_of_bullet_3() { return static_cast<int32_t>(offsetof(Player_Movement_t3938410573, ___bullet_3)); }
	inline GameObject_t4012695102 * get_bullet_3() const { return ___bullet_3; }
	inline GameObject_t4012695102 ** get_address_of_bullet_3() { return &___bullet_3; }
	inline void set_bullet_3(GameObject_t4012695102 * value)
	{
		___bullet_3 = value;
		Il2CppCodeGenWriteBarrier(&___bullet_3, value);
	}

	inline static int32_t get_offset_of_MoveSpeedBeardy_4() { return static_cast<int32_t>(offsetof(Player_Movement_t3938410573, ___MoveSpeedBeardy_4)); }
	inline float get_MoveSpeedBeardy_4() const { return ___MoveSpeedBeardy_4; }
	inline float* get_address_of_MoveSpeedBeardy_4() { return &___MoveSpeedBeardy_4; }
	inline void set_MoveSpeedBeardy_4(float value)
	{
		___MoveSpeedBeardy_4 = value;
	}

	inline static int32_t get_offset_of_isDrunk_5() { return static_cast<int32_t>(offsetof(Player_Movement_t3938410573, ___isDrunk_5)); }
	inline bool get_isDrunk_5() const { return ___isDrunk_5; }
	inline bool* get_address_of_isDrunk_5() { return &___isDrunk_5; }
	inline void set_isDrunk_5(bool value)
	{
		___isDrunk_5 = value;
	}

	inline static int32_t get_offset_of_isChrono_6() { return static_cast<int32_t>(offsetof(Player_Movement_t3938410573, ___isChrono_6)); }
	inline bool get_isChrono_6() const { return ___isChrono_6; }
	inline bool* get_address_of_isChrono_6() { return &___isChrono_6; }
	inline void set_isChrono_6(bool value)
	{
		___isChrono_6 = value;
	}

	inline static int32_t get_offset_of_isManiac_7() { return static_cast<int32_t>(offsetof(Player_Movement_t3938410573, ___isManiac_7)); }
	inline bool get_isManiac_7() const { return ___isManiac_7; }
	inline bool* get_address_of_isManiac_7() { return &___isManiac_7; }
	inline void set_isManiac_7(bool value)
	{
		___isManiac_7 = value;
	}

	inline static int32_t get_offset_of_isPearl_8() { return static_cast<int32_t>(offsetof(Player_Movement_t3938410573, ___isPearl_8)); }
	inline bool get_isPearl_8() const { return ___isPearl_8; }
	inline bool* get_address_of_isPearl_8() { return &___isPearl_8; }
	inline void set_isPearl_8(bool value)
	{
		___isPearl_8 = value;
	}

	inline static int32_t get_offset_of_frostEffect_9() { return static_cast<int32_t>(offsetof(Player_Movement_t3938410573, ___frostEffect_9)); }
	inline FrostEffect_t3874425173 * get_frostEffect_9() const { return ___frostEffect_9; }
	inline FrostEffect_t3874425173 ** get_address_of_frostEffect_9() { return &___frostEffect_9; }
	inline void set_frostEffect_9(FrostEffect_t3874425173 * value)
	{
		___frostEffect_9 = value;
		Il2CppCodeGenWriteBarrier(&___frostEffect_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
