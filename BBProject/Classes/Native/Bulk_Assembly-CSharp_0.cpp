﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// boomFollowEnemy
struct boomFollowEnemy_t3351201388;
// boomFollowPlayer
struct boomFollowPlayer_t1120993405;
// BulletFly
struct BulletFly_t3267336177;
// UnityEngine.Transform
struct Transform_t284553113;
// System.Object
struct Il2CppObject;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;
// Checkpointmove
struct Checkpointmove_t399268217;
// CoinsCollision
struct CoinsCollision_t1324380528;
// DestroyOutOFBounds
struct DestroyOutOFBounds_t2076988544;
// EnBulletFly
struct EnBulletFly_t3690094920;
// Parameters
struct Parameters_t2452200970;
// EnBulletFly4
struct EnBulletFly4_t2723792876;
// EnBulletFly4a
struct EnBulletFly4a_t2833200629;
// EnemyMovement
struct EnemyMovement_t1797938231;
// EnemyMovement2
struct EnemyMovement2_t4196477659;
// EnemyMovement3
struct EnemyMovement3_t4196477660;
// UnityEngine.Animator
struct Animator_t792326996;
// EnemyMovement4
struct EnemyMovement4_t4196477661;
// EnemyMovement5a
struct EnemyMovement5a_t1241788739;
// EnShoot
struct EnShoot_t42311894;
// EnShoot2
struct EnShoot2_t1311668764;
// EnShoot4
struct EnShoot4_t1311668766;
// FrostEffect
struct FrostEffect_t3874425173;
// UnityEngine.RenderTexture
struct RenderTexture_t12905170;
// GoldCoinMovement
struct GoldCoinMovement_t1915910912;
// KeepCoffinBounded
struct KeepCoffinBounded_t702131559;
// KeepEnemyBounded
struct KeepEnemyBounded_t255541818;
// KeepWithinBounds
struct KeepWithinBounds_t1755115333;
// MainButtons
struct MainButtons_t1024182120;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.UI.Slider
struct Slider_t1468074762;
// UnityEngine.UI.Toggle
struct Toggle_t1499417981;
// MainMenu
struct MainMenu_t55996120;
// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t3626201768;
// OnCollisionDie
struct OnCollisionDie_t1988392845;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1217738301;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t594472611;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// OrbitCamera
struct OrbitCamera_t2298935279;
// Parallaxing
struct Parallaxing_t2299696843;
// UnityEngine.UI.Text
struct Text_t3286458198;
// PauseButton
struct PauseButton_t3671462056;
// UnityEngine.GUITexture
struct GUITexture_t63494093;
// PetsFollow
struct PetsFollow_t690634789;
// Player_Movement
struct Player_Movement_t3938410573;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// PShoot
struct PShoot_t2370192815;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t262790558;
// PowerUpEffect
struct PowerUpEffect_t2596559889;
// PowerUpMove
struct PowerUpMove_t2161606225;
// ScrollBG
struct ScrollBG_t3957445618;
// SeaShellMovement
struct SeaShellMovement_t4149019408;
// SeaSpikyMovement
struct SeaSpikyMovement_t463284986;
// ShieldUvAnimation
struct ShieldUvAnimation_t644239002;
// UnityEngine.Renderer
struct Renderer_t1092684080;
// sniperBulletFly
struct sniperBulletFly_t3858882530;
// SpawnCoins
struct SpawnCoins_t3299938663;
// SpawnEnemies
struct SpawnEnemies_t3297838667;
// SpawnEnemies2
struct SpawnEnemies2_t3448750919;
// TestParticles
struct TestParticles_t3963318811;
// TryAgain
struct TryAgain_t1990705797;
// UnityStandardAssets.Utility.SmoothFollow
struct SmoothFollow_t1823522624;
// yyo
struct yyo_t120143;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_boomFollowEnemy3351201388.h"
#include "AssemblyU2DCSharp_boomFollowEnemy3351201388MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharp_boomFollowPlayer1120993405.h"
#include "AssemblyU2DCSharp_boomFollowPlayer1120993405MethodDeclarations.h"
#include "AssemblyU2DCSharp_BulletFly3267336177.h"
#include "AssemblyU2DCSharp_BulletFly3267336177MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"
#include "AssemblyU2DCSharp_Checkpointmove399268217.h"
#include "AssemblyU2DCSharp_Checkpointmove399268217MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"
#include "AssemblyU2DCSharp_CoinsCollision1324380528.h"
#include "AssemblyU2DCSharp_CoinsCollision1324380528MethodDeclarations.h"
#include "AssemblyU2DCSharp_DestroyOutOFBounds2076988544.h"
#include "AssemblyU2DCSharp_DestroyOutOFBounds2076988544MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_EnBulletFly3690094920.h"
#include "AssemblyU2DCSharp_EnBulletFly3690094920MethodDeclarations.h"
#include "AssemblyU2DCSharp_Parameters2452200970.h"
#include "AssemblyU2DCSharp_EnBulletFly42723792876.h"
#include "AssemblyU2DCSharp_EnBulletFly42723792876MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnBulletFly4a2833200629.h"
#include "AssemblyU2DCSharp_EnBulletFly4a2833200629MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnemyMovement1797938231.h"
#include "AssemblyU2DCSharp_EnemyMovement1797938231MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnemyMovement24196477659.h"
#include "AssemblyU2DCSharp_EnemyMovement24196477659MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnemyMovement34196477660.h"
#include "AssemblyU2DCSharp_EnemyMovement34196477660MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator792326996MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharp_EnemyMovement44196477661.h"
#include "AssemblyU2DCSharp_EnemyMovement44196477661MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnemyMovement5a1241788739.h"
#include "AssemblyU2DCSharp_EnemyMovement5a1241788739MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnShoot42311894.h"
#include "AssemblyU2DCSharp_EnShoot42311894MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnShoot21311668764.h"
#include "AssemblyU2DCSharp_EnShoot21311668764MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnShoot41311668766.h"
#include "AssemblyU2DCSharp_EnShoot41311668766MethodDeclarations.h"
#include "AssemblyU2DCSharp_FrostEffect3874425173.h"
#include "AssemblyU2DCSharp_FrostEffect3874425173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material1886596500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3998140498.h"
#include "UnityEngine_UnityEngine_Material1886596500.h"
#include "UnityEngine_UnityEngine_Texture2D2509538522.h"
#include "UnityEngine_UnityEngine_Texture1769722184.h"
#include "UnityEngine_UnityEngine_RenderTexture12905170.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Graphics1688236792MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoldCoinMovement1915910912.h"
#include "AssemblyU2DCSharp_GoldCoinMovement1915910912MethodDeclarations.h"
#include "AssemblyU2DCSharp_KeepCoffinBounded702131559.h"
#include "AssemblyU2DCSharp_KeepCoffinBounded702131559MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "AssemblyU2DCSharp_KeepEnemyBounded255541818.h"
#include "AssemblyU2DCSharp_KeepEnemyBounded255541818MethodDeclarations.h"
#include "AssemblyU2DCSharp_KeepWithinBounds1755115333.h"
#include "AssemblyU2DCSharp_KeepWithinBounds1755115333MethodDeclarations.h"
#include "AssemblyU2DCSharp_MainButtons1024182120.h"
#include "AssemblyU2DCSharp_MainButtons1024182120MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3733964924MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioListener1735598807MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle1499417981MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle1499417981.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider1468074762.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider1468074762MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource3628549054.h"
#include "UnityEngine_UnityEngine_AudioClip3714538611.h"
#include "UnityEngine_UnityEngine_AudioSource3628549054MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag1523288937MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3374395064.h"
#include "AssemblyU2DCSharp_MainMenu55996120.h"
#include "AssemblyU2DCSharp_MainMenu55996120MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasRenderer3626201768MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasRenderer3626201768.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic933884113MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic933884113.h"
#include "AssemblyU2DCSharp_OnCollisionDie1988392845.h"
#include "AssemblyU2DCSharp_OnCollisionDie1988392845MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_MeshRenderer1217738301.h"
#include "UnityEngine_UnityEngine_CircleCollider2D594472611.h"
#include "AssemblyU2DCSharp_OrbitCamera2298935279.h"
#include "AssemblyU2DCSharp_OrbitCamera2298935279MethodDeclarations.h"
#include "AssemblyU2DCSharp_Parallaxing2299696843.h"
#include "AssemblyU2DCSharp_Parallaxing2299696843MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_Parameters2452200970MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "AssemblyU2DCSharp_PauseButton3671462056.h"
#include "AssemblyU2DCSharp_PauseButton3671462056MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITexture63494093.h"
#include "AssemblyU2DCSharp_PetsFollow690634789.h"
#include "AssemblyU2DCSharp_PetsFollow690634789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2223784725MethodDeclarations.h"
#include "AssemblyU2DCSharp_Player_Movement3938410573.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2223784725.h"
#include "AssemblyU2DCSharp_PShoot2370192815.h"
#include "AssemblyU2DCSharp_Player_Movement3938410573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884.h"
#include "UnityEngine_UnityEngine_TouchPhase1905076713.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2D262790558.h"
#include "AssemblyU2DCSharp_PowerUpEffect2596559889.h"
#include "AssemblyU2DCSharp_PowerUpEffect2596559889MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharp_PowerUpMove2161606225.h"
#include "AssemblyU2DCSharp_PowerUpMove2161606225MethodDeclarations.h"
#include "AssemblyU2DCSharp_PShoot2370192815MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScrollBG3957445618.h"
#include "AssemblyU2DCSharp_ScrollBG3957445618MethodDeclarations.h"
#include "AssemblyU2DCSharp_SeaShellMovement4149019408.h"
#include "AssemblyU2DCSharp_SeaShellMovement4149019408MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"
#include "AssemblyU2DCSharp_SeaSpikyMovement463284986.h"
#include "AssemblyU2DCSharp_SeaSpikyMovement463284986MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShieldUvAnimation644239002.h"
#include "AssemblyU2DCSharp_ShieldUvAnimation644239002MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080.h"
#include "AssemblyU2DCSharp_sniperBulletFly3858882530.h"
#include "AssemblyU2DCSharp_sniperBulletFly3858882530MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpawnCoins3299938663.h"
#include "AssemblyU2DCSharp_SpawnCoins3299938663MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "AssemblyU2DCSharp_SpawnEnemies3297838667.h"
#include "AssemblyU2DCSharp_SpawnEnemies3297838667MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpawnEnemies23448750919.h"
#include "AssemblyU2DCSharp_SpawnEnemies23448750919MethodDeclarations.h"
#include "AssemblyU2DCSharp_TestParticles3963318811.h"
#include "AssemblyU2DCSharp_TestParticles3963318811MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction999919624MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction999919624.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_TryAgain1990705797.h"
#include "AssemblyU2DCSharp_TryAgain1990705797MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Smoo1823522624.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Smoo1823522624MethodDeclarations.h"
#include "AssemblyU2DCSharp_yyo120143.h"
#include "AssemblyU2DCSharp_yyo120143MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t284553113_m811718087(__this, method) ((  Transform_t284553113 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Parameters>()
#define GameObject_GetComponent_TisParameters_t2452200970_m1256535827(__this, method) ((  Parameters_t2452200970 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, method) ((  Animator_t792326996 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t792326996_m2581074431(__this, method) ((  Animator_t792326996 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151(__this, method) ((  AudioSource_t3628549054 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t3354615620_m2140199269(__this, method) ((  Image_t3354615620 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.CanvasRenderer>()
#define GameObject_GetComponent_TisCanvasRenderer_t3626201768_m3097889003(__this, method) ((  CanvasRenderer_t3626201768 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(__this, method) ((  MeshRenderer_t1217738301 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.CircleCollider2D>()
#define GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224(__this, method) ((  CircleCollider2D_t594472611 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t1217738301_m3731621022(__this, method) ((  MeshRenderer_t1217738301 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t3286458198_m202917489(__this, method) ((  Text_t3286458198 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.GUITexture>()
#define GameObject_GetComponent_TisGUITexture_t63494093_m3829342502(__this, method) ((  GUITexture_t63494093 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Player_Movement>()
#define GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356(__this, method) ((  Player_Movement_t3938410573 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113(__this, method) ((  SpriteRenderer_t2223784725 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PShoot>()
#define GameObject_GetComponent_TisPShoot_t2370192815_m1940435022(__this, method) ((  PShoot_t2370192815 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<FrostEffect>()
#define GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324(__this, method) ((  FrostEffect_t3874425173 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider2D>()
#define GameObject_GetComponent_TisBoxCollider2D_t262790558_m262378119(__this, method) ((  BoxCollider2D_t262790558 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t1092684080_m4102086307(__this, method) ((  Renderer_t1092684080 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519(__this, method) ((  BoxCollider2D_t262790558 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t4012695102_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void boomFollowEnemy::.ctor()
extern "C"  void boomFollowEnemy__ctor_m1966275199 (boomFollowEnemy_t3351201388 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void boomFollowEnemy::Update()
extern Il2CppCodeGenString* _stringLiteral67100520;
extern const uint32_t boomFollowEnemy_Update_m2551851118_MetadataUsageId;
extern "C"  void boomFollowEnemy_Update_m2551851118 (boomFollowEnemy_t3351201388 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (boomFollowEnemy_Update_m2551851118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral67100520, /*hidden argument*/NULL);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005c;
		}
	}
	{
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral67100520, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t284553113 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, (1.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3111394108(L_2, L_7, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_8, (0.05f), /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void boomFollowPlayer::.ctor()
extern "C"  void boomFollowPlayer__ctor_m2290115262 (boomFollowPlayer_t1120993405 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void boomFollowPlayer::Start()
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t boomFollowPlayer_Start_m1237253054_MetadataUsageId;
extern "C"  void boomFollowPlayer_Start_m1237253054 (boomFollowPlayer_t1120993405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (boomFollowPlayer_Start_m1237253054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		__this->set_Player_2(L_0);
		return;
	}
}
// System.Void boomFollowPlayer::Update()
extern "C"  void boomFollowPlayer_Update_m4000958479 (boomFollowPlayer_t1120993405 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_Player_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0055;
		}
	}
	{
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = __this->get_Player_2();
		NullCheck(L_3);
		Transform_t284553113 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, (-0.8f), (-0.8f), (1.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3111394108(L_2, L_7, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_8, (0.3f), /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void BulletFly::.ctor()
extern "C"  void BulletFly__ctor_m2887310490 (BulletFly_t3267336177 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BulletFly::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const uint32_t BulletFly_Start_m1834448282_MetadataUsageId;
extern "C"  void BulletFly_Start_m1834448282 (BulletFly_t3267336177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BulletFly_Start_m1834448282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_0);
		Transform_t284553113 * L_1 = __this->get__trans_2();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_2);
		__this->set_moveSpeed_4((5.0f));
		return;
	}
}
// System.Void BulletFly::Update()
extern "C"  void BulletFly_Update_m1039174067 (BulletFly_t3267336177 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_2();
		Vector3_t3525329789 * L_1 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3-(float)((float)((float)((float)((float)L_4*(float)L_5))*(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__StartPos_3();
		float L_9 = L_8->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of__StartPos_3();
		float L_11 = L_10->get_z_3();
		Vector3_t3525329789  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, L_7, L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BulletFly::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppCodeGenString* _stringLiteral3721122659;
extern Il2CppCodeGenString* _stringLiteral67100520;
extern const uint32_t BulletFly_OnTriggerEnter2D_m3331571230_MetadataUsageId;
extern "C"  void BulletFly_OnTriggerEnter2D_m3331571230 (BulletFly_t3267336177 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BulletFly_OnTriggerEnter2D_m3331571230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		Collider2D_t1890038195 * L_2 = ___target;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m305486283(L_2, _stringLiteral67100520, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Checkpointmove::.ctor()
extern "C"  void Checkpointmove__ctor_m1389628226 (Checkpointmove_t399268217 * __this, const MethodInfo* method)
{
	{
		__this->set_lerpTime_2((1.0f));
		__this->set_moveDistance_4((10.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Checkpointmove::Start()
extern "C"  void Checkpointmove_Start_m336766018 (Checkpointmove_t399268217 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		__this->set_startPos_5(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Transform_t284553113 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_up_m297874561(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_moveDistance_4();
		Vector3_t3525329789  L_7 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_3, L_7, /*hidden argument*/NULL);
		__this->set_endPos_6(L_8);
		return;
	}
}
// System.Void Checkpointmove::Update()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t Checkpointmove_Update_m1855664139_MetadataUsageId;
extern "C"  void Checkpointmove_Update_m1855664139 (Checkpointmove_t399268217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Checkpointmove_Update_m1855664139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		__this->set_currentLerpTime_3((0.0f));
	}

IL_0017:
	{
		float L_1 = __this->get_currentLerpTime_3();
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentLerpTime_3(((float)((float)L_1+(float)L_2)));
		float L_3 = __this->get_currentLerpTime_3();
		float L_4 = __this->get_lerpTime_2();
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0046;
		}
	}
	{
		float L_5 = __this->get_lerpTime_2();
		__this->set_currentLerpTime_3(L_5);
	}

IL_0046:
	{
		float L_6 = __this->get_currentLerpTime_3();
		float L_7 = __this->get_lerpTime_2();
		V_0 = ((float)((float)L_6/(float)L_7));
		Transform_t284553113 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_9 = __this->get_startPos_5();
		Vector3_t3525329789  L_10 = __this->get_endPos_6();
		float L_11 = V_0;
		Vector3_t3525329789  L_12 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_position_m3111394108(L_8, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CoinsCollision::.ctor()
extern "C"  void CoinsCollision__ctor_m808843563 (CoinsCollision_t1324380528 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CoinsCollision::Start()
extern "C"  void CoinsCollision_Start_m4050948651 (CoinsCollision_t1324380528 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DestroyOutOFBounds::.ctor()
extern "C"  void DestroyOutOFBounds__ctor_m531676187 (DestroyOutOFBounds_t2076988544 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyOutOFBounds::Start()
extern "C"  void DestroyOutOFBounds_Start_m3773781275 (DestroyOutOFBounds_t2076988544 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DestroyOutOFBounds::Update()
extern Il2CppCodeGenString* _stringLiteral1007614042;
extern const uint32_t DestroyOutOFBounds_Update_m1028954706_MetadataUsageId;
extern "C"  void DestroyOutOFBounds_Update_m1028954706 (DestroyOutOFBounds_t2076988544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DestroyOutOFBounds_Update_m1028954706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1007614042, (3.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyOutOFBounds::DieOutOfBounds()
extern "C"  void DestroyOutOFBounds_DieOutOfBounds_m3512348035 (DestroyOutOFBounds_t2076988544 * __this, const MethodInfo* method)
{
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t3533968274 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_3 = Camera_WorldToScreenPoint_m2400233676(L_0, L_2, /*hidden argument*/NULL);
		Vector2_t3525329788  L_4 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_y_2();
		int32_t L_6 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_5) > ((float)(((float)((float)((int32_t)((int32_t)L_6+(int32_t)((int32_t)10)))))))))
		{
			goto IL_0067;
		}
	}
	{
		float L_7 = (&V_0)->get_y_2();
		if ((((float)L_7) < ((float)(-10.0f))))
		{
			goto IL_0067;
		}
	}
	{
		float L_8 = (&V_0)->get_x_1();
		int32_t L_9 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_8) > ((float)(((float)((float)((int32_t)((int32_t)L_9+(int32_t)((int32_t)10)))))))))
		{
			goto IL_0067;
		}
	}
	{
		float L_10 = (&V_0)->get_x_1();
		if ((!(((float)L_10) < ((float)(-10.0f)))))
		{
			goto IL_0072;
		}
	}

IL_0067:
	{
		GameObject_t4012695102 * L_11 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void EnBulletFly::.ctor()
extern "C"  void EnBulletFly__ctor_m1342049059 (EnBulletFly_t3690094920 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnBulletFly::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t EnBulletFly_Start_m289186851_MetadataUsageId;
extern "C"  void EnBulletFly_Start_m289186851 (EnBulletFly_t3690094920 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnBulletFly_Start_m289186851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_6(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_6();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_7(L_2);
		Parameters_t2452200970 * L_3 = __this->get_parameters_7();
		NullCheck(L_3);
		float L_4 = L_3->get_enemyBulletSpeed_6();
		__this->set_moveSpeed_4(L_4);
		Transform_t284553113 * L_5 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_5);
		Transform_t284553113 * L_6 = __this->get__trans_2();
		NullCheck(L_6);
		Vector3_t3525329789  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_7);
		return;
	}
}
// System.Void EnBulletFly::Update()
extern "C"  void EnBulletFly_Update_m380709962 (EnBulletFly_t3690094920 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_2();
		Vector3_t3525329789 * L_1 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))*(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__StartPos_3();
		float L_9 = L_8->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of__StartPos_3();
		float L_11 = L_10->get_z_3();
		Vector3_t3525329789  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, L_7, L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnBulletFly::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppCodeGenString* _stringLiteral1260913290;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2475494281;
extern const uint32_t EnBulletFly_OnTriggerEnter2D_m1586250933_MetadataUsageId;
extern "C"  void EnBulletFly_OnTriggerEnter2D_m1586250933 (EnBulletFly_t3690094920 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnBulletFly_OnTriggerEnter2D_m1586250933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral1260913290, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Collider2D_t1890038195 * L_2 = ___target;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m305486283(L_2, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_004a;
		}
	}

IL_0025:
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_CompareTag_m3153977471(L_4, _stringLiteral1260913290, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		Collider2D_t1890038195 * L_6 = ___target;
		NullCheck(L_6);
		bool L_7 = Component_CompareTag_m305486283(L_6, _stringLiteral2475494281, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0055;
		}
	}

IL_004a:
	{
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void EnBulletFly4::.ctor()
extern "C"  void EnBulletFly4__ctor_m3291632687 (EnBulletFly4_t2723792876 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnBulletFly4::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t EnBulletFly4_Start_m2238770479_MetadataUsageId;
extern "C"  void EnBulletFly4_Start_m2238770479 (EnBulletFly4_t2723792876 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnBulletFly4_Start_m2238770479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_6(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_6();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_7(L_2);
		Parameters_t2452200970 * L_3 = __this->get_parameters_7();
		NullCheck(L_3);
		float L_4 = L_3->get_enemyBulletSpeed_6();
		__this->set_moveSpeed_4(L_4);
		Transform_t284553113 * L_5 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_5);
		Transform_t284553113 * L_6 = __this->get__trans_2();
		NullCheck(L_6);
		Vector3_t3525329789  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_7);
		return;
	}
}
// System.Void EnBulletFly4::Update()
extern "C"  void EnBulletFly4_Update_m688260286 (EnBulletFly4_t2723792876 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_2();
		Vector3_t3525329789 * L_1 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))*(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_9 = L_8;
		float L_10 = L_9->get_y_2();
		float L_11 = __this->get_moveSpeed_4();
		float L_12 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = ((float)((float)L_10+(float)((float)((float)L_11*(float)L_12))));
		V_0 = L_13;
		L_9->set_y_2(L_13);
		float L_14 = V_0;
		Vector3_t3525329789 * L_15 = __this->get_address_of__StartPos_3();
		float L_16 = L_15->get_z_3();
		Vector3_t3525329789  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2926210380(&L_17, L_7, L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnBulletFly4::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppCodeGenString* _stringLiteral1260913290;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2475494281;
extern const uint32_t EnBulletFly4_OnTriggerEnter2D_m759319849_MetadataUsageId;
extern "C"  void EnBulletFly4_OnTriggerEnter2D_m759319849 (EnBulletFly4_t2723792876 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnBulletFly4_OnTriggerEnter2D_m759319849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral1260913290, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Collider2D_t1890038195 * L_2 = ___target;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m305486283(L_2, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_004a;
		}
	}

IL_0025:
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_CompareTag_m3153977471(L_4, _stringLiteral1260913290, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		Collider2D_t1890038195 * L_6 = ___target;
		NullCheck(L_6);
		bool L_7 = Component_CompareTag_m305486283(L_6, _stringLiteral2475494281, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0055;
		}
	}

IL_004a:
	{
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void EnBulletFly4a::.ctor()
extern "C"  void EnBulletFly4a__ctor_m3346009878 (EnBulletFly4a_t2833200629 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnBulletFly4a::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t EnBulletFly4a_Start_m2293147670_MetadataUsageId;
extern "C"  void EnBulletFly4a_Start_m2293147670 (EnBulletFly4a_t2833200629 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnBulletFly4a_Start_m2293147670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_6(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_6();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_7(L_2);
		Parameters_t2452200970 * L_3 = __this->get_parameters_7();
		NullCheck(L_3);
		float L_4 = L_3->get_enemyBulletSpeed_6();
		__this->set_moveSpeed_4(L_4);
		Transform_t284553113 * L_5 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_5);
		Transform_t284553113 * L_6 = __this->get__trans_2();
		NullCheck(L_6);
		Vector3_t3525329789  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_7);
		return;
	}
}
// System.Void EnBulletFly4a::Update()
extern "C"  void EnBulletFly4a_Update_m2373953207 (EnBulletFly4a_t2833200629 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_2();
		Vector3_t3525329789 * L_1 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))*(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_9 = L_8;
		float L_10 = L_9->get_y_2();
		float L_11 = __this->get_moveSpeed_4();
		float L_12 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = ((float)((float)L_10-(float)((float)((float)L_11*(float)L_12))));
		V_0 = L_13;
		L_9->set_y_2(L_13);
		float L_14 = V_0;
		Vector3_t3525329789 * L_15 = __this->get_address_of__StartPos_3();
		float L_16 = L_15->get_z_3();
		Vector3_t3525329789  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2926210380(&L_17, L_7, L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnBulletFly4a::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppCodeGenString* _stringLiteral1260913290;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2475494281;
extern const uint32_t EnBulletFly4a_OnTriggerEnter2D_m2177747746_MetadataUsageId;
extern "C"  void EnBulletFly4a_OnTriggerEnter2D_m2177747746 (EnBulletFly4a_t2833200629 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnBulletFly4a_OnTriggerEnter2D_m2177747746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral1260913290, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Collider2D_t1890038195 * L_2 = ___target;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m305486283(L_2, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_004a;
		}
	}

IL_0025:
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_CompareTag_m3153977471(L_4, _stringLiteral1260913290, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		Collider2D_t1890038195 * L_6 = ___target;
		NullCheck(L_6);
		bool L_7 = Component_CompareTag_m305486283(L_6, _stringLiteral2475494281, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0055;
		}
	}

IL_004a:
	{
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void EnemyMovement::.ctor()
extern "C"  void EnemyMovement__ctor_m1262408468 (EnemyMovement_t1797938231 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t EnemyMovement_Start_m209546260_MetadataUsageId;
extern "C"  void EnemyMovement_Start_m209546260 (EnemyMovement_t1797938231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement_Start_m209546260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Parameters_t2452200970 * V_1 = NULL;
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_3(L_0);
		Transform_t284553113 * L_1 = __this->get__trans_3();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set__startingPos_2(L_2);
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t4012695102 * L_4 = V_0;
		NullCheck(L_4);
		Parameters_t2452200970 * L_5 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_4, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		V_1 = L_5;
		Parameters_t2452200970 * L_6 = V_1;
		NullCheck(L_6);
		float L_7 = L_6->get_enemySpeed_8();
		__this->set_moveSpeed_4(L_7);
		return;
	}
}
// System.Void EnemyMovement::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t EnemyMovement_Update_m2206818937_MetadataUsageId;
extern "C"  void EnemyMovement_Update_m2206818937 (EnemyMovement_t1797938231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement_Update_m2206818937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_3();
		Vector3_t3525329789 * L_1 = __this->get_address_of__startingPos_2();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))/(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__startingPos_2();
		float L_9 = L_8->get_y_2();
		float L_10 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_11 = Mathf_PingPong_m763741129(NULL /*static, unused*/, L_10, (2.0f), /*hidden argument*/NULL);
		Vector3_t3525329789 * L_12 = __this->get_address_of__startingPos_2();
		float L_13 = L_12->get_z_3();
		Vector3_t3525329789  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2926210380(&L_14, L_7, ((float)((float)L_9+(float)L_11)), L_13, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement2::.ctor()
extern "C"  void EnemyMovement2__ctor_m1215801376 (EnemyMovement2_t4196477659 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement2::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t EnemyMovement2_Start_m162939168_MetadataUsageId;
extern "C"  void EnemyMovement2_Start_m162939168 (EnemyMovement2_t4196477659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement2_Start_m162939168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Parameters_t2452200970 * V_1 = NULL;
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_0);
		Transform_t284553113 * L_1 = __this->get__trans_2();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_2);
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t4012695102 * L_4 = V_0;
		NullCheck(L_4);
		Parameters_t2452200970 * L_5 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_4, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		V_1 = L_5;
		Parameters_t2452200970 * L_6 = V_1;
		NullCheck(L_6);
		float L_7 = L_6->get_enemySpeed_8();
		__this->set_moveSpeed_4(L_7);
		return;
	}
}
// System.Void EnemyMovement2::Update()
extern "C"  void EnemyMovement2_Update_m761999085 (EnemyMovement2_t4196477659 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_2();
		Vector3_t3525329789 * L_1 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))/(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__StartPos_3();
		float L_9 = L_8->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of__StartPos_3();
		float L_11 = L_10->get_z_3();
		Vector3_t3525329789  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, L_7, L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement3::.ctor()
extern "C"  void EnemyMovement3__ctor_m1019287871 (EnemyMovement3_t4196477660 * __this, const MethodInfo* method)
{
	{
		__this->set_explodeOnce_8((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement3::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t EnemyMovement3_Start_m4261392959_MetadataUsageId;
extern "C"  void EnemyMovement3_Start_m4261392959 (EnemyMovement3_t4196477660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement3_Start_m4261392959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Parameters_t2452200970 * V_1 = NULL;
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_0);
		Transform_t284553113 * L_1 = __this->get__trans_2();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_2);
		GameObject_t4012695102 * L_3 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		__this->set_player_5(L_3);
		Animator_t792326996 * L_4 = Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var);
		__this->set_ani_6(L_4);
		Animator_t792326996 * L_5 = __this->get_ani_6();
		NullCheck(L_5);
		Animator_set_speed_m2513936029(L_5, (0.3f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_6 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t4012695102 * L_7 = V_0;
		NullCheck(L_7);
		Parameters_t2452200970 * L_8 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_7, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		V_1 = L_8;
		Parameters_t2452200970 * L_9 = V_1;
		NullCheck(L_9);
		float L_10 = L_9->get_enemySpeed_8();
		__this->set_moveSpeed_4(L_10);
		return;
	}
}
// System.Void EnemyMovement3::Update()
extern Il2CppCodeGenString* _stringLiteral2985818337;
extern Il2CppCodeGenString* _stringLiteral1986761522;
extern const uint32_t EnemyMovement3_Update_m3260015022_MetadataUsageId;
extern "C"  void EnemyMovement3_Update_m3260015022 (EnemyMovement3_t4196477660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement3_Update_m3260015022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		GameObject_t4012695102 * L_0 = __this->get_player_5();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0073;
		}
	}
	{
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_3);
		Transform_t284553113 * L_4 = __this->get__trans_2();
		Vector3_t3525329789 * L_5 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_6 = L_5;
		float L_7 = L_6->get_x_1();
		float L_8 = __this->get_moveSpeed_4();
		float L_9 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = ((float)((float)L_7+(float)((float)((float)((float)((float)L_8*(float)L_9))*(float)(1.5f)))));
		V_2 = L_10;
		L_6->set_x_1(L_10);
		float L_11 = V_2;
		Vector3_t3525329789 * L_12 = __this->get_address_of__StartPos_3();
		float L_13 = L_12->get_y_2();
		Vector3_t3525329789 * L_14 = __this->get_address_of__StartPos_3();
		float L_15 = L_14->get_z_3();
		Vector3_t3525329789  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2926210380(&L_16, L_11, L_13, L_15, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m3111394108(L_4, L_16, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_0073:
	{
		GameObject_t4012695102 * L_17 = __this->get_player_5();
		NullCheck(L_17);
		Transform_t284553113 * L_18 = GameObject_get_transform_m1278640159(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t3525329789  L_19 = Transform_get_position_m2211398607(L_18, /*hidden argument*/NULL);
		Transform_t284553113 * L_20 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t3525329789  L_21 = Transform_get_position_m2211398607(L_20, /*hidden argument*/NULL);
		float L_22 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		float L_23 = V_0;
		if ((!(((float)L_23) < ((float)(100.0f)))))
		{
			goto IL_0140;
		}
	}
	{
		GameObject_t4012695102 * L_24 = __this->get_player_5();
		NullCheck(L_24);
		Transform_t284553113 * L_25 = GameObject_get_transform_m1278640159(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t3525329789  L_26 = Transform_get_position_m2211398607(L_25, /*hidden argument*/NULL);
		Transform_t284553113 * L_27 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t3525329789  L_28 = Transform_get_position_m2211398607(L_27, /*hidden argument*/NULL);
		Vector3_t3525329789  L_29 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		V_1 = L_29;
		Vector3_Normalize_m3984983796((&V_1), /*hidden argument*/NULL);
		Transform_t284553113 * L_30 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_31 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t3525329789  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		Vector3_t3525329789  L_33 = V_1;
		float L_34 = __this->get_moveSpeed_4();
		Vector3_t3525329789  L_35 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		float L_36 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_37 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t3525329789  L_38 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_37, (1.3f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_39 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_32, L_38, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_position_m3111394108(L_30, L_39, /*hidden argument*/NULL);
		float L_40 = V_0;
		if ((!(((float)L_40) < ((float)(10.0f)))))
		{
			goto IL_0140;
		}
	}
	{
		bool L_41 = __this->get_explodeOnce_8();
		if (!L_41)
		{
			goto IL_0140;
		}
	}
	{
		Animator_t792326996 * L_42 = __this->get_ani_6();
		NullCheck(L_42);
		Animator_SetBool_m2336836203(L_42, _stringLiteral2985818337, (bool)1, /*hidden argument*/NULL);
		__this->set_explodeOnce_8((bool)0);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1986761522, (2.5f), /*hidden argument*/NULL);
	}

IL_0140:
	{
		return;
	}
}
// System.Void EnemyMovement3::destroyMe()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1221705904;
extern const uint32_t EnemyMovement3_destroyMe_m2578077487_MetadataUsageId;
extern "C"  void EnemyMovement3_destroyMe_m2578077487 (EnemyMovement3_t4196477660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement3_destroyMe_m2578077487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral1221705904, L_1, /*hidden argument*/NULL);
		Transform_t284553113 * L_3 = __this->get__trans_2();
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_5 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_6 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_2, L_4, L_5, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)CastclassSealed(L_6, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_7 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_7, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement4::.ctor()
extern "C"  void EnemyMovement4__ctor_m822774366 (EnemyMovement4_t4196477661 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement4::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t EnemyMovement4_Start_m4064879454_MetadataUsageId;
extern "C"  void EnemyMovement4_Start_m4064879454 (EnemyMovement4_t4196477661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement4_Start_m4064879454_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Parameters_t2452200970 * V_1 = NULL;
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_3(L_0);
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set_StartingPos_4(L_2);
		int32_t L_3 = Random_Range_m75452833(NULL /*static, unused*/, 1, 7, /*hidden argument*/NULL);
		__this->set_randomPos_5((((float)((float)L_3))));
		GameObject_t4012695102 * L_4 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t4012695102 * L_5 = V_0;
		NullCheck(L_5);
		Parameters_t2452200970 * L_6 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_5, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		V_1 = L_6;
		Parameters_t2452200970 * L_7 = V_1;
		NullCheck(L_7);
		float L_8 = L_7->get_enemySpeed_8();
		__this->set_moveSpeed_2(L_8);
		return;
	}
}
// System.Void EnemyMovement4::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t EnemyMovement4_Update_m1463063663_MetadataUsageId;
extern "C"  void EnemyMovement4_Update_m1463063663 (EnemyMovement4_t4196477661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement4_Update_m1463063663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_3();
		Vector3_t3525329789 * L_1 = __this->get_address_of_StartingPos_4();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_2();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))/(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of_StartingPos_4();
		float L_9 = L_8->get_y_2();
		float L_10 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = __this->get_randomPos_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_12 = Mathf_PingPong_m763741129(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Vector3_t3525329789 * L_13 = __this->get_address_of_StartingPos_4();
		float L_14 = L_13->get_z_3();
		Vector3_t3525329789  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector3__ctor_m2926210380(&L_15, L_7, ((float)((float)L_9+(float)L_12)), L_14, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement5a::.ctor()
extern "C"  void EnemyMovement5a__ctor_m2323861896 (EnemyMovement5a_t1241788739 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement5a::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t EnemyMovement5a_Start_m1270999688_MetadataUsageId;
extern "C"  void EnemyMovement5a_Start_m1270999688 (EnemyMovement5a_t1241788739 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement5a_Start_m1270999688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Parameters_t2452200970 * V_1 = NULL;
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_0);
		Transform_t284553113 * L_1 = __this->get__trans_2();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_2);
		float L_3 = Random_Range_m3362417303(NULL /*static, unused*/, (0.5f), (3.0f), /*hidden argument*/NULL);
		__this->set_randMove_5(L_3);
		GameObject_t4012695102 * L_4 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t4012695102 * L_5 = V_0;
		NullCheck(L_5);
		Parameters_t2452200970 * L_6 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_5, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		V_1 = L_6;
		Parameters_t2452200970 * L_7 = V_1;
		NullCheck(L_7);
		float L_8 = L_7->get_enemySpeed_8();
		__this->set_moveSpeed_4(L_8);
		return;
	}
}
// System.Void EnemyMovement5a::Update()
extern "C"  void EnemyMovement5a_Update_m752136837 (EnemyMovement5a_t1241788739 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_2();
		Vector3_t3525329789 * L_1 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = __this->get_randMove_5();
		float L_6 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = ((float)((float)L_3+(float)((float)((float)((float)((float)((float)((float)L_4*(float)L_5))*(float)L_6))/(float)(2.0f)))));
		V_0 = L_7;
		L_2->set_x_1(L_7);
		float L_8 = V_0;
		Vector3_t3525329789 * L_9 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_10 = L_9;
		float L_11 = L_10->get_y_2();
		float L_12 = __this->get_moveSpeed_4();
		float L_13 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = ((float)((float)L_11+(float)((float)((float)((float)((float)L_12*(float)L_13))/(float)(2.0f)))));
		V_0 = L_14;
		L_10->set_y_2(L_14);
		float L_15 = V_0;
		Vector3_t3525329789 * L_16 = __this->get_address_of__StartPos_3();
		float L_17 = L_16->get_z_3();
		Vector3_t3525329789  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, L_8, L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot::.ctor()
extern "C"  void EnShoot__ctor_m3512475477 (EnShoot_t42311894 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern Il2CppCodeGenString* _stringLiteral79860735;
extern const uint32_t EnShoot_Start_m2459613269_MetadataUsageId;
extern "C"  void EnShoot_Start_m2459613269 (EnShoot_t42311894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnShoot_Start_m2459613269_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_5(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_5();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_6(L_2);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral79860735, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot::Update()
extern "C"  void EnShoot_Update_m3239419480 (EnShoot_t42311894 * __this, const MethodInfo* method)
{
	{
		Parameters_t2452200970 * L_0 = __this->get_parameters_6();
		NullCheck(L_0);
		float L_1 = L_0->get_enemyShootSpeed_11();
		__this->set_moveSpeed_2(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		__this->set__transPos_4(L_3);
		Vector3_t3525329789 * L_4 = __this->get_address_of__transPos_4();
		float L_5 = L_4->get_x_1();
		Vector3_t3525329789 * L_6 = __this->get_address_of__transPos_4();
		float L_7 = L_6->get_y_2();
		Vector3_t3525329789 * L_8 = __this->get_address_of__transPos_4();
		float L_9 = L_8->get_z_3();
		Vector3_t3525329789  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, ((float)((float)L_5+(float)(1.0f))), ((float)((float)L_7-(float)(0.8f))), L_9, /*hidden argument*/NULL);
		__this->set__desiredPos_3(L_10);
		return;
	}
}
// System.Void EnShoot::Shoot()
extern const Il2CppType* Object_t3878351788_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3221256425;
extern Il2CppCodeGenString* _stringLiteral106058036;
extern Il2CppCodeGenString* _stringLiteral79860735;
extern const uint32_t EnShoot_Shoot_m2128903378_MetadataUsageId;
extern "C"  void EnShoot_Shoot_m2128903378 (EnShoot_t42311894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnShoot_Shoot_m2128903378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3221256425, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get__desiredPos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_5 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral106058036, L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = __this->get__desiredPos_3();
		Quaternion_t1891715979  L_7 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_8 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)IsInstSealed(L_8, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_9 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_9, (0.1f), /*hidden argument*/NULL);
		float L_10 = __this->get_moveSpeed_2();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral79860735, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot2::.ctor()
extern "C"  void EnShoot2__ctor_m2248401919 (EnShoot2_t1311668764 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot2::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern Il2CppCodeGenString* _stringLiteral79860735;
extern const uint32_t EnShoot2_Start_m1195539711_MetadataUsageId;
extern "C"  void EnShoot2_Start_m1195539711 (EnShoot2_t1311668764 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnShoot2_Start_m1195539711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_5(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_5();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_6(L_2);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral79860735, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot2::Update()
extern "C"  void EnShoot2_Update_m2707844846 (EnShoot2_t1311668764 * __this, const MethodInfo* method)
{
	{
		Parameters_t2452200970 * L_0 = __this->get_parameters_6();
		NullCheck(L_0);
		float L_1 = L_0->get_enemyShootSpeed_11();
		__this->set_moveSpeed_2(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		__this->set__transPos_4(L_3);
		Vector3_t3525329789 * L_4 = __this->get_address_of__transPos_4();
		float L_5 = L_4->get_x_1();
		Vector3_t3525329789 * L_6 = __this->get_address_of__transPos_4();
		float L_7 = L_6->get_y_2();
		Vector3_t3525329789 * L_8 = __this->get_address_of__transPos_4();
		float L_9 = L_8->get_z_3();
		Vector3_t3525329789  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, ((float)((float)L_5+(float)(1.0f))), ((float)((float)L_7-(float)(0.5f))), L_9, /*hidden argument*/NULL);
		__this->set__desiredPos_3(L_10);
		return;
	}
}
// System.Void EnShoot2::Shoot()
extern const Il2CppType* Object_t3878351788_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3221256426;
extern Il2CppCodeGenString* _stringLiteral106058036;
extern Il2CppCodeGenString* _stringLiteral79860735;
extern const uint32_t EnShoot2_Shoot_m864829820_MetadataUsageId;
extern "C"  void EnShoot2_Shoot_m864829820 (EnShoot2_t1311668764 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnShoot2_Shoot_m864829820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3221256426, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get__desiredPos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_5 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral106058036, L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = __this->get__desiredPos_3();
		Quaternion_t1891715979  L_7 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_8 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)IsInstSealed(L_8, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_9 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_9, (0.1f), /*hidden argument*/NULL);
		float L_10 = __this->get_moveSpeed_2();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral79860735, ((float)((float)L_10+(float)(2.0f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot4::.ctor()
extern "C"  void EnShoot4__ctor_m1855374909 (EnShoot4_t1311668766 * __this, const MethodInfo* method)
{
	{
		__this->set_moveSpeed_2((5.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot4::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern Il2CppCodeGenString* _stringLiteral79860735;
extern const uint32_t EnShoot4_Start_m802512701_MetadataUsageId;
extern "C"  void EnShoot4_Start_m802512701 (EnShoot4_t1311668766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnShoot4_Start_m802512701_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_5(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_5();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_6(L_2);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral79860735, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnShoot4::Update()
extern "C"  void EnShoot4_Update_m3408909424 (EnShoot4_t1311668766 * __this, const MethodInfo* method)
{
	{
		Parameters_t2452200970 * L_0 = __this->get_parameters_6();
		NullCheck(L_0);
		float L_1 = L_0->get_enemyShootSpeed_11();
		__this->set_moveSpeed_2(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		__this->set__transPos_4(L_3);
		Vector3_t3525329789 * L_4 = __this->get_address_of__transPos_4();
		float L_5 = L_4->get_x_1();
		Vector3_t3525329789 * L_6 = __this->get_address_of__transPos_4();
		float L_7 = L_6->get_y_2();
		Vector3_t3525329789 * L_8 = __this->get_address_of__transPos_4();
		float L_9 = L_8->get_z_3();
		Vector3_t3525329789  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, ((float)((float)L_5+(float)(1.0f))), ((float)((float)L_7-(float)(0.5f))), L_9, /*hidden argument*/NULL);
		__this->set__desiredPos_3(L_10);
		return;
	}
}
// System.Void EnShoot4::Shoot()
extern const Il2CppType* Object_t3878351788_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3221256428;
extern Il2CppCodeGenString* _stringLiteral1074701557;
extern Il2CppCodeGenString* _stringLiteral106058036;
extern Il2CppCodeGenString* _stringLiteral79860735;
extern const uint32_t EnShoot4_Shoot_m471802810_MetadataUsageId;
extern "C"  void EnShoot4_Shoot_m471802810 (EnShoot4_t1311668766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnShoot4_Shoot_m471802810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3221256428, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get__desiredPos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_5 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral1074701557, L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = __this->get__desiredPos_3();
		Quaternion_t1891715979  L_7 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_9 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral106058036, L_8, /*hidden argument*/NULL);
		Vector3_t3525329789  L_10 = __this->get__desiredPos_3();
		Quaternion_t1891715979  L_11 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_12 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)IsInstSealed(L_12, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_13 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_13, (0.1f), /*hidden argument*/NULL);
		float L_14 = __this->get_moveSpeed_2();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral79860735, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrostEffect::.ctor()
extern "C"  void FrostEffect__ctor_m2054758838 (FrostEffect_t3874425173 * __this, const MethodInfo* method)
{
	{
		__this->set_FrostAmount_2((0.5f));
		__this->set_EdgeSharpness_3((1.0f));
		__this->set_maxFrost_5((1.0f));
		__this->set_seethroughness_6((0.2f));
		__this->set_distortion_7((0.1f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrostEffect::Awake()
extern TypeInfo* Material_t1886596500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3509194645;
extern Il2CppCodeGenString* _stringLiteral4257613543;
extern const uint32_t FrostEffect_Awake_m2292364057_MetadataUsageId;
extern "C"  void FrostEffect_Awake_m2292364057 (FrostEffect_t3874425173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FrostEffect_Awake_m2292364057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t3998140498 * L_0 = __this->get_Shader_10();
		Material_t1886596500 * L_1 = (Material_t1886596500 *)il2cpp_codegen_object_new(Material_t1886596500_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_1, L_0, /*hidden argument*/NULL);
		__this->set_material_11(L_1);
		Material_t1886596500 * L_2 = __this->get_material_11();
		Texture2D_t2509538522 * L_3 = __this->get_Frost_8();
		NullCheck(L_2);
		Material_SetTexture_m1833724755(L_2, _stringLiteral3509194645, L_3, /*hidden argument*/NULL);
		Material_t1886596500 * L_4 = __this->get_material_11();
		Texture2D_t2509538522 * L_5 = __this->get_FrostNormals_9();
		NullCheck(L_4);
		Material_SetTexture_m1833724755(L_4, _stringLiteral4257613543, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrostEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3509194645;
extern Il2CppCodeGenString* _stringLiteral4257613543;
extern Il2CppCodeGenString* _stringLiteral2376966698;
extern Il2CppCodeGenString* _stringLiteral2127408613;
extern Il2CppCodeGenString* _stringLiteral3271716488;
extern Il2CppCodeGenString* _stringLiteral1523753564;
extern const uint32_t FrostEffect_OnRenderImage_m2495992040_MetadataUsageId;
extern "C"  void FrostEffect_OnRenderImage_m2495992040 (FrostEffect_t3874425173 * __this, RenderTexture_t12905170 * ___source, RenderTexture_t12905170 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FrostEffect_OnRenderImage_m2495992040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_004c;
		}
	}
	{
		Material_t1886596500 * L_1 = __this->get_material_11();
		Texture2D_t2509538522 * L_2 = __this->get_Frost_8();
		NullCheck(L_1);
		Material_SetTexture_m1833724755(L_1, _stringLiteral3509194645, L_2, /*hidden argument*/NULL);
		Material_t1886596500 * L_3 = __this->get_material_11();
		Texture2D_t2509538522 * L_4 = __this->get_FrostNormals_9();
		NullCheck(L_3);
		Material_SetTexture_m1833724755(L_3, _stringLiteral4257613543, L_4, /*hidden argument*/NULL);
		float L_5 = __this->get_EdgeSharpness_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Max_m3923796455(NULL /*static, unused*/, (1.0f), L_5, /*hidden argument*/NULL);
		__this->set_EdgeSharpness_3(L_6);
	}

IL_004c:
	{
		Material_t1886596500 * L_7 = __this->get_material_11();
		float L_8 = __this->get_FrostAmount_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_maxFrost_5();
		float L_11 = __this->get_minFrost_4();
		float L_12 = __this->get_minFrost_4();
		float L_13 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)((float)((float)L_9*(float)((float)((float)L_10-(float)L_11))))+(float)L_12)), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetFloat_m981710063(L_7, _stringLiteral2376966698, L_13, /*hidden argument*/NULL);
		Material_t1886596500 * L_14 = __this->get_material_11();
		float L_15 = __this->get_EdgeSharpness_3();
		NullCheck(L_14);
		Material_SetFloat_m981710063(L_14, _stringLiteral2127408613, L_15, /*hidden argument*/NULL);
		Material_t1886596500 * L_16 = __this->get_material_11();
		float L_17 = __this->get_seethroughness_6();
		NullCheck(L_16);
		Material_SetFloat_m981710063(L_16, _stringLiteral3271716488, L_17, /*hidden argument*/NULL);
		Material_t1886596500 * L_18 = __this->get_material_11();
		float L_19 = __this->get_distortion_7();
		NullCheck(L_18);
		Material_SetFloat_m981710063(L_18, _stringLiteral1523753564, L_19, /*hidden argument*/NULL);
		RenderTexture_t12905170 * L_20 = ___source;
		RenderTexture_t12905170 * L_21 = ___destination;
		Material_t1886596500 * L_22 = __this->get_material_11();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoldCoinMovement::.ctor()
extern "C"  void GoldCoinMovement__ctor_m398265243 (GoldCoinMovement_t1915910912 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoldCoinMovement::Start()
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern Il2CppCodeGenString* _stringLiteral934522521;
extern const uint32_t GoldCoinMovement_Start_m3640370331_MetadataUsageId;
extern "C"  void GoldCoinMovement_Start_m3640370331 (GoldCoinMovement_t1915910912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GoldCoinMovement_Start_m3640370331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_5(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_5();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_6(L_2);
		Object_t3878351788 * L_3 = Resources_Load_m2187391845(NULL /*static, unused*/, _stringLiteral934522521, /*hidden argument*/NULL);
		__this->set_sparkles_7(((GameObject_t4012695102 *)IsInstSealed(L_3, GameObject_t4012695102_il2cpp_TypeInfo_var)));
		Transform_t284553113 * L_4 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_3(L_4);
		Transform_t284553113 * L_5 = __this->get__trans_3();
		NullCheck(L_5);
		Vector3_t3525329789  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		__this->set__startingPos_2(L_6);
		__this->set_moveSpeed_4((4.0f));
		return;
	}
}
// System.Void GoldCoinMovement::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t GoldCoinMovement_Update_m1188182738_MetadataUsageId;
extern "C"  void GoldCoinMovement_Update_m1188182738 (GoldCoinMovement_t1915910912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GoldCoinMovement_Update_m1188182738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_3();
		Vector3_t3525329789 * L_1 = __this->get_address_of__startingPos_2();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))/(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__startingPos_2();
		float L_9 = L_8->get_y_2();
		float L_10 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_11 = Mathf_PingPong_m763741129(NULL /*static, unused*/, L_10, (2.0f), /*hidden argument*/NULL);
		Vector3_t3525329789 * L_12 = __this->get_address_of__startingPos_2();
		float L_13 = L_12->get_z_3();
		Vector3_t3525329789  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2926210380(&L_14, L_7, ((float)((float)L_9+(float)L_11)), L_13, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoldCoinMovement::OnTriggerEnter2D(UnityEngine.Collider2D)
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t GoldCoinMovement_OnTriggerEnter2D_m1905341245_MetadataUsageId;
extern "C"  void GoldCoinMovement_OnTriggerEnter2D_m1905341245 (GoldCoinMovement_t1915910912 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GoldCoinMovement_OnTriggerEnter2D_m1905341245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		Collider2D_t1890038195 * L_0 = ___target;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m305486283(L_0, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		GameObject_t4012695102 * L_2 = __this->get_sparkles_7();
		Transform_t284553113 * L_3 = __this->get__trans_3();
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_5 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_6 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_2, L_4, L_5, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)CastclassSealed(L_6, GameObject_t4012695102_il2cpp_TypeInfo_var));
		Parameters_t2452200970 * L_7 = __this->get_parameters_6();
		Parameters_t2452200970 * L_8 = L_7;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_Coins_13();
		NullCheck(L_8);
		L_8->set_Coins_13(((int32_t)((int32_t)L_9+(int32_t)1)));
		GameObject_t4012695102 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_10, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_11 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_11, (1.0f), /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void KeepCoffinBounded::.ctor()
extern "C"  void KeepCoffinBounded__ctor_m1561928676 (KeepCoffinBounded_t702131559 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KeepCoffinBounded::Start()
extern "C"  void KeepCoffinBounded_Start_m509066468 (KeepCoffinBounded_t702131559 * __this, const MethodInfo* method)
{
	{
		Camera_t3533968274 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_1 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_2 = Camera_ScreenToWorldPoint_m1572306334(L_0, L_1, /*hidden argument*/NULL);
		__this->set_bottomLeft_2(L_2);
		Camera_t3533968274 * L_3 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t3533968274 * L_4 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Camera_get_pixelWidth_m175903141(L_4, /*hidden argument*/NULL);
		Camera_t3533968274 * L_6 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Camera_get_pixelHeight_m1661844426(L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m1846874791(&L_8, (((float)((float)L_5))), (((float)((float)L_7))), /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3525329789  L_9 = Camera_ScreenToWorldPoint_m1572306334(L_3, L_8, /*hidden argument*/NULL);
		__this->set_topRight_3(L_9);
		return;
	}
}
// System.Void KeepCoffinBounded::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t KeepCoffinBounded_Update_m2902010793_MetadataUsageId;
extern "C"  void KeepCoffinBounded_Update_m2902010793 (KeepCoffinBounded_t702131559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeepCoffinBounded_Update_m2902010793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3_t3525329789 * L_0 = __this->get_address_of_bottomLeft_2();
		float L_1 = L_0->get_x_1();
		Vector3_t3525329789 * L_2 = __this->get_address_of_bottomLeft_2();
		float L_3 = L_2->get_y_2();
		Vector3_t3525329789 * L_4 = __this->get_address_of_topRight_3();
		float L_5 = L_4->get_x_1();
		Vector3_t3525329789 * L_6 = __this->get_address_of_bottomLeft_2();
		float L_7 = L_6->get_x_1();
		Vector3_t3525329789 * L_8 = __this->get_address_of_topRight_3();
		float L_9 = L_8->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of_bottomLeft_2();
		float L_11 = L_10->get_y_2();
		Rect__ctor_m3291325233((&V_0), L_1, L_3, ((float)((float)L_5-(float)L_7)), ((float)((float)L_9-(float)L_11)), /*hidden argument*/NULL);
		Transform_t284553113 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3525329789  L_14 = Transform_get_position_m2211398607(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_1)->get_x_1();
		float L_16 = Rect_get_xMin_m371109962((&V_0), /*hidden argument*/NULL);
		float L_17 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		Transform_t284553113 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t3525329789  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		float L_21 = (&V_2)->get_y_2();
		float L_22 = Rect_get_yMin_m399739113((&V_0), /*hidden argument*/NULL);
		float L_23 = Rect_get_yMax_m399510395((&V_0), /*hidden argument*/NULL);
		float L_24 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		Transform_t284553113 * L_25 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t3525329789  L_26 = Transform_get_position_m2211398607(L_25, /*hidden argument*/NULL);
		V_3 = L_26;
		float L_27 = (&V_3)->get_z_3();
		Vector3_t3525329789  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2926210380(&L_28, L_18, L_24, L_27, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_position_m3111394108(L_12, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KeepEnemyBounded::.ctor()
extern "C"  void KeepEnemyBounded__ctor_m323470753 (KeepEnemyBounded_t255541818 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KeepEnemyBounded::Start()
extern "C"  void KeepEnemyBounded_Start_m3565575841 (KeepEnemyBounded_t255541818 * __this, const MethodInfo* method)
{
	{
		Camera_t3533968274 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_1 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_2 = Camera_ScreenToWorldPoint_m1572306334(L_0, L_1, /*hidden argument*/NULL);
		__this->set_bottomLeft_2(L_2);
		Camera_t3533968274 * L_3 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t3533968274 * L_4 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Camera_get_pixelWidth_m175903141(L_4, /*hidden argument*/NULL);
		Camera_t3533968274 * L_6 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Camera_get_pixelHeight_m1661844426(L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m1846874791(&L_8, (((float)((float)L_5))), (((float)((float)L_7))), /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3525329789  L_9 = Camera_ScreenToWorldPoint_m1572306334(L_3, L_8, /*hidden argument*/NULL);
		__this->set_topRight_3(L_9);
		return;
	}
}
// System.Void KeepEnemyBounded::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t KeepEnemyBounded_Update_m3164520844_MetadataUsageId;
extern "C"  void KeepEnemyBounded_Update_m3164520844 (KeepEnemyBounded_t255541818 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeepEnemyBounded_Update_m3164520844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3_t3525329789 * L_0 = __this->get_address_of_bottomLeft_2();
		float L_1 = L_0->get_x_1();
		Vector3_t3525329789 * L_2 = __this->get_address_of_bottomLeft_2();
		float L_3 = L_2->get_y_2();
		Vector3_t3525329789 * L_4 = __this->get_address_of_topRight_3();
		float L_5 = L_4->get_x_1();
		Vector3_t3525329789 * L_6 = __this->get_address_of_bottomLeft_2();
		float L_7 = L_6->get_x_1();
		Vector3_t3525329789 * L_8 = __this->get_address_of_topRight_3();
		float L_9 = L_8->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of_bottomLeft_2();
		float L_11 = L_10->get_y_2();
		Rect__ctor_m3291325233((&V_0), L_1, L_3, ((float)((float)L_5-(float)L_7)), ((float)((float)L_9-(float)L_11)), /*hidden argument*/NULL);
		Transform_t284553113 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3525329789  L_14 = Transform_get_position_m2211398607(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_1)->get_x_1();
		float L_16 = Rect_get_xMin_m371109962((&V_0), /*hidden argument*/NULL);
		float L_17 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		Transform_t284553113 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t3525329789  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		float L_21 = (&V_2)->get_y_2();
		float L_22 = Rect_get_yMin_m399739113((&V_0), /*hidden argument*/NULL);
		float L_23 = Rect_get_yMax_m399510395((&V_0), /*hidden argument*/NULL);
		float L_24 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		Transform_t284553113 * L_25 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t3525329789  L_26 = Transform_get_position_m2211398607(L_25, /*hidden argument*/NULL);
		V_3 = L_26;
		float L_27 = (&V_3)->get_z_3();
		Vector3_t3525329789  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2926210380(&L_28, L_18, L_24, L_27, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_position_m3111394108(L_12, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KeepWithinBounds::.ctor()
extern "C"  void KeepWithinBounds__ctor_m3617625334 (KeepWithinBounds_t1755115333 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KeepWithinBounds::Start()
extern "C"  void KeepWithinBounds_Start_m2564763126 (KeepWithinBounds_t1755115333 * __this, const MethodInfo* method)
{
	{
		Camera_t3533968274 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_1 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_2 = Camera_ScreenToWorldPoint_m1572306334(L_0, L_1, /*hidden argument*/NULL);
		__this->set_bottomLeft_2(L_2);
		Camera_t3533968274 * L_3 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t3533968274 * L_4 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Camera_get_pixelWidth_m175903141(L_4, /*hidden argument*/NULL);
		Camera_t3533968274 * L_6 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Camera_get_pixelHeight_m1661844426(L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m1846874791(&L_8, (((float)((float)L_5))), (((float)((float)L_7))), /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3525329789  L_9 = Camera_ScreenToWorldPoint_m1572306334(L_3, L_8, /*hidden argument*/NULL);
		__this->set_topRight_3(L_9);
		return;
	}
}
// System.Void KeepWithinBounds::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t KeepWithinBounds_Update_m2204097751_MetadataUsageId;
extern "C"  void KeepWithinBounds_Update_m2204097751 (KeepWithinBounds_t1755115333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeepWithinBounds_Update_m2204097751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3_t3525329789 * L_0 = __this->get_address_of_bottomLeft_2();
		float L_1 = L_0->get_x_1();
		Vector3_t3525329789 * L_2 = __this->get_address_of_bottomLeft_2();
		float L_3 = L_2->get_y_2();
		Vector3_t3525329789 * L_4 = __this->get_address_of_topRight_3();
		float L_5 = L_4->get_x_1();
		Vector3_t3525329789 * L_6 = __this->get_address_of_bottomLeft_2();
		float L_7 = L_6->get_x_1();
		Vector3_t3525329789 * L_8 = __this->get_address_of_topRight_3();
		float L_9 = L_8->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of_bottomLeft_2();
		float L_11 = L_10->get_y_2();
		Rect__ctor_m3291325233((&V_0), L_1, L_3, ((float)((float)L_5-(float)L_7)), ((float)((float)L_9-(float)L_11)), /*hidden argument*/NULL);
		Transform_t284553113 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3525329789  L_14 = Transform_get_position_m2211398607(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_1)->get_x_1();
		float L_16 = Rect_get_xMin_m371109962((&V_0), /*hidden argument*/NULL);
		float L_17 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		Transform_t284553113 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t3525329789  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		float L_21 = (&V_2)->get_y_2();
		float L_22 = Rect_get_yMin_m399739113((&V_0), /*hidden argument*/NULL);
		float L_23 = Rect_get_yMax_m399510395((&V_0), /*hidden argument*/NULL);
		float L_24 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		Transform_t284553113 * L_25 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t3525329789  L_26 = Transform_get_position_m2211398607(L_25, /*hidden argument*/NULL);
		V_3 = L_26;
		float L_27 = (&V_3)->get_z_3();
		Vector3_t3525329789  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2926210380(&L_28, L_18, L_24, L_27, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_position_m3111394108(L_12, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainButtons::.ctor()
extern "C"  void MainButtons__ctor_m3880093955 (MainButtons_t1024182120 * __this, const MethodInfo* method)
{
	{
		__this->set_menuCounter_14(1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainButtons::Start()
extern TypeInfo* AudioClip_t3714538611_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t792326996_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1637965293;
extern Il2CppCodeGenString* _stringLiteral3967544799;
extern Il2CppCodeGenString* _stringLiteral109627663;
extern Il2CppCodeGenString* _stringLiteral1451218071;
extern Il2CppCodeGenString* _stringLiteral2038087063;
extern Il2CppCodeGenString* _stringLiteral3378223602;
extern Il2CppCodeGenString* _stringLiteral3276555704;
extern const uint32_t MainButtons_Start_m2827231747_MetadataUsageId;
extern "C"  void MainButtons_Start_m2827231747 (MainButtons_t1024182120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainButtons_Start_m2827231747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1637965293, /*hidden argument*/NULL);
		__this->set_hasPlayed_15(L_0);
		int32_t L_1 = __this->get_hasPlayed_15();
		if (L_1)
		{
			goto IL_0040;
		}
	}
	{
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral1637965293, 1, /*hidden argument*/NULL);
		PlayerPrefs_SetFloat_m1687591347(NULL /*static, unused*/, _stringLiteral3967544799, (3.5f), /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral109627663, 1, /*hidden argument*/NULL);
	}

IL_0040:
	{
		int32_t L_2 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral109627663, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0081;
		}
	}
	{
		AudioListener_set_pause_m581137981(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		AudioListener_set_volume_m1072709503(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		Toggle_t1499417981 * L_3 = __this->get_toggleSounds_19();
		NullCheck(L_3);
		Toggle_set_isOn_m3467664234(L_3, (bool)1, /*hidden argument*/NULL);
		Text_t3286458198 * L_4 = __this->get_soundText_10();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral1451218071);
		goto IL_00ad;
	}

IL_0081:
	{
		AudioListener_set_pause_m581137981(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		AudioListener_set_volume_m1072709503(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		Toggle_t1499417981 * L_5 = __this->get_toggleSounds_19();
		NullCheck(L_5);
		Toggle_set_isOn_m3467664234(L_5, (bool)0, /*hidden argument*/NULL);
		Text_t3286458198 * L_6 = __this->get_soundText_10();
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteral2038087063);
	}

IL_00ad:
	{
		Slider_t1468074762 * L_7 = __this->get_Slider_17();
		float L_8 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3967544799, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_7, L_8);
		GameObject_t4012695102 * L_9 = __this->get_Share_5();
		NullCheck(L_9);
		Animator_t792326996 * L_10 = GameObject_GetComponent_TisAnimator_t792326996_m2581074431(L_9, /*hidden argument*/GameObject_GetComponent_TisAnimator_t792326996_m2581074431_MethodInfo_var);
		__this->set_shareAni_12(L_10);
		GameObject_t4012695102 * L_11 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		AudioSource_t3628549054 * L_12 = GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151(L_11, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151_MethodInfo_var);
		__this->set_audioPlay_7(L_12);
		Object_t3878351788 * L_13 = Resources_Load_m2187391845(NULL /*static, unused*/, _stringLiteral3378223602, /*hidden argument*/NULL);
		__this->set_buttonPlay_8(((AudioClip_t3714538611 *)IsInstSealed(L_13, AudioClip_t3714538611_il2cpp_TypeInfo_var)));
		Object_t3878351788 * L_14 = Resources_Load_m2187391845(NULL /*static, unused*/, _stringLiteral3276555704, /*hidden argument*/NULL);
		__this->set_buttonCancel_9(((AudioClip_t3714538611 *)IsInstSealed(L_14, AudioClip_t3714538611_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void MainButtons::OnPlay()
extern Il2CppCodeGenString* _stringLiteral2211858;
extern const uint32_t MainButtons_OnPlay_m1840751828_MetadataUsageId;
extern "C"  void MainButtons_OnPlay_m1840751828 (MainButtons_t1024182120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainButtons_OnPlay_m1840751828_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t3628549054 * L_0 = __this->get_audioPlay_7();
		AudioClip_t3714538611 * L_1 = __this->get_buttonPlay_8();
		NullCheck(L_0);
		AudioSource_set_clip_m19502010(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_2 = __this->get_audioPlay_7();
		NullCheck(L_2);
		AudioSource_Play_m1360558992(L_2, /*hidden argument*/NULL);
		SceneManager_LoadSceneAsync_m1034954248(NULL /*static, unused*/, _stringLiteral2211858, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainButtons::OnShare()
extern Il2CppCodeGenString* _stringLiteral2175704758;
extern const uint32_t MainButtons_OnShare_m3776576353_MetadataUsageId;
extern "C"  void MainButtons_OnShare_m3776576353 (MainButtons_t1024182120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainButtons_OnShare_m3776576353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_shareAni_12();
		NullCheck(L_0);
		bool L_1 = Animator_GetBool_m436748612(L_0, _stringLiteral2175704758, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		Animator_t792326996 * L_2 = __this->get_shareAni_12();
		NullCheck(L_2);
		Animator_SetBool_m2336836203(L_2, _stringLiteral2175704758, (bool)0, /*hidden argument*/NULL);
		goto IL_003c;
	}

IL_002b:
	{
		Animator_t792326996 * L_3 = __this->get_shareAni_12();
		NullCheck(L_3);
		Animator_SetBool_m2336836203(L_3, _stringLiteral2175704758, (bool)1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void MainButtons::onSettings()
extern Il2CppCodeGenString* _stringLiteral2623751848;
extern const uint32_t MainButtons_onSettings_m2990676579_MetadataUsageId;
extern "C"  void MainButtons_onSettings_m2990676579 (MainButtons_t1024182120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainButtons_onSettings_m2990676579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_mainAni_13();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral2623751848, (bool)1, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_menuCounter_14();
		__this->set_menuCounter_14(((int32_t)((int32_t)L_1+(int32_t)1)));
		return;
	}
}
// System.Void MainButtons::onBack()
extern Il2CppCodeGenString* _stringLiteral2623751848;
extern const uint32_t MainButtons_onBack_m1365186983_MetadataUsageId;
extern "C"  void MainButtons_onBack_m1365186983 (MainButtons_t1024182120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainButtons_onBack_m1365186983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_menuCounter_14();
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		Animator_t792326996 * L_1 = __this->get_mainAni_13();
		NullCheck(L_1);
		Animator_SetBool_m2336836203(L_1, _stringLiteral2623751848, (bool)0, /*hidden argument*/NULL);
		PlayerPrefs_Save_m3891538519(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void MainButtons::AdjustSpeed(UnityEngine.UI.Slider)
extern Il2CppCodeGenString* _stringLiteral3967544799;
extern const uint32_t MainButtons_AdjustSpeed_m993147477_MetadataUsageId;
extern "C"  void MainButtons_AdjustSpeed_m993147477 (MainButtons_t1024182120 * __this, Slider_t1468074762 * ___slider, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainButtons_AdjustSpeed_m993147477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Slider_t1468074762 * L_0 = ___slider;
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		__this->set_sensitivity_16(L_1);
		float L_2 = __this->get_sensitivity_16();
		PlayerPrefs_SetFloat_m1687591347(NULL /*static, unused*/, _stringLiteral3967544799, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainButtons::toggleSound(UnityEngine.UI.Toggle)
extern Il2CppCodeGenString* _stringLiteral1451218071;
extern Il2CppCodeGenString* _stringLiteral109627663;
extern Il2CppCodeGenString* _stringLiteral2038087063;
extern const uint32_t MainButtons_toggleSound_m1727625887_MetadataUsageId;
extern "C"  void MainButtons_toggleSound_m1727625887 (MainButtons_t1024182120 * __this, Toggle_t1499417981 * ___toggleSounds, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainButtons_toggleSound_m1727625887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Toggle_t1499417981 * L_0 = ___toggleSounds;
		NullCheck(L_0);
		bool L_1 = Toggle_get_isOn_m2105608497(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		AudioListener_set_pause_m581137981(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		AudioListener_set_volume_m1072709503(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		Text_t3286458198 * L_2 = __this->get_soundText_10();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral1451218071);
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral109627663, 1, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_003b:
	{
		AudioListener_set_pause_m581137981(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		AudioListener_set_volume_m1072709503(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		Text_t3286458198 * L_3 = __this->get_soundText_10();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, _stringLiteral2038087063);
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral109627663, 0, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void MainMenu::.ctor()
extern "C"  void MainMenu__ctor_m310215363 (MainMenu_t55996120 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::Start()
extern TypeInfo* CanvasRendererU5BU5D_t2901501369_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t3354615620_m2140199269_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCanvasRenderer_t3626201768_m3097889003_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3826579694;
extern const uint32_t MainMenu_Start_m3552320451_MetadataUsageId;
extern "C"  void MainMenu_Start_m3552320451 (MainMenu_t55996120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_Start_m3552320451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	CanvasRenderer_t3626201768 * V_1 = NULL;
	CanvasRendererU5BU5D_t2901501369* V_2 = NULL;
	int32_t V_3 = 0;
	{
		GameObjectU5BU5D_t3499186955* L_0 = GameObject_FindGameObjectsWithTag_m3058873418(NULL /*static, unused*/, _stringLiteral3826579694, /*hidden argument*/NULL);
		__this->set_ButtonsFade_5(L_0);
		GameObjectU5BU5D_t3499186955* L_1 = __this->get_ButtonsFade_5();
		NullCheck(L_1);
		__this->set_allRend_6(((CanvasRendererU5BU5D_t2901501369*)SZArrayNew(CanvasRendererU5BU5D_t2901501369_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))));
		__this->set_isLerped_4((bool)0);
		GameObject_t4012695102 * L_2 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Image_t3354615620 * L_3 = GameObject_GetComponent_TisImage_t3354615620_m2140199269(L_2, /*hidden argument*/GameObject_GetComponent_TisImage_t3354615620_m2140199269_MethodInfo_var);
		__this->set_meshR_2(L_3);
		V_0 = 0;
		goto IL_005b;
	}

IL_0042:
	{
		CanvasRendererU5BU5D_t2901501369* L_4 = __this->get_allRend_6();
		int32_t L_5 = V_0;
		GameObjectU5BU5D_t3499186955* L_6 = __this->get_ButtonsFade_5();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		CanvasRenderer_t3626201768 * L_9 = GameObject_GetComponent_TisCanvasRenderer_t3626201768_m3097889003(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/GameObject_GetComponent_TisCanvasRenderer_t3626201768_m3097889003_MethodInfo_var);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_9);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CanvasRenderer_t3626201768 *)L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_11 = V_0;
		GameObjectU5BU5D_t3499186955* L_12 = __this->get_ButtonsFade_5();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0042;
		}
	}
	{
		CanvasRendererU5BU5D_t2901501369* L_13 = __this->get_allRend_6();
		V_2 = L_13;
		V_3 = 0;
		goto IL_008a;
	}

IL_0077:
	{
		CanvasRendererU5BU5D_t2901501369* L_14 = V_2;
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		V_1 = ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16)));
		CanvasRenderer_t3626201768 * L_17 = V_1;
		NullCheck(L_17);
		CanvasRenderer_SetAlpha_m3610852573(L_17, (0.0f), /*hidden argument*/NULL);
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_008a:
	{
		int32_t L_19 = V_3;
		CanvasRendererU5BU5D_t2901501369* L_20 = V_2;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		return;
	}
}
// System.Void MainMenu::Update()
extern Il2CppCodeGenString* _stringLiteral1798161030;
extern const uint32_t MainMenu_Update_m2753603754_MetadataUsageId;
extern "C"  void MainMenu_Update_m2753603754 (MainMenu_t55996120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_Update_m2753603754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isShowingSplashScreen_m238201170(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1798161030, (0.2f), /*hidden argument*/NULL);
		bool L_1 = __this->get_isLerped_4();
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		MonoBehaviour_CancelInvoke_m3230208631(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void MainMenu::doBlend()
extern Il2CppCodeGenString* _stringLiteral2784039730;
extern const uint32_t MainMenu_doBlend_m863553799_MetadataUsageId;
extern "C"  void MainMenu_doBlend_m863553799 (MainMenu_t55996120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_doBlend_m863553799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CanvasRenderer_t3626201768 * V_0 = NULL;
	CanvasRendererU5BU5D_t2901501369* V_1 = NULL;
	int32_t V_2 = 0;
	{
		float L_0 = __this->get_lerp_3();
		if ((!(((float)L_0) < ((float)(1.0f)))))
		{
			goto IL_007e;
		}
	}
	{
		bool L_1 = __this->get_isLerped_4();
		if (L_1)
		{
			goto IL_007e;
		}
	}
	{
		float L_2 = __this->get_lerp_3();
		float L_3 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lerp_3(((float)((float)L_2+(float)((float)((float)(1.0f)*(float)L_3)))));
		Image_t3354615620 * L_4 = __this->get_meshR_2();
		NullCheck(L_4);
		Material_t1886596500 * L_5 = VirtFuncInvoker0< Material_t1886596500 * >::Invoke(26 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_4);
		float L_6 = __this->get_lerp_3();
		NullCheck(L_5);
		Material_SetFloat_m981710063(L_5, _stringLiteral2784039730, L_6, /*hidden argument*/NULL);
		CanvasRendererU5BU5D_t2901501369* L_7 = __this->get_allRend_6();
		V_1 = L_7;
		V_2 = 0;
		goto IL_0070;
	}

IL_005c:
	{
		CanvasRendererU5BU5D_t2901501369* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_0 = ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		CanvasRenderer_t3626201768 * L_11 = V_0;
		float L_12 = __this->get_lerp_3();
		NullCheck(L_11);
		CanvasRenderer_SetAlpha_m3610852573(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0070:
	{
		int32_t L_14 = V_2;
		CanvasRendererU5BU5D_t2901501369* L_15 = V_1;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		goto IL_0085;
	}

IL_007e:
	{
		__this->set_isLerped_4((bool)1);
	}

IL_0085:
	{
		return;
	}
}
// System.Void OnCollisionDie::.ctor()
extern "C"  void OnCollisionDie__ctor_m3021636526 (OnCollisionDie_t1988392845 * __this, const MethodInfo* method)
{
	{
		__this->set_currentLevel_7(1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnCollisionDie::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern Il2CppCodeGenString* _stringLiteral2482852845;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t OnCollisionDie_Start_m1968774318_MetadataUsageId;
extern "C"  void OnCollisionDie_Start_m1968774318 (OnCollisionDie_t1988392845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnCollisionDie_Start_m1968774318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_4(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_4();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_5(L_2);
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2482852845, /*hidden argument*/NULL);
		__this->set_Sphere_6(L_3);
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_CompareTag_m3153977471(L_4, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0046;
		}
	}

IL_0046:
	{
		return;
	}
}
// System.Void OnCollisionDie::Update()
extern "C"  void OnCollisionDie_Update_m908313887 (OnCollisionDie_t1988392845 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		__this->set_pos_2(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Vector3_t3525329789  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m2926210380(&L_4, (0.0f), (0.0f), (4.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_5 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_behindRocks_3(L_5);
		return;
	}
}
// System.Void OnCollisionDie::OnTriggerEnter2D(UnityEngine.Collider2D)
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral67100520;
extern Il2CppCodeGenString* _stringLiteral3721122659;
extern Il2CppCodeGenString* _stringLiteral3834188923;
extern Il2CppCodeGenString* _stringLiteral2475494281;
extern Il2CppCodeGenString* _stringLiteral2977462423;
extern Il2CppCodeGenString* _stringLiteral59093274;
extern Il2CppCodeGenString* _stringLiteral3862149722;
extern Il2CppCodeGenString* _stringLiteral2941537076;
extern Il2CppCodeGenString* _stringLiteral59093275;
extern Il2CppCodeGenString* _stringLiteral1309992761;
extern Il2CppCodeGenString* _stringLiteral2941537077;
extern Il2CppCodeGenString* _stringLiteral59093276;
extern Il2CppCodeGenString* _stringLiteral3052803096;
extern Il2CppCodeGenString* _stringLiteral2941537078;
extern Il2CppCodeGenString* _stringLiteral59093277;
extern Il2CppCodeGenString* _stringLiteral500646135;
extern Il2CppCodeGenString* _stringLiteral2941537079;
extern Il2CppCodeGenString* _stringLiteral816588514;
extern Il2CppCodeGenString* _stringLiteral3064538706;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral1260913290;
extern Il2CppCodeGenString* _stringLiteral2615117;
extern Il2CppCodeGenString* _stringLiteral924806977;
extern Il2CppCodeGenString* _stringLiteral2906718780;
extern Il2CppCodeGenString* _stringLiteral69916416;
extern const uint32_t OnCollisionDie_OnTriggerEnter2D_m35539338_MetadataUsageId;
extern "C"  void OnCollisionDie_OnTriggerEnter2D_m35539338 (OnCollisionDie_t1988392845 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnCollisionDie_OnTriggerEnter2D_m35539338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	GameObject_t4012695102 * V_1 = NULL;
	GameObject_t4012695102 * V_2 = NULL;
	GameObject_t4012695102 * V_3 = NULL;
	int32_t V_4 = 0;
	GameObject_t4012695102 * V_5 = NULL;
	GameObject_t4012695102 * V_6 = NULL;
	GameObject_t4012695102 * V_7 = NULL;
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral67100520, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Collider2D_t1890038195 * L_2 = ___target;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m305486283(L_2, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0094;
		}
	}

IL_0025:
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_CompareTag_m3153977471(L_4, _stringLiteral67100520, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004a;
		}
	}
	{
		Collider2D_t1890038195 * L_6 = ___target;
		NullCheck(L_6);
		bool L_7 = Component_CompareTag_m305486283(L_6, _stringLiteral3834188923, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0094;
		}
	}

IL_004a:
	{
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = GameObject_CompareTag_m3153977471(L_8, _stringLiteral67100520, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006f;
		}
	}
	{
		Collider2D_t1890038195 * L_10 = ___target;
		NullCheck(L_10);
		bool L_11 = Component_CompareTag_m305486283(L_10, _stringLiteral2475494281, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0094;
		}
	}

IL_006f:
	{
		GameObject_t4012695102 * L_12 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = GameObject_CompareTag_m3153977471(L_12, _stringLiteral67100520, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0318;
		}
	}
	{
		Collider2D_t1890038195 * L_14 = ___target;
		NullCheck(L_14);
		bool L_15 = Component_CompareTag_m305486283(L_14, _stringLiteral2977462423, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0318;
		}
	}

IL_0094:
	{
		GameObject_t4012695102 * L_16 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		String_t* L_17 = Object_get_name_m3709440845(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_17, _stringLiteral59093274, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00c8;
		}
	}
	{
		GameObject_t4012695102 * L_19 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m3709440845(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_20, _stringLiteral3862149722, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0102;
		}
	}

IL_00c8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_23 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2941537076, L_22, /*hidden argument*/NULL);
		Vector3_t3525329789  L_24 = __this->get_pos_2();
		Quaternion_t1891715979  L_25 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_26 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_23, L_24, L_25, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)CastclassSealed(L_26, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_27 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_27, (3.0f), /*hidden argument*/NULL);
		goto IL_0247;
	}

IL_0102:
	{
		GameObject_t4012695102 * L_28 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29 = Object_get_name_m3709440845(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_29, _stringLiteral59093275, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_0136;
		}
	}
	{
		GameObject_t4012695102 * L_31 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		String_t* L_32 = Object_get_name_m3709440845(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_33 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_32, _stringLiteral1309992761, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0170;
		}
	}

IL_0136:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_35 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2941537077, L_34, /*hidden argument*/NULL);
		Vector3_t3525329789  L_36 = __this->get_pos_2();
		Quaternion_t1891715979  L_37 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_38 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_35, L_36, L_37, /*hidden argument*/NULL);
		V_1 = ((GameObject_t4012695102 *)CastclassSealed(L_38, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_39 = V_1;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_39, (3.0f), /*hidden argument*/NULL);
		goto IL_0247;
	}

IL_0170:
	{
		GameObject_t4012695102 * L_40 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = Object_get_name_m3709440845(L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_42 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_41, _stringLiteral59093276, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_01a4;
		}
	}
	{
		GameObject_t4012695102 * L_43 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		String_t* L_44 = Object_get_name_m3709440845(L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_45 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_44, _stringLiteral3052803096, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_01de;
		}
	}

IL_01a4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_46 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_47 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2941537078, L_46, /*hidden argument*/NULL);
		Vector3_t3525329789  L_48 = __this->get_pos_2();
		Quaternion_t1891715979  L_49 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_50 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_47, L_48, L_49, /*hidden argument*/NULL);
		V_2 = ((GameObject_t4012695102 *)CastclassSealed(L_50, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_51 = V_2;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_51, (3.0f), /*hidden argument*/NULL);
		goto IL_0247;
	}

IL_01de:
	{
		GameObject_t4012695102 * L_52 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		String_t* L_53 = Object_get_name_m3709440845(L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_54 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_53, _stringLiteral59093277, /*hidden argument*/NULL);
		if (L_54)
		{
			goto IL_0212;
		}
	}
	{
		GameObject_t4012695102 * L_55 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		String_t* L_56 = Object_get_name_m3709440845(L_55, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_57 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_56, _stringLiteral500646135, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0247;
		}
	}

IL_0212:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_58 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_59 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2941537079, L_58, /*hidden argument*/NULL);
		Vector3_t3525329789  L_60 = __this->get_pos_2();
		Quaternion_t1891715979  L_61 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_62 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_59, L_60, L_61, /*hidden argument*/NULL);
		V_3 = ((GameObject_t4012695102 *)CastclassSealed(L_62, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_63 = V_3;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_63, (3.0f), /*hidden argument*/NULL);
	}

IL_0247:
	{
		Collider2D_t1890038195 * L_64 = ___target;
		NullCheck(L_64);
		bool L_65 = Component_CompareTag_m305486283(L_64, _stringLiteral3834188923, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_0262;
		}
	}
	{
		Collider2D_t1890038195 * L_66 = ___target;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_66, (1.0f), /*hidden argument*/NULL);
	}

IL_0262:
	{
		Parameters_t2452200970 * L_67 = __this->get_parameters_5();
		bool L_68 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_0285;
		}
	}
	{
		Parameters_t2452200970 * L_69 = __this->get_parameters_5();
		Parameters_t2452200970 * L_70 = L_69;
		NullCheck(L_70);
		int32_t L_71 = L_70->get_Enemies_14();
		NullCheck(L_70);
		L_70->set_Enemies_14(((int32_t)((int32_t)L_71+(int32_t)1)));
	}

IL_0285:
	{
		GameObject_t4012695102 * L_72 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		int32_t L_73 = Random_Range_m75452833(NULL /*static, unused*/, 1, 4, /*hidden argument*/NULL);
		V_4 = L_73;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_74 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_75 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral816588514, L_74, /*hidden argument*/NULL);
		Vector3_t3525329789  L_76 = __this->get_pos_2();
		Quaternion_t1891715979  L_77 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_78 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_75, L_76, L_77, /*hidden argument*/NULL);
		V_5 = ((GameObject_t4012695102 *)CastclassSealed(L_78, GameObject_t4012695102_il2cpp_TypeInfo_var));
		int32_t L_79 = V_4;
		int32_t L_80 = L_79;
		Il2CppObject * L_81 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_80);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_82 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3064538706, L_81, /*hidden argument*/NULL);
		Type_t * L_83 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_84 = Resources_Load_m3601699608(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
		Vector3_t3525329789  L_85 = __this->get_behindRocks_3();
		Quaternion_t1891715979  L_86 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_87 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_84, L_85, L_86, /*hidden argument*/NULL);
		V_6 = ((GameObject_t4012695102 *)CastclassSealed(L_87, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_88 = V_5;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_88, (1.0f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_89 = V_6;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_89, (3.0f), /*hidden argument*/NULL);
		goto IL_04c2;
	}

IL_0318:
	{
		GameObject_t4012695102 * L_90 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_90);
		bool L_91 = GameObject_CompareTag_m3153977471(L_90, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_033d;
		}
	}
	{
		Collider2D_t1890038195 * L_92 = ___target;
		NullCheck(L_92);
		bool L_93 = Component_CompareTag_m305486283(L_92, _stringLiteral67100520, /*hidden argument*/NULL);
		if (L_93)
		{
			goto IL_03d1;
		}
	}

IL_033d:
	{
		GameObject_t4012695102 * L_94 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_94);
		bool L_95 = GameObject_CompareTag_m3153977471(L_94, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_95)
		{
			goto IL_0362;
		}
	}
	{
		Collider2D_t1890038195 * L_96 = ___target;
		NullCheck(L_96);
		bool L_97 = Component_CompareTag_m305486283(L_96, _stringLiteral1260913290, /*hidden argument*/NULL);
		if (L_97)
		{
			goto IL_03d1;
		}
	}

IL_0362:
	{
		GameObject_t4012695102 * L_98 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_98);
		bool L_99 = GameObject_CompareTag_m3153977471(L_98, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_99)
		{
			goto IL_0387;
		}
	}
	{
		Collider2D_t1890038195 * L_100 = ___target;
		NullCheck(L_100);
		bool L_101 = Component_CompareTag_m305486283(L_100, _stringLiteral2615117, /*hidden argument*/NULL);
		if (L_101)
		{
			goto IL_03d1;
		}
	}

IL_0387:
	{
		GameObject_t4012695102 * L_102 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_102);
		bool L_103 = GameObject_CompareTag_m3153977471(L_102, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_03ac;
		}
	}
	{
		Collider2D_t1890038195 * L_104 = ___target;
		NullCheck(L_104);
		bool L_105 = Component_CompareTag_m305486283(L_104, _stringLiteral924806977, /*hidden argument*/NULL);
		if (L_105)
		{
			goto IL_03d1;
		}
	}

IL_03ac:
	{
		GameObject_t4012695102 * L_106 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_106);
		bool L_107 = GameObject_CompareTag_m3153977471(L_106, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_107)
		{
			goto IL_0418;
		}
	}
	{
		Collider2D_t1890038195 * L_108 = ___target;
		NullCheck(L_108);
		bool L_109 = Component_CompareTag_m305486283(L_108, _stringLiteral2906718780, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_0418;
		}
	}

IL_03d1:
	{
		GameObject_t4012695102 * L_110 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_110, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_111 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_112 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral816588514, L_111, /*hidden argument*/NULL);
		Vector3_t3525329789  L_113 = __this->get_pos_2();
		Quaternion_t1891715979  L_114 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_115 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_112, L_113, L_114, /*hidden argument*/NULL);
		V_7 = ((GameObject_t4012695102 *)CastclassSealed(L_115, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_116 = V_7;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_116, (1.0f), /*hidden argument*/NULL);
		goto IL_04c2;
	}

IL_0418:
	{
		GameObject_t4012695102 * L_117 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_117);
		bool L_118 = GameObject_CompareTag_m3153977471(L_117, _stringLiteral2475494281, /*hidden argument*/NULL);
		if (!L_118)
		{
			goto IL_043d;
		}
	}
	{
		Collider2D_t1890038195 * L_119 = ___target;
		NullCheck(L_119);
		bool L_120 = Component_CompareTag_m305486283(L_119, _stringLiteral67100520, /*hidden argument*/NULL);
		if (L_120)
		{
			goto IL_0462;
		}
	}

IL_043d:
	{
		GameObject_t4012695102 * L_121 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_121);
		bool L_122 = GameObject_CompareTag_m3153977471(L_121, _stringLiteral2475494281, /*hidden argument*/NULL);
		if (!L_122)
		{
			goto IL_048f;
		}
	}
	{
		Collider2D_t1890038195 * L_123 = ___target;
		NullCheck(L_123);
		bool L_124 = Component_CompareTag_m305486283(L_123, _stringLiteral1260913290, /*hidden argument*/NULL);
		if (!L_124)
		{
			goto IL_048f;
		}
	}

IL_0462:
	{
		Collider2D_t1890038195 * L_125 = ___target;
		Object_Destroy_m176400816(NULL /*static, unused*/, L_125, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_126 = __this->get_Sphere_6();
		NullCheck(L_126);
		MeshRenderer_t1217738301 * L_127 = GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(L_126, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var);
		NullCheck(L_127);
		Renderer_set_enabled_m2514140131(L_127, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_128 = __this->get_Sphere_6();
		NullCheck(L_128);
		CircleCollider2D_t594472611 * L_129 = GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224(L_128, /*hidden argument*/GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224_MethodInfo_var);
		NullCheck(L_129);
		Behaviour_set_enabled_m2046806933(L_129, (bool)0, /*hidden argument*/NULL);
		goto IL_04c2;
	}

IL_048f:
	{
		GameObject_t4012695102 * L_130 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_130);
		bool L_131 = GameObject_CompareTag_m3153977471(L_130, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_04c2;
		}
	}
	{
		Collider2D_t1890038195 * L_132 = ___target;
		NullCheck(L_132);
		bool L_133 = Component_CompareTag_m305486283(L_132, _stringLiteral69916416, /*hidden argument*/NULL);
		if (!L_133)
		{
			goto IL_04c2;
		}
	}
	{
		int32_t L_134 = __this->get_currentLevel_7();
		__this->set_currentLevel_7(((int32_t)((int32_t)L_134+(int32_t)1)));
	}

IL_04c2:
	{
		return;
	}
}
// System.Void OnCollisionDie::OnParticleCollision(UnityEngine.GameObject)
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2906718780;
extern Il2CppCodeGenString* _stringLiteral816588514;
extern const uint32_t OnCollisionDie_OnParticleCollision_m1081715761_MetadataUsageId;
extern "C"  void OnCollisionDie_OnParticleCollision_m1081715761 (OnCollisionDie_t1988392845 * __this, GameObject_t4012695102 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnCollisionDie_OnParticleCollision_m1081715761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0065;
		}
	}
	{
		GameObject_t4012695102 * L_2 = ___target;
		NullCheck(L_2);
		bool L_3 = GameObject_CompareTag_m3153977471(L_2, _stringLiteral2906718780, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0065;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_6 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral816588514, L_5, /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = __this->get_pos_2();
		Quaternion_t1891715979  L_8 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_9 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)CastclassSealed(L_9, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_10 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_10, (1.0f), /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void OrbitCamera::.ctor()
extern "C"  void OrbitCamera__ctor_m2107310172 (OrbitCamera_t2298935279 * __this, const MethodInfo* method)
{
	{
		__this->set_Distance_3((10.0f));
		__this->set_DistanceMin_4((5.0f));
		__this->set_DistanceMax_5((15.0f));
		__this->set_X_MouseSensitivity_10((5.0f));
		__this->set_Y_MouseSensitivity_11((5.0f));
		__this->set_MouseWheelSensitivity_12((5.0f));
		__this->set_Y_MinLimit_13((15.0f));
		__this->set_Y_MaxLimit_14((70.0f));
		__this->set_DistanceSmooth_15((0.025f));
		Vector3_t3525329789  L_0 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_desiredPosition_17(L_0);
		__this->set_X_Smooth_18((0.05f));
		__this->set_Y_Smooth_19((0.1f));
		Vector3_t3525329789  L_1 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_position_23(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Start()
extern "C"  void OrbitCamera_Start_m1054447964 (OrbitCamera_t2298935279 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = __this->get_TargetLookAt_2();
		NullCheck(L_0);
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t284553113 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		float L_6 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		__this->set_Distance_3(L_6);
		float L_7 = __this->get_Distance_3();
		float L_8 = __this->get_DistanceMax_5();
		if ((!(((float)L_7) > ((float)L_8))))
		{
			goto IL_0048;
		}
	}
	{
		float L_9 = __this->get_Distance_3();
		__this->set_DistanceMax_5(L_9);
	}

IL_0048:
	{
		float L_10 = __this->get_Distance_3();
		__this->set_startingDistance_6(L_10);
		OrbitCamera_Reset_m4048710409(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Update()
extern "C"  void OrbitCamera_Update_m2628967985 (OrbitCamera_t2298935279 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void OrbitCamera::LateUpdate()
extern "C"  void OrbitCamera_LateUpdate_m3776455543 (OrbitCamera_t2298935279 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = __this->get_TargetLookAt_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		OrbitCamera_HandlePlayerInput_m790181339(__this, /*hidden argument*/NULL);
		OrbitCamera_CalculateDesiredPosition_m641278533(__this, /*hidden argument*/NULL);
		OrbitCamera_UpdatePosition_m1656847674(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::HandlePlayerInput()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2907718525;
extern Il2CppCodeGenString* _stringLiteral2907718526;
extern Il2CppCodeGenString* _stringLiteral2268470611;
extern const uint32_t OrbitCamera_HandlePlayerInput_m790181339_MetadataUsageId;
extern "C"  void OrbitCamera_HandlePlayerInput_m790181339 (OrbitCamera_t2298935279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrbitCamera_HandlePlayerInput_m790181339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.01f);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_004d;
		}
	}
	{
		float L_1 = __this->get_mouseX_8();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		float L_3 = __this->get_X_MouseSensitivity_10();
		__this->set_mouseX_8(((float)((float)L_1+(float)((float)((float)L_2*(float)L_3)))));
		float L_4 = __this->get_mouseY_9();
		float L_5 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		float L_6 = __this->get_Y_MouseSensitivity_11();
		__this->set_mouseY_9(((float)((float)L_4-(float)((float)((float)L_5*(float)L_6)))));
	}

IL_004d:
	{
		float L_7 = __this->get_mouseY_9();
		float L_8 = __this->get_Y_MinLimit_13();
		float L_9 = __this->get_Y_MaxLimit_14();
		float L_10 = OrbitCamera_ClampAngle_m1631653633(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_mouseY_9(L_10);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_11 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2268470611, /*hidden argument*/NULL);
		float L_12 = V_0;
		if ((((float)L_11) < ((float)((-L_12)))))
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_13 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2268470611, /*hidden argument*/NULL);
		float L_14 = V_0;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_00bb;
		}
	}

IL_008c:
	{
		float L_15 = __this->get_Distance_3();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_16 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2268470611, /*hidden argument*/NULL);
		float L_17 = __this->get_MouseWheelSensitivity_12();
		float L_18 = __this->get_DistanceMin_4();
		float L_19 = __this->get_DistanceMax_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_20 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_15-(float)((float)((float)L_16*(float)L_17)))), L_18, L_19, /*hidden argument*/NULL);
		__this->set_desiredDistance_7(L_20);
	}

IL_00bb:
	{
		return;
	}
}
// System.Void OrbitCamera::CalculateDesiredPosition()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t OrbitCamera_CalculateDesiredPosition_m641278533_MetadataUsageId;
extern "C"  void OrbitCamera_CalculateDesiredPosition_m641278533 (OrbitCamera_t2298935279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrbitCamera_CalculateDesiredPosition_m641278533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_Distance_3();
		float L_1 = __this->get_desiredDistance_7();
		float* L_2 = __this->get_address_of_velocityDistance_16();
		float L_3 = __this->get_DistanceSmooth_15();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_4 = Mathf_SmoothDamp_m488292903(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_Distance_3(L_4);
		float L_5 = __this->get_mouseY_9();
		float L_6 = __this->get_mouseX_8();
		float L_7 = __this->get_Distance_3();
		Vector3_t3525329789  L_8 = OrbitCamera_CalculatePosition_m2715746098(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
		__this->set_desiredPosition_17(L_8);
		return;
	}
}
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
extern "C"  Vector3_t3525329789  OrbitCamera_CalculatePosition_m2715746098 (OrbitCamera_t2298935279 * __this, float ___rotationX, float ___rotationY, float ___distance, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1891715979  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___distance;
		Vector3__ctor_m2926210380((&V_0), (0.0f), (0.0f), ((-L_0)), /*hidden argument*/NULL);
		float L_1 = ___rotationX;
		float L_2 = ___rotationY;
		Quaternion_t1891715979  L_3 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		V_1 = L_3;
		Transform_t284553113 * L_4 = __this->get_TargetLookAt_2();
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_6 = V_1;
		Vector3_t3525329789  L_7 = V_0;
		Vector3_t3525329789  L_8 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t3525329789  L_9 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void OrbitCamera::UpdatePosition()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t OrbitCamera_UpdatePosition_m1656847674_MetadataUsageId;
extern "C"  void OrbitCamera_UpdatePosition_m1656847674 (OrbitCamera_t2298935279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrbitCamera_UpdatePosition_m1656847674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Vector3_t3525329789 * L_0 = __this->get_address_of_position_23();
		float L_1 = L_0->get_x_1();
		Vector3_t3525329789 * L_2 = __this->get_address_of_desiredPosition_17();
		float L_3 = L_2->get_x_1();
		float* L_4 = __this->get_address_of_velX_20();
		float L_5 = __this->get_X_Smooth_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_6 = Mathf_SmoothDamp_m488292903(NULL /*static, unused*/, L_1, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t3525329789 * L_7 = __this->get_address_of_position_23();
		float L_8 = L_7->get_y_2();
		Vector3_t3525329789 * L_9 = __this->get_address_of_desiredPosition_17();
		float L_10 = L_9->get_y_2();
		float* L_11 = __this->get_address_of_velY_21();
		float L_12 = __this->get_Y_Smooth_19();
		float L_13 = Mathf_SmoothDamp_m488292903(NULL /*static, unused*/, L_8, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Vector3_t3525329789 * L_14 = __this->get_address_of_position_23();
		float L_15 = L_14->get_z_3();
		Vector3_t3525329789 * L_16 = __this->get_address_of_desiredPosition_17();
		float L_17 = L_16->get_z_3();
		float* L_18 = __this->get_address_of_velZ_22();
		float L_19 = __this->get_X_Smooth_18();
		float L_20 = Mathf_SmoothDamp_m488292903(NULL /*static, unused*/, L_15, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		float L_21 = V_0;
		float L_22 = V_1;
		float L_23 = V_2;
		Vector3_t3525329789  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m2926210380(&L_24, L_21, L_22, L_23, /*hidden argument*/NULL);
		__this->set_position_23(L_24);
		Transform_t284553113 * L_25 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_26 = __this->get_position_23();
		NullCheck(L_25);
		Transform_set_position_m3111394108(L_25, L_26, /*hidden argument*/NULL);
		Transform_t284553113 * L_27 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_28 = __this->get_TargetLookAt_2();
		NullCheck(L_27);
		Transform_LookAt_m2663225588(L_27, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Reset()
extern "C"  void OrbitCamera_Reset_m4048710409 (OrbitCamera_t2298935279 * __this, const MethodInfo* method)
{
	{
		__this->set_mouseX_8((0.0f));
		__this->set_mouseY_9((0.0f));
		float L_0 = __this->get_startingDistance_6();
		__this->set_Distance_3(L_0);
		float L_1 = __this->get_Distance_3();
		__this->set_desiredDistance_7(L_1);
		return;
	}
}
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t OrbitCamera_ClampAngle_m1631653633_MetadataUsageId;
extern "C"  float OrbitCamera_ClampAngle_m1631653633 (OrbitCamera_t2298935279 * __this, float ___angle, float ___min, float ___max, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrbitCamera_ClampAngle_m1631653633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_002d;
	}

IL_0005:
	{
		float L_0 = ___angle;
		if ((!(((float)L_0) < ((float)(-360.0f)))))
		{
			goto IL_0019;
		}
	}
	{
		float L_1 = ___angle;
		___angle = ((float)((float)L_1+(float)(360.0f)));
	}

IL_0019:
	{
		float L_2 = ___angle;
		if ((!(((float)L_2) > ((float)(360.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = ___angle;
		___angle = ((float)((float)L_3-(float)(360.0f)));
	}

IL_002d:
	{
		float L_4 = ___angle;
		if ((((float)L_4) < ((float)(-360.0f))))
		{
			goto IL_0005;
		}
	}
	{
		float L_5 = ___angle;
		if ((((float)L_5) > ((float)(360.0f))))
		{
			goto IL_0005;
		}
	}
	{
		float L_6 = ___angle;
		float L_7 = ___min;
		float L_8 = ___max;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void Parallaxing::.ctor()
extern TypeInfo* SingleU5BU5D_t1219431280_il2cpp_TypeInfo_var;
extern const uint32_t Parallaxing__ctor_m2631817472_MetadataUsageId;
extern "C"  void Parallaxing__ctor_m2631817472 (Parallaxing_t2299696843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parallaxing__ctor_m2631817472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingleU5BU5D_t1219431280* L_0 = ((SingleU5BU5D_t1219431280*)SZArrayNew(SingleU5BU5D_t1219431280_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)(0.3f));
		SingleU5BU5D_t1219431280* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)(0.6f));
		SingleU5BU5D_t1219431280* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)(1.0f));
		__this->set_speed_3(L_2);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Parallaxing::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern const uint32_t Parallaxing_Start_m1578955264_MetadataUsageId;
extern "C"  void Parallaxing_Start_m1578955264 (Parallaxing_t2299696843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parallaxing_Start_m1578955264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Parameters_t2452200970 * L_1 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_0, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		NullCheck(L_1);
		float L_2 = L_1->get_spawnCoeficient_16();
		__this->set_parameters_4(L_2);
		return;
	}
}
// System.Void Parallaxing::Update()
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t1217738301_m3731621022_MethodInfo_var;
extern const uint32_t Parallaxing_Update_m1708825101_MetadataUsageId;
extern "C"  void Parallaxing_Update_m1708825101 (Parallaxing_t2299696843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parallaxing_Update_m1708825101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MeshRenderer_t1217738301 * V_1 = NULL;
	Material_t1886596500 * V_2 = NULL;
	Vector2_t3525329788  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = 0;
		goto IL_007b;
	}

IL_0007:
	{
		SingleU5BU5D_t1219431280* L_0 = __this->get_speed_3();
		int32_t L_1 = V_0;
		SingleU5BU5D_t1219431280* L_2 = __this->get_speed_3();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		float L_5 = __this->get_parameters_4();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (float)((float)((float)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)))*(float)L_5)));
		TransformU5BU5D_t3681339876* L_6 = __this->get_backgrounds_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		MeshRenderer_t1217738301 * L_9 = Component_GetComponent_TisMeshRenderer_t1217738301_m3731621022(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/Component_GetComponent_TisMeshRenderer_t1217738301_m3731621022_MethodInfo_var);
		V_1 = L_9;
		MeshRenderer_t1217738301 * L_10 = V_1;
		NullCheck(L_10);
		Material_t1886596500 * L_11 = Renderer_get_material_m2720864603(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		Material_t1886596500 * L_12 = V_2;
		NullCheck(L_12);
		Vector2_t3525329788  L_13 = Material_get_mainTextureOffset_m3247688085(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		Vector2_t3525329788 * L_14 = (&V_3);
		float L_15 = L_14->get_x_1();
		float L_16 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		SingleU5BU5D_t1219431280* L_17 = __this->get_speed_3();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		L_14->set_x_1(((float)((float)L_15-(float)((float)((float)((float)((float)L_16/(float)(20.0f)))*(float)((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19))))))));
		float L_20 = (&V_3)->get_x_1();
		(&V_3)->set_x_1((fmodf(L_20, (-3.0f))));
		Material_t1886596500 * L_21 = V_2;
		Vector2_t3525329788  L_22 = V_3;
		NullCheck(L_21);
		Material_set_mainTextureOffset_m3397882654(L_21, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_007b:
	{
		int32_t L_24 = V_0;
		TransformU5BU5D_t3681339876* L_25 = __this->get_backgrounds_2();
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Parameters::.ctor()
extern "C"  void Parameters__ctor_m1181885905 (Parameters_t2452200970 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Parameters::Start()
extern const MethodInfo* GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral65287138;
extern Il2CppCodeGenString* _stringLiteral59078150;
extern const uint32_t Parameters_Start_m129023697_MetadataUsageId;
extern "C"  void Parameters_Start_m129023697 (Parameters_t2452200970 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parameters_Start_m129023697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral65287138, /*hidden argument*/NULL);
		__this->set_GoCoins_2(L_0);
		GameObject_t4012695102 * L_1 = __this->get_GoCoins_2();
		NullCheck(L_1);
		Text_t3286458198 * L_2 = GameObject_GetComponent_TisText_t3286458198_m202917489(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var);
		__this->set_scoreText_4(L_2);
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral59078150, /*hidden argument*/NULL);
		__this->set_GoEnemies_3(L_3);
		GameObject_t4012695102 * L_4 = __this->get_GoEnemies_3();
		NullCheck(L_4);
		Text_t3286458198 * L_5 = GameObject_GetComponent_TisText_t3286458198_m202917489(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var);
		__this->set_enemiesText_5(L_5);
		__this->set_playerShootSpeed_10((2.0f));
		__this->set_enemyShootSpeed_11((2.0f));
		__this->set_enemyBulletSpeed_6((3.0f));
		__this->set_PlayerBulletSpeed_7((2.0f));
		__this->set_playerSpeed_9((5.0f));
		__this->set_enemySpeed_8((5.0f));
		__this->set_goldCoinSpeed_12((5.0f));
		__this->set_Coins_13(0);
		__this->set_Enemies_14(0);
		__this->set_currentCheckPoint_15(0);
		__this->set_spawnCoeficient_16((1.0f));
		return;
	}
}
// System.Void Parameters::Update()
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1059;
extern Il2CppCodeGenString* _stringLiteral3538920214;
extern const uint32_t Parameters_Update_m4005586780_MetadataUsageId;
extern "C"  void Parameters_Update_m4005586780 (Parameters_t2452200970 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parameters_Update_m4005586780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Text_t3286458198 * L_0 = __this->get_scoreText_4();
		int32_t* L_1 = __this->get_address_of_Coins_13();
		String_t* L_2 = Int32_ToString_m1286526384(L_1, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_currentCheckPoint_15();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2809334143(NULL /*static, unused*/, L_2, _stringLiteral1059, L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_6);
		Text_t3286458198 * L_7 = __this->get_enemiesText_5();
		int32_t* L_8 = __this->get_address_of_Enemies_14();
		String_t* L_9 = Int32_ToString_m1286526384(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_9);
		int32_t L_10 = __this->get_currentCheckPoint_15();
		V_0 = L_10;
		int32_t L_11 = V_0;
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 0)
		{
			goto IL_0069;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 1)
		{
			goto IL_009a;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 2)
		{
			goto IL_00cb;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 3)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 4)
		{
			goto IL_012d;
		}
	}
	{
		goto IL_016e;
	}

IL_0069:
	{
		__this->set_enemyBulletSpeed_6((4.0f));
		__this->set_enemySpeed_8((6.0f));
		__this->set_enemyShootSpeed_11((2.7f));
		__this->set_spawnCoeficient_16((0.8f));
		goto IL_0173;
	}

IL_009a:
	{
		__this->set_enemyBulletSpeed_6((5.0f));
		__this->set_enemySpeed_8((7.0f));
		__this->set_enemyShootSpeed_11((2.2f));
		__this->set_spawnCoeficient_16((0.7f));
		goto IL_0173;
	}

IL_00cb:
	{
		__this->set_enemyBulletSpeed_6((6.0f));
		__this->set_enemySpeed_8((8.0f));
		__this->set_enemyShootSpeed_11((1.7f));
		__this->set_spawnCoeficient_16((0.6f));
		goto IL_0173;
	}

IL_00fc:
	{
		__this->set_enemyBulletSpeed_6((7.0f));
		__this->set_enemySpeed_8((8.5f));
		__this->set_enemyShootSpeed_11((1.3f));
		__this->set_spawnCoeficient_16((0.5f));
		goto IL_0173;
	}

IL_012d:
	{
		__this->set_enemyBulletSpeed_6((7.5f));
		__this->set_enemySpeed_8((9.0f));
		__this->set_enemyShootSpeed_11((1.1f));
		__this->set_spawnCoeficient_16((0.4f));
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3538920214, (3.0f), /*hidden argument*/NULL);
		goto IL_0173;
	}

IL_016e:
	{
		goto IL_0173;
	}

IL_0173:
	{
		return;
	}
}
// System.Void Parameters::goToNextScene()
extern "C"  void Parameters_goToNextScene_m1511013029 (Parameters_t2452200970 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m946446301(NULL /*static, unused*/, /*hidden argument*/NULL);
		SceneManager_LoadScene_m193744610(NULL /*static, unused*/, ((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseButton::.ctor()
extern "C"  void PauseButton__ctor_m811182019 (PauseButton_t3671462056 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseButton::Start()
extern const MethodInfo* GameObject_GetComponent_TisGUITexture_t63494093_m3829342502_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3671462056;
extern const uint32_t PauseButton_Start_m4053287107_MetadataUsageId;
extern "C"  void PauseButton_Start_m4053287107 (PauseButton_t3671462056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PauseButton_Start_m4053287107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = __this->get_darkGO_7();
		NullCheck(L_0);
		GUITexture_t63494093 * L_1 = GameObject_GetComponent_TisGUITexture_t63494093_m3829342502(L_0, /*hidden argument*/GameObject_GetComponent_TisGUITexture_t63494093_m3829342502_MethodInfo_var);
		__this->set_darkenScreen_9(L_1);
		GameObject_t4012695102 * L_2 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3671462056, /*hidden argument*/NULL);
		NullCheck(L_2);
		AudioSource_t3628549054 * L_3 = GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151(L_2, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151_MethodInfo_var);
		__this->set_playSound_11(L_3);
		__this->set_paused_2((bool)0);
		GameObject_t4012695102 * L_4 = __this->get_pausePanel_4();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_5 = __this->get_exitButton_5();
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_6 = __this->get_tryAgainButton_6();
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseButton::OnPause()
extern "C"  void PauseButton_OnPause_m2350323320 (PauseButton_t3671462056 * __this, const MethodInfo* method)
{
	{
		Time_set_timeScale_m1848691981(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_paused_2((bool)1);
		GameObject_t4012695102 * L_0 = __this->get_pausePanel_4();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = __this->get_exitButton_5();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = __this->get_tryAgainButton_6();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)1, /*hidden argument*/NULL);
		GUITexture_t63494093 * L_3 = __this->get_darkenScreen_9();
		NullCheck(L_3);
		Behaviour_set_enabled_m2046806933(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_4 = __this->get_scoreBoard_8();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_5 = __this->get_playSound_11();
		AudioClip_t3714538611 * L_6 = __this->get_pauseSound_10();
		NullCheck(L_5);
		AudioSource_set_clip_m19502010(L_5, L_6, /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_7 = __this->get_playSound_11();
		NullCheck(L_7);
		AudioSource_Play_m1360558992(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseButton::OnResume()
extern "C"  void PauseButton_OnResume_m2531132685 (PauseButton_t3671462056 * __this, const MethodInfo* method)
{
	{
		Time_set_timeScale_m1848691981(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_paused_2((bool)0);
		GameObject_t4012695102 * L_0 = __this->get_pausePanel_4();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = __this->get_exitButton_5();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = __this->get_tryAgainButton_6();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)0, /*hidden argument*/NULL);
		GUITexture_t63494093 * L_3 = __this->get_darkenScreen_9();
		NullCheck(L_3);
		Behaviour_set_enabled_m2046806933(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_4 = __this->get_scoreBoard_8();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseButton::TryAgain()
extern "C"  void PauseButton_TryAgain_m4206283334 (PauseButton_t3671462056 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m946446301(NULL /*static, unused*/, /*hidden argument*/NULL);
		SceneManager_LoadScene_m193744610(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Time_set_timeScale_m1848691981(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseButton::GoToMenu()
extern "C"  void PauseButton_GoToMenu_m668902947 (PauseButton_t3671462056 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m946446301(NULL /*static, unused*/, /*hidden argument*/NULL);
		SceneManager_LoadScene_m193744610(NULL /*static, unused*/, ((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/NULL);
		Time_set_timeScale_m1848691981(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PetsFollow::.ctor()
extern "C"  void PetsFollow__ctor_m3435573782 (PetsFollow_t690634789 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_7((10.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PetsFollow::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t PetsFollow_Start_m2382711574_MetadataUsageId;
extern "C"  void PetsFollow_Start_m2382711574 (PetsFollow_t690634789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PetsFollow_Start_m2382711574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Random_Range_m75452833(NULL /*static, unused*/, 1, 7, /*hidden argument*/NULL);
		__this->set_randomPos_8((((float)((float)L_0))));
		GameObject_t4012695102 * L_1 = GameObject_FindWithTag_m3162815092(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		__this->set_wayPoint_5(L_1);
		Transform_t284553113 * L_2 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_2);
		Transform_t284553113 * L_3 = __this->get__trans_2();
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_4);
		GameObject_t4012695102 * L_5 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		NullCheck(L_5);
		Parameters_t2452200970 * L_6 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_5, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		NullCheck(L_6);
		float L_7 = L_6->get_enemySpeed_8();
		__this->set_moveSpeed_4(L_7);
		return;
	}
}
// System.Void PetsFollow::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPShoot_t2370192815_m1940435022_MethodInfo_var;
extern const uint32_t PetsFollow_Update_m855466935_MetadataUsageId;
extern "C"  void PetsFollow_Update_m855466935 (PetsFollow_t690634789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PetsFollow_Update_m855466935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	{
		GameObject_t4012695102 * L_0 = __this->get_wayPoint_5();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_015e;
		}
	}
	{
		GameObject_t4012695102 * L_2 = __this->get_wayPoint_5();
		NullCheck(L_2);
		Player_Movement_t3938410573 * L_3 = GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356(L_2, /*hidden argument*/GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356_MethodInfo_var);
		NullCheck(L_3);
		bool L_4 = L_3->get_isPearl_8();
		if (!L_4)
		{
			goto IL_010f;
		}
	}
	{
		GameObject_t4012695102 * L_5 = __this->get_wayPoint_5();
		NullCheck(L_5);
		Transform_t284553113 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3525329789  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		Transform_t284553113 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3525329789  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		float L_10 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		float L_11 = V_0;
		if ((!(((float)L_11) < ((float)(20.0f)))))
		{
			goto IL_010a;
		}
	}
	{
		GameObject_t4012695102 * L_12 = __this->get_wayPoint_5();
		NullCheck(L_12);
		Transform_t284553113 * L_13 = GameObject_get_transform_m1278640159(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3525329789  L_14 = Transform_get_position_m2211398607(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_1)->get_x_1();
		GameObject_t4012695102 * L_16 = __this->get_wayPoint_5();
		NullCheck(L_16);
		Transform_t284553113 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3525329789  L_18 = Transform_get_position_m2211398607(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = (&V_2)->get_y_2();
		float L_20 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_21 = __this->get_randomPos_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_22 = Mathf_PingPong_m763741129(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_23 = __this->get_wayPoint_5();
		NullCheck(L_23);
		Transform_t284553113 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t3525329789  L_25 = Transform_get_position_m2211398607(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = (&V_3)->get_z_3();
		Vector3_t3525329789  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m2926210380(&L_27, ((float)((float)L_15-(float)(2.0f))), ((float)((float)L_19+(float)L_22)), L_26, /*hidden argument*/NULL);
		__this->set_wayPointPos_6(L_27);
		Transform_t284553113 * L_28 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_29 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t3525329789  L_30 = Transform_get_position_m2211398607(L_29, /*hidden argument*/NULL);
		Vector3_t3525329789  L_31 = __this->get_wayPointPos_6();
		float L_32 = __this->get_speed_7();
		float L_33 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_34 = Vector3_MoveTowards_m2405650085(NULL /*static, unused*/, L_30, L_31, ((float)((float)L_32*(float)L_33)), /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_position_m3111394108(L_28, L_34, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_35 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		SpriteRenderer_t2223784725 * L_36 = GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113(L_35, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113_MethodInfo_var);
		NullCheck(L_36);
		SpriteRenderer_set_flipX_m4285205544(L_36, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_37 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		PShoot_t2370192815 * L_38 = GameObject_GetComponent_TisPShoot_t2370192815_m1940435022(L_37, /*hidden argument*/GameObject_GetComponent_TisPShoot_t2370192815_m1940435022_MethodInfo_var);
		NullCheck(L_38);
		Behaviour_set_enabled_m2046806933(L_38, (bool)1, /*hidden argument*/NULL);
	}

IL_010a:
	{
		goto IL_015e;
	}

IL_010f:
	{
		Transform_t284553113 * L_39 = __this->get__trans_2();
		Vector3_t3525329789 * L_40 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_41 = L_40;
		float L_42 = L_41->get_x_1();
		float L_43 = __this->get_moveSpeed_4();
		float L_44 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_45 = ((float)((float)L_42+(float)((float)((float)((float)((float)L_43*(float)L_44))/(float)(2.0f)))));
		V_4 = L_45;
		L_41->set_x_1(L_45);
		float L_46 = V_4;
		Vector3_t3525329789 * L_47 = __this->get_address_of__StartPos_3();
		float L_48 = L_47->get_y_2();
		Vector3_t3525329789 * L_49 = __this->get_address_of__StartPos_3();
		float L_50 = L_49->get_z_3();
		Vector3_t3525329789  L_51;
		memset(&L_51, 0, sizeof(L_51));
		Vector3__ctor_m2926210380(&L_51, L_46, L_48, L_50, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_position_m3111394108(L_39, L_51, /*hidden argument*/NULL);
	}

IL_015e:
	{
		return;
	}
}
// System.Void Player_Movement::.ctor()
extern "C"  void Player_Movement__ctor_m2462805438 (Player_Movement_t3938410573 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player_Movement::Start()
extern const MethodInfo* GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t792326996_m2581074431_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3967544799;
extern Il2CppCodeGenString* _stringLiteral3759572844;
extern Il2CppCodeGenString* _stringLiteral100478661;
extern const uint32_t Player_Movement_Start_m1409943230_MetadataUsageId;
extern "C"  void Player_Movement_Start_m1409943230 (Player_Movement_t3938410573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_Movement_Start_m1409943230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3967544799, /*hidden argument*/NULL);
		__this->set_MoveSpeedBeardy_4(L_0);
		GameObject_t4012695102 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3759572844, /*hidden argument*/NULL);
		NullCheck(L_1);
		FrostEffect_t3874425173 * L_2 = GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324(L_1, /*hidden argument*/GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324_MethodInfo_var);
		__this->set_frostEffect_9(L_2);
		int32_t L_3 = Application_get_loadedLevel_m946446301(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0046;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Animator_t792326996 * L_5 = GameObject_GetComponent_TisAnimator_t792326996_m2581074431(L_4, /*hidden argument*/GameObject_GetComponent_TisAnimator_t792326996_m2581074431_MethodInfo_var);
		NullCheck(L_5);
		Animator_SetBool_m2336836203(L_5, _stringLiteral100478661, (bool)1, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void Player_Movement::Update()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1636471135;
extern Il2CppCodeGenString* _stringLiteral3727713878;
extern Il2CppCodeGenString* _stringLiteral3448012360;
extern const uint32_t Player_Movement_Update_m764419343_MetadataUsageId;
extern "C"  void Player_Movement_Update_m764419343 (Player_Movement_t3938410573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_Movement_Update_m764419343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3525329788  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t1603883884  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t1603883884  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Touch_t1603883884  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Touch_t1603883884  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		bool L_0 = __this->get_isDrunk_5();
		if (L_0)
		{
			goto IL_0071;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0071;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_2 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = Touch_get_phase_m3314549414((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0071;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_4 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_3 = L_4;
		Vector2_t3525329788  L_5 = Touch_get_deltaPosition_m3983677995((&V_3), /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t284553113 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_7 = (&V_0)->get_x_1();
		float L_8 = __this->get_MoveSpeedBeardy_4();
		float L_9 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = (&V_0)->get_y_2();
		float L_11 = __this->get_MoveSpeedBeardy_4();
		float L_12 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_Translate_m2900276092(L_6, ((float)((float)L_7*(float)((float)((float)L_8*(float)L_9)))), ((float)((float)L_10*(float)((float)((float)L_11*(float)L_12)))), (0.0f), /*hidden argument*/NULL);
	}

IL_0071:
	{
		bool L_13 = __this->get_isDrunk_5();
		if (!L_13)
		{
			goto IL_00f6;
		}
	}
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1636471135, (10.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_14 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_00f6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_15 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_4 = L_15;
		int32_t L_16 = Touch_get_phase_m3314549414((&V_4), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_00f6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_17 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_5 = L_17;
		Vector2_t3525329788  L_18 = Touch_get_deltaPosition_m3983677995((&V_5), /*hidden argument*/NULL);
		V_1 = L_18;
		Transform_t284553113 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_20 = (&V_1)->get_x_1();
		float L_21 = __this->get_MoveSpeedBeardy_4();
		float L_22 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = (&V_1)->get_y_2();
		float L_24 = __this->get_MoveSpeedBeardy_4();
		float L_25 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_Translate_m2900276092(L_19, ((float)((float)((-L_20))*(float)((float)((float)L_21*(float)L_22)))), ((float)((float)((-L_23))*(float)((float)((float)L_24*(float)L_25)))), (0.0f), /*hidden argument*/NULL);
	}

IL_00f6:
	{
		bool L_26 = __this->get_isManiac_7();
		if (!L_26)
		{
			goto IL_0118;
		}
	}
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3727713878, (10.0f), /*hidden argument*/NULL);
		__this->set_isManiac_7((bool)0);
	}

IL_0118:
	{
		bool L_27 = __this->get_isChrono_6();
		if (!L_27)
		{
			goto IL_013a;
		}
	}
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3448012360, (10.0f), /*hidden argument*/NULL);
		__this->set_isChrono_6((bool)0);
	}

IL_013a:
	{
		return;
	}
}
// System.Void Player_Movement::timerManiac()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t262790558_m262378119_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t792326996_m2581074431_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2003027519;
extern Il2CppCodeGenString* _stringLiteral2977462423;
extern Il2CppCodeGenString* _stringLiteral3887574139;
extern const uint32_t Player_Movement_timerManiac_m2031532946_MetadataUsageId;
extern "C"  void Player_Movement_timerManiac_m2031532946 (Player_Movement_t3938410573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_Movement_timerManiac_m2031532946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2003027519, /*hidden argument*/NULL);
		FrostEffect_t3874425173 * L_0 = __this->get_frostEffect_9();
		NullCheck(L_0);
		Behaviour_set_enabled_m2046806933(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2977462423, /*hidden argument*/NULL);
		NullCheck(L_1);
		BoxCollider2D_t262790558 * L_2 = GameObject_GetComponent_TisBoxCollider2D_t262790558_m262378119(L_1, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t262790558_m262378119_MethodInfo_var);
		NullCheck(L_2);
		Behaviour_set_enabled_m2046806933(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Animator_t792326996 * L_4 = GameObject_GetComponent_TisAnimator_t792326996_m2581074431(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_t792326996_m2581074431_MethodInfo_var);
		NullCheck(L_4);
		Animator_SetBool_m2336836203(L_4, _stringLiteral3887574139, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player_Movement::timerDrunk()
extern "C"  void Player_Movement_timerDrunk_m607062405 (Player_Movement_t3938410573 * __this, const MethodInfo* method)
{
	{
		__this->set_isDrunk_5((bool)0);
		FrostEffect_t3874425173 * L_0 = __this->get_frostEffect_9();
		NullCheck(L_0);
		L_0->set_FrostAmount_2((0.32f));
		FrostEffect_t3874425173 * L_1 = __this->get_frostEffect_9();
		NullCheck(L_1);
		L_1->set_seethroughness_6((0.1f));
		FrostEffect_t3874425173 * L_2 = __this->get_frostEffect_9();
		NullCheck(L_2);
		L_2->set_distortion_7((0.02f));
		FrostEffect_t3874425173 * L_3 = __this->get_frostEffect_9();
		NullCheck(L_3);
		Behaviour_set_enabled_m2046806933(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player_Movement::timerChrono()
extern "C"  void Player_Movement_timerChrono_m3821313796 (Player_Movement_t3938410573 * __this, const MethodInfo* method)
{
	{
		Time_set_timeScale_m1848691981(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player_Movement::OnTriggerEnter2D(UnityEngine.Collider2D)
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral76987430;
extern Il2CppCodeGenString* _stringLiteral414055392;
extern const uint32_t Player_Movement_OnTriggerEnter2D_m2494488954_MetadataUsageId;
extern "C"  void Player_Movement_OnTriggerEnter2D_m2494488954 (Player_Movement_t3938410573 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_Movement_OnTriggerEnter2D_m2494488954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider2D_t1890038195 * L_0 = ___target;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m305486283(L_0, _stringLiteral76987430, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		__this->set_isPearl_8((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral414055392, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void PowerUpEffect::.ctor()
extern "C"  void PowerUpEffect__ctor_m3321759866 (PowerUpEffect_t2596559889 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PowerUpEffect::Start()
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral79854923;
extern const uint32_t PowerUpEffect_Start_m2268897658_MetadataUsageId;
extern "C"  void PowerUpEffect_Start_m2268897658 (PowerUpEffect_t2596559889 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PowerUpEffect_Start_m2268897658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral79854923, /*hidden argument*/NULL);
		__this->set_Shine_4(L_0);
		GameObject_t4012695102 * L_1 = __this->get_Shine_4();
		NullCheck(L_1);
		SpriteRenderer_t2223784725 * L_2 = GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113(L_1, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113_MethodInfo_var);
		__this->set_changeEffect_2(L_2);
		Color_t1588175760  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m2252924356(&L_3, (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_FadeColor_3(L_3);
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_set_layer_m1872241535(L_4, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PowerUpEffect::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral769484670;
extern Il2CppCodeGenString* _stringLiteral1708706678;
extern const uint32_t PowerUpEffect_Update_m1622202835_MetadataUsageId;
extern "C"  void PowerUpEffect_Update_m1622202835 (PowerUpEffect_t2596559889 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PowerUpEffect_Update_m1622202835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t1588175760  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t1588175760  L_1 = __this->get_FadeColor_3();
		float L_2 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_3 = Mathf_PingPong_m763741129(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Color_t1588175760  L_4 = Color_Lerp_m3494628845(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		SpriteRenderer_t2223784725 * L_5 = __this->get_changeEffect_2();
		Color_t1588175760  L_6 = V_0;
		NullCheck(L_5);
		SpriteRenderer_set_color_m2701854973(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m3709440845(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, _stringLiteral769484670, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		GameObject_t4012695102 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m3709440845(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_11, _stringLiteral1708706678, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_009f;
		}
	}

IL_0060:
	{
		Transform_t284553113 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_14 = L_13;
		NullCheck(L_14);
		Vector3_t3525329789  L_15 = Transform_get_localScale_m3886572677(L_14, /*hidden argument*/NULL);
		Vector3_t3525329789  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2926210380(&L_16, (0.03f), (0.03f), (0.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_17 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localScale_m310756934(L_14, L_17, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_18 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_18, (0.8f), /*hidden argument*/NULL);
		goto IL_00d9;
	}

IL_009f:
	{
		Transform_t284553113 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_20 = L_19;
		NullCheck(L_20);
		Vector3_t3525329789  L_21 = Transform_get_localScale_m3886572677(L_20, /*hidden argument*/NULL);
		Vector3_t3525329789  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2926210380(&L_22, (0.1f), (0.1f), (0.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_23 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localScale_m310756934(L_20, L_23, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_24 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_24, (0.8f), /*hidden argument*/NULL);
	}

IL_00d9:
	{
		return;
	}
}
// System.Void PowerUpMove::.ctor()
extern "C"  void PowerUpMove__ctor_m1159628858 (PowerUpMove_t2161606225 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PowerUpMove::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t PowerUpMove_Start_m106766650_MetadataUsageId;
extern "C"  void PowerUpMove_Start_m106766650 (PowerUpMove_t2161606225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PowerUpMove_Start_m106766650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_3(L_0);
		Transform_t284553113 * L_1 = __this->get__trans_3();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set__startingPos_2(L_2);
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_8(L_3);
		GameObject_t4012695102 * L_4 = __this->get_go_8();
		NullCheck(L_4);
		Parameters_t2452200970 * L_5 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_4, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_9(L_5);
		float L_6 = Random_Range_m3362417303(NULL /*static, unused*/, (1.0f), (4.0f), /*hidden argument*/NULL);
		__this->set_randomPos_5(L_6);
		__this->set_moveSpeed_4((3.0f));
		Camera_t3533968274 * L_7 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, (((float)((float)((int32_t)((int32_t)L_8/(int32_t)2))))), (((float)((float)((int32_t)((int32_t)L_9/(int32_t)2))))), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3525329789  L_11 = Camera_ScreenToWorldPoint_m1572306334(L_7, L_10, /*hidden argument*/NULL);
		__this->set_centerOfScreen_7(L_11);
		return;
	}
}
// System.Void PowerUpMove::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t PowerUpMove_Update_m3315618323_MetadataUsageId;
extern "C"  void PowerUpMove_Update_m3315618323 (PowerUpMove_t2161606225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PowerUpMove_Update_m3315618323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_3();
		Vector3_t3525329789 * L_1 = __this->get_address_of__startingPos_2();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))*(float)(2.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__startingPos_2();
		float L_9 = L_8->get_y_2();
		float L_10 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = __this->get_randomPos_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_12 = Mathf_PingPong_m763741129(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Vector3_t3525329789 * L_13 = __this->get_address_of__startingPos_2();
		float L_14 = L_13->get_z_3();
		Vector3_t3525329789  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector3__ctor_m2926210380(&L_15, L_7, ((float)((float)L_9+(float)L_12)), L_14, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_15, /*hidden argument*/NULL);
		Vector3_t3525329789 * L_16 = __this->get_address_of_myScale_6();
		L_16->set_x_1((7.0f));
		return;
	}
}
// System.Void PowerUpMove::OnTriggerEnter2D(UnityEngine.Collider2D)
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t792326996_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t262790558_m262378119_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2875670398;
extern Il2CppCodeGenString* _stringLiteral3721122659;
extern Il2CppCodeGenString* _stringLiteral4244527627;
extern Il2CppCodeGenString* _stringLiteral677068266;
extern Il2CppCodeGenString* _stringLiteral2107379594;
extern Il2CppCodeGenString* _stringLiteral3086617355;
extern Il2CppCodeGenString* _stringLiteral3300234507;
extern Il2CppCodeGenString* _stringLiteral2475494281;
extern Il2CppCodeGenString* _stringLiteral2482852845;
extern Il2CppCodeGenString* _stringLiteral3981905976;
extern Il2CppCodeGenString* _stringLiteral3536747986;
extern Il2CppCodeGenString* _stringLiteral2706072738;
extern Il2CppCodeGenString* _stringLiteral2792256045;
extern Il2CppCodeGenString* _stringLiteral1985802089;
extern Il2CppCodeGenString* _stringLiteral3887574139;
extern Il2CppCodeGenString* _stringLiteral3759572844;
extern Il2CppCodeGenString* _stringLiteral2977462423;
extern Il2CppCodeGenString* _stringLiteral1105209358;
extern Il2CppCodeGenString* _stringLiteral1995605478;
extern Il2CppCodeGenString* _stringLiteral3554891170;
extern Il2CppCodeGenString* _stringLiteral719253920;
extern Il2CppCodeGenString* _stringLiteral2426371220;
extern Il2CppCodeGenString* _stringLiteral244032353;
extern const uint32_t PowerUpMove_OnTriggerEnter2D_m2383021694_MetadataUsageId;
extern "C"  void PowerUpMove_OnTriggerEnter2D_m2383021694 (PowerUpMove_t2161606225 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PowerUpMove_OnTriggerEnter2D_m2383021694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	GameObject_t4012695102 * V_1 = NULL;
	GameObject_t4012695102 * V_2 = NULL;
	GameObject_t4012695102 * V_3 = NULL;
	GameObject_t4012695102 * V_4 = NULL;
	GameObject_t4012695102 * V_5 = NULL;
	{
		Collider2D_t1890038195 * L_0 = ___target;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m305486283(L_0, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0084;
		}
	}
	{
		GameObject_t4012695102 * L_2 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m3709440845(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_3, _stringLiteral2875670398, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0084;
		}
	}
	{
		Collider2D_t1890038195 * L_5 = ___target;
		NullCheck(L_5);
		bool L_6 = Component_CompareTag_m305486283(L_5, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0084;
		}
	}
	{
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_9 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral4244527627, L_8, /*hidden argument*/NULL);
		Transform_t284553113 * L_10 = __this->get__trans_3();
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_12 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_13 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_9, L_11, L_12, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)CastclassSealed(L_13, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_14 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_14, (1.0f), /*hidden argument*/NULL);
		goto IL_04a7;
	}

IL_0084:
	{
		Collider2D_t1890038195 * L_15 = ___target;
		NullCheck(L_15);
		bool L_16 = Component_CompareTag_m305486283(L_15, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ae;
		}
	}
	{
		GameObject_t4012695102 * L_17 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = Object_get_name_m3709440845(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_18, _stringLiteral677068266, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00c8;
		}
	}

IL_00ae:
	{
		GameObject_t4012695102 * L_20 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = Object_get_name_m3709440845(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_21, _stringLiteral2107379594, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_012d;
		}
	}

IL_00c8:
	{
		Collider2D_t1890038195 * L_23 = ___target;
		NullCheck(L_23);
		bool L_24 = Component_CompareTag_m305486283(L_23, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_012d;
		}
	}
	{
		GameObject_t4012695102 * L_25 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_27 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3086617355, L_26, /*hidden argument*/NULL);
		Vector3_t3525329789  L_28 = __this->get_centerOfScreen_7();
		Quaternion_t1891715979  L_29 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_30 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		V_1 = ((GameObject_t4012695102 *)CastclassSealed(L_30, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_31 = V_1;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_31, (1.0f), /*hidden argument*/NULL);
		Parameters_t2452200970 * L_32 = __this->get_parameters_9();
		NullCheck(L_32);
		L_32->set_playerShootSpeed_10((1.0f));
		goto IL_04a7;
	}

IL_012d:
	{
		Collider2D_t1890038195 * L_33 = ___target;
		NullCheck(L_33);
		bool L_34 = Component_CompareTag_m305486283(L_33, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0157;
		}
	}
	{
		GameObject_t4012695102 * L_35 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m3709440845(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_37 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_36, _stringLiteral3300234507, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_0171;
		}
	}

IL_0157:
	{
		GameObject_t4012695102 * L_38 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		String_t* L_39 = Object_get_name_m3709440845(L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_40 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_39, _stringLiteral2475494281, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_01f0;
		}
	}

IL_0171:
	{
		Collider2D_t1890038195 * L_41 = ___target;
		NullCheck(L_41);
		bool L_42 = Component_CompareTag_m305486283(L_41, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_01f0;
		}
	}
	{
		GameObject_t4012695102 * L_43 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2482852845, /*hidden argument*/NULL);
		NullCheck(L_43);
		MeshRenderer_t1217738301 * L_44 = GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(L_43, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var);
		NullCheck(L_44);
		Renderer_set_enabled_m2514140131(L_44, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_45 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2482852845, /*hidden argument*/NULL);
		NullCheck(L_45);
		CircleCollider2D_t594472611 * L_46 = GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224(L_45, /*hidden argument*/GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224_MethodInfo_var);
		NullCheck(L_46);
		Behaviour_set_enabled_m2046806933(L_46, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_47 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_48 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_49 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3981905976, L_48, /*hidden argument*/NULL);
		Vector3_t3525329789  L_50 = __this->get_centerOfScreen_7();
		Quaternion_t1891715979  L_51 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_52 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_49, L_50, L_51, /*hidden argument*/NULL);
		V_2 = ((GameObject_t4012695102 *)CastclassSealed(L_52, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_53 = V_2;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_53, (1.0f), /*hidden argument*/NULL);
		goto IL_04a7;
	}

IL_01f0:
	{
		Collider2D_t1890038195 * L_54 = ___target;
		NullCheck(L_54);
		bool L_55 = Component_CompareTag_m305486283(L_54, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_021a;
		}
	}
	{
		GameObject_t4012695102 * L_56 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		String_t* L_57 = Object_get_name_m3709440845(L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_57, _stringLiteral3536747986, /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_0234;
		}
	}

IL_021a:
	{
		GameObject_t4012695102 * L_59 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		String_t* L_60 = Object_get_name_m3709440845(L_59, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_61 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_60, _stringLiteral2706072738, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_02e2;
		}
	}

IL_0234:
	{
		Collider2D_t1890038195 * L_62 = ___target;
		NullCheck(L_62);
		bool L_63 = Component_CompareTag_m305486283(L_62, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_02e2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_64 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_65 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2792256045, L_64, /*hidden argument*/NULL);
		Vector3_t3525329789  L_66 = __this->get_centerOfScreen_7();
		Quaternion_t1891715979  L_67 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_68 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_65, L_66, L_67, /*hidden argument*/NULL);
		V_3 = ((GameObject_t4012695102 *)CastclassSealed(L_68, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_69 = V_3;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_69, (1.0f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_70 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1985802089, /*hidden argument*/NULL);
		NullCheck(L_70);
		Player_Movement_t3938410573 * L_71 = GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356(L_70, /*hidden argument*/GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356_MethodInfo_var);
		NullCheck(L_71);
		L_71->set_isManiac_7((bool)1);
		GameObject_t4012695102 * L_72 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1985802089, /*hidden argument*/NULL);
		NullCheck(L_72);
		Animator_t792326996 * L_73 = GameObject_GetComponent_TisAnimator_t792326996_m2581074431(L_72, /*hidden argument*/GameObject_GetComponent_TisAnimator_t792326996_m2581074431_MethodInfo_var);
		NullCheck(L_73);
		Animator_SetBool_m2336836203(L_73, _stringLiteral3887574139, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_74 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3759572844, /*hidden argument*/NULL);
		NullCheck(L_74);
		FrostEffect_t3874425173 * L_75 = GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324(L_74, /*hidden argument*/GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324_MethodInfo_var);
		NullCheck(L_75);
		Behaviour_set_enabled_m2046806933(L_75, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_76 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2977462423, /*hidden argument*/NULL);
		NullCheck(L_76);
		BoxCollider2D_t262790558 * L_77 = GameObject_GetComponent_TisBoxCollider2D_t262790558_m262378119(L_76, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t262790558_m262378119_MethodInfo_var);
		NullCheck(L_77);
		Behaviour_set_enabled_m2046806933(L_77, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_78 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		goto IL_04a7;
	}

IL_02e2:
	{
		Collider2D_t1890038195 * L_79 = ___target;
		NullCheck(L_79);
		bool L_80 = Component_CompareTag_m305486283(L_79, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_030c;
		}
	}
	{
		GameObject_t4012695102 * L_81 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_81);
		String_t* L_82 = Object_get_name_m3709440845(L_81, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_83 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_82, _stringLiteral1105209358, /*hidden argument*/NULL);
		if (L_83)
		{
			goto IL_0326;
		}
	}

IL_030c:
	{
		GameObject_t4012695102 * L_84 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_84);
		String_t* L_85 = Object_get_name_m3709440845(L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_86 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_85, _stringLiteral1995605478, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_03f2;
		}
	}

IL_0326:
	{
		Collider2D_t1890038195 * L_87 = ___target;
		NullCheck(L_87);
		bool L_88 = Component_CompareTag_m305486283(L_87, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (L_88)
		{
			goto IL_03f2;
		}
	}
	{
		GameObject_t4012695102 * L_89 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_90 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_91 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3554891170, L_90, /*hidden argument*/NULL);
		Vector3_t3525329789  L_92 = __this->get_centerOfScreen_7();
		Quaternion_t1891715979  L_93 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_94 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_91, L_92, L_93, /*hidden argument*/NULL);
		V_4 = ((GameObject_t4012695102 *)CastclassSealed(L_94, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_95 = V_4;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_95, (1.0f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_96 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3759572844, /*hidden argument*/NULL);
		NullCheck(L_96);
		FrostEffect_t3874425173 * L_97 = GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324(L_96, /*hidden argument*/GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324_MethodInfo_var);
		NullCheck(L_97);
		L_97->set_FrostAmount_2((0.5f));
		GameObject_t4012695102 * L_98 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3759572844, /*hidden argument*/NULL);
		NullCheck(L_98);
		FrostEffect_t3874425173 * L_99 = GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324(L_98, /*hidden argument*/GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324_MethodInfo_var);
		NullCheck(L_99);
		L_99->set_seethroughness_6((0.7f));
		GameObject_t4012695102 * L_100 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3759572844, /*hidden argument*/NULL);
		NullCheck(L_100);
		FrostEffect_t3874425173 * L_101 = GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324(L_100, /*hidden argument*/GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324_MethodInfo_var);
		NullCheck(L_101);
		L_101->set_distortion_7((0.08f));
		GameObject_t4012695102 * L_102 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3759572844, /*hidden argument*/NULL);
		NullCheck(L_102);
		FrostEffect_t3874425173 * L_103 = GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324(L_102, /*hidden argument*/GameObject_GetComponent_TisFrostEffect_t3874425173_m199049324_MethodInfo_var);
		NullCheck(L_103);
		Behaviour_set_enabled_m2046806933(L_103, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_104 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1985802089, /*hidden argument*/NULL);
		NullCheck(L_104);
		Player_Movement_t3938410573 * L_105 = GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356(L_104, /*hidden argument*/GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356_MethodInfo_var);
		NullCheck(L_105);
		L_105->set_isDrunk_5((bool)1);
		goto IL_04a7;
	}

IL_03f2:
	{
		Collider2D_t1890038195 * L_106 = ___target;
		NullCheck(L_106);
		bool L_107 = Component_CompareTag_m305486283(L_106, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_107)
		{
			goto IL_041c;
		}
	}
	{
		GameObject_t4012695102 * L_108 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_108);
		String_t* L_109 = Object_get_name_m3709440845(L_108, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_110 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_109, _stringLiteral719253920, /*hidden argument*/NULL);
		if (L_110)
		{
			goto IL_0436;
		}
	}

IL_041c:
	{
		GameObject_t4012695102 * L_111 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_111);
		String_t* L_112 = Object_get_name_m3709440845(L_111, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_113 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_112, _stringLiteral2426371220, /*hidden argument*/NULL);
		if (!L_113)
		{
			goto IL_04a7;
		}
	}

IL_0436:
	{
		Collider2D_t1890038195 * L_114 = ___target;
		NullCheck(L_114);
		bool L_115 = Component_CompareTag_m305486283(L_114, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (L_115)
		{
			goto IL_04a7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_116 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_117 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral244032353, L_116, /*hidden argument*/NULL);
		Vector3_t3525329789  L_118 = __this->get_centerOfScreen_7();
		Quaternion_t1891715979  L_119 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_120 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_117, L_118, L_119, /*hidden argument*/NULL);
		V_5 = ((GameObject_t4012695102 *)CastclassSealed(L_120, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_121 = V_5;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_121, (0.5f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_122 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1985802089, /*hidden argument*/NULL);
		NullCheck(L_122);
		Player_Movement_t3938410573 * L_123 = GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356(L_122, /*hidden argument*/GameObject_GetComponent_TisPlayer_Movement_t3938410573_m2030634356_MethodInfo_var);
		NullCheck(L_123);
		L_123->set_isChrono_6((bool)1);
		GameObject_t4012695102 * L_124 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_124, /*hidden argument*/NULL);
		Time_set_timeScale_m1848691981(NULL /*static, unused*/, (0.5f), /*hidden argument*/NULL);
	}

IL_04a7:
	{
		return;
	}
}
// System.Void PShoot::.ctor()
extern "C"  void PShoot__ctor_m393417676 (PShoot_t2370192815 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PShoot::Start()
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern Il2CppCodeGenString* _stringLiteral79860735;
extern const uint32_t PShoot_Start_m3635522764_MetadataUsageId;
extern "C"  void PShoot_Start_m3635522764 (PShoot_t2370192815 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PShoot_Start_m3635522764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_3(L_0);
		GameObject_t4012695102 * L_1 = __this->get_go_3();
		NullCheck(L_1);
		Parameters_t2452200970 * L_2 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_1, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_4(L_2);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral79860735, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PShoot::Update()
extern "C"  void PShoot_Update_m1037908161 (PShoot_t2370192815 * __this, const MethodInfo* method)
{
	{
		Parameters_t2452200970 * L_0 = __this->get_parameters_4();
		NullCheck(L_0);
		float L_1 = L_0->get_playerShootSpeed_10();
		__this->set_moveSpeed_2(L_1);
		return;
	}
}
// System.Void PShoot::Shoot()
extern const Il2CppType* Object_t3878351788_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral914869486;
extern Il2CppCodeGenString* _stringLiteral3040892166;
extern Il2CppCodeGenString* _stringLiteral3029739;
extern Il2CppCodeGenString* _stringLiteral79860735;
extern const uint32_t PShoot_Shoot_m3304812873_MetadataUsageId;
extern "C"  void PShoot_Shoot_m3304812873 (PShoot_t2370192815 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PShoot_Shoot_m3304812873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral914869486, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_3 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3040892166, L_2, /*hidden argument*/NULL);
		Transform_t284553113 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, (-0.8f), (-2.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_8 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_3, L_7, L_8, /*hidden argument*/NULL);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_10 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3040892166, L_9, /*hidden argument*/NULL);
		Transform_t284553113 * L_11 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t3525329789  L_12 = Transform_get_position_m2211398607(L_11, /*hidden argument*/NULL);
		Vector3_t3525329789  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2926210380(&L_13, (-0.8f), (2.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_14 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_15 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_10, L_14, L_15, /*hidden argument*/NULL);
		goto IL_010d;
	}

IL_00a0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_17 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3040892166, L_16, /*hidden argument*/NULL);
		Transform_t284553113 * L_18 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t3525329789  L_19 = Transform_get_position_m2211398607(L_18, /*hidden argument*/NULL);
		Vector3_t3525329789  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2926210380(&L_20, (-0.8f), (-0.8f), (1.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_21 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_22 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_17, L_21, L_22, /*hidden argument*/NULL);
		Type_t * L_23 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3878351788_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_24 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3029739, L_23, /*hidden argument*/NULL);
		Transform_t284553113 * L_25 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t3525329789  L_26 = Transform_get_position_m2211398607(L_25, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_27 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_24, L_26, L_27, /*hidden argument*/NULL);
	}

IL_010d:
	{
		float L_28 = __this->get_moveSpeed_2();
		float L_29 = Time_get_timeScale_m1970669766(NULL /*static, unused*/, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral79860735, ((float)((float)L_28*(float)L_29)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScrollBG::.ctor()
extern "C"  void ScrollBG__ctor_m1600731881 (ScrollBG_t3957445618 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScrollBG::Update()
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t1217738301_m3731621022_MethodInfo_var;
extern const uint32_t ScrollBG_Update_m4104910148_MetadataUsageId;
extern "C"  void ScrollBG_Update_m4104910148 (ScrollBG_t3957445618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScrollBG_Update_m4104910148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t1217738301 * V_0 = NULL;
	Material_t1886596500 * V_1 = NULL;
	Vector2_t3525329788  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		MeshRenderer_t1217738301 * L_0 = Component_GetComponent_TisMeshRenderer_t1217738301_m3731621022(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t1217738301_m3731621022_MethodInfo_var);
		V_0 = L_0;
		MeshRenderer_t1217738301 * L_1 = V_0;
		NullCheck(L_1);
		Material_t1886596500 * L_2 = Renderer_get_material_m2720864603(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Material_t1886596500 * L_3 = V_1;
		NullCheck(L_3);
		Vector2_t3525329788  L_4 = Material_get_mainTextureOffset_m3247688085(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		Vector2_t3525329788 * L_5 = (&V_2);
		float L_6 = L_5->get_x_1();
		float L_7 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_5->set_x_1(((float)((float)L_6+(float)((float)((float)L_7/(float)(120.0f))))));
		Material_t1886596500 * L_8 = V_1;
		Vector2_t3525329788  L_9 = V_2;
		NullCheck(L_8);
		Material_set_mainTextureOffset_m3397882654(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SeaShellMovement::.ctor()
extern "C"  void SeaShellMovement__ctor_m4170356107 (SeaShellMovement_t4149019408 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SeaShellMovement::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t SeaShellMovement_Start_m3117493899_MetadataUsageId;
extern "C"  void SeaShellMovement_Start_m3117493899 (SeaShellMovement_t4149019408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SeaShellMovement_Start_m3117493899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_0);
		Transform_t284553113 * L_1 = __this->get__trans_2();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_2);
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		NullCheck(L_3);
		Parameters_t2452200970 * L_4 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_3, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		NullCheck(L_4);
		float L_5 = L_4->get_enemySpeed_8();
		__this->set_moveSpeed_4(L_5);
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		SpriteRenderer_t2223784725 * L_7 = GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113(L_6, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113_MethodInfo_var);
		__this->set_spriteName_5(L_7);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		CircleCollider2D_t594472611 * L_9 = GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224(L_8, /*hidden argument*/GameObject_GetComponent_TisCircleCollider2D_t594472611_m3236006224_MethodInfo_var);
		__this->set_collider_6(L_9);
		return;
	}
}
// System.Void SeaShellMovement::Update()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral117638123;
extern const uint32_t SeaShellMovement_Update_m2158882530_MetadataUsageId;
extern "C"  void SeaShellMovement_Update_m2158882530 (SeaShellMovement_t4149019408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SeaShellMovement_Update_m2158882530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_2();
		Vector3_t3525329789 * L_1 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))/(float)(8.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__StartPos_3();
		float L_9 = L_8->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of__StartPos_3();
		float L_11 = L_10->get_z_3();
		Vector3_t3525329789  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, L_7, L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_12, /*hidden argument*/NULL);
		SpriteRenderer_t2223784725 * L_13 = __this->get_spriteName_5();
		NullCheck(L_13);
		Sprite_t4006040370 * L_14 = SpriteRenderer_get_sprite_m3481747968(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m3709440845(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, _stringLiteral117638123, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_007d;
		}
	}
	{
		CircleCollider2D_t594472611 * L_17 = __this->get_collider_6();
		NullCheck(L_17);
		Behaviour_set_enabled_m2046806933(L_17, (bool)1, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_007d:
	{
		CircleCollider2D_t594472611 * L_18 = __this->get_collider_6();
		NullCheck(L_18);
		Behaviour_set_enabled_m2046806933(L_18, (bool)0, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void SeaSpikyMovement::.ctor()
extern "C"  void SeaSpikyMovement__ctor_m698332385 (SeaSpikyMovement_t463284986 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SeaSpikyMovement::Start()
extern "C"  void SeaSpikyMovement_Start_m3940437473 (SeaSpikyMovement_t463284986 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_0, (7.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShieldUvAnimation::.ctor()
extern "C"  void ShieldUvAnimation__ctor_m2563976209 (ShieldUvAnimation_t644239002 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShieldUvAnimation::Start()
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t1092684080_m4102086307_MethodInfo_var;
extern const uint32_t ShieldUvAnimation_Start_m1511114001_MetadataUsageId;
extern "C"  void ShieldUvAnimation_Start_m1511114001 (ShieldUvAnimation_t644239002 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShieldUvAnimation_Start_m1511114001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = __this->get_iShield_2();
		NullCheck(L_0);
		Renderer_t1092684080 * L_1 = GameObject_GetComponent_TisRenderer_t1092684080_m4102086307(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t1092684080_m4102086307_MethodInfo_var);
		NullCheck(L_1);
		Material_t1886596500 * L_2 = Renderer_get_material_m2720864603(L_1, /*hidden argument*/NULL);
		__this->set_mMaterial_4(L_2);
		__this->set_mTime_5((0.0f));
		return;
	}
}
// System.Void ShieldUvAnimation::Update()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral772558290;
extern const uint32_t ShieldUvAnimation_Update_m3900713244_MetadataUsageId;
extern "C"  void ShieldUvAnimation_Update_m3900713244 (ShieldUvAnimation_t644239002 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShieldUvAnimation_Update_m3900713244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_mTime_5();
		float L_1 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_iSpeed_3();
		__this->set_mTime_5(((float)((float)L_0+(float)((float)((float)L_1*(float)L_2)))));
		Material_t1886596500 * L_3 = __this->get_mMaterial_4();
		float L_4 = __this->get_mTime_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Repeat_m3424250200(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Material_SetFloat_m981710063(L_3, _stringLiteral772558290, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void sniperBulletFly::.ctor()
extern "C"  void sniperBulletFly__ctor_m3215369673 (sniperBulletFly_t3858882530 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void sniperBulletFly::Start()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const uint32_t sniperBulletFly_Start_m2162507465_MetadataUsageId;
extern "C"  void sniperBulletFly_Start_m2162507465 (sniperBulletFly_t3858882530 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (sniperBulletFly_Start_m2162507465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set__trans_2(L_0);
		Transform_t284553113 * L_1 = __this->get__trans_2();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set__StartPos_3(L_2);
		__this->set_moveSpeed_4((5.0f));
		return;
	}
}
// System.Void sniperBulletFly::Update()
extern "C"  void sniperBulletFly_Update_m2619074148 (sniperBulletFly_t3858882530 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get__trans_2();
		Vector3_t3525329789 * L_1 = __this->get_address_of__StartPos_3();
		Vector3_t3525329789 * L_2 = L_1;
		float L_3 = L_2->get_x_1();
		float L_4 = __this->get_moveSpeed_4();
		float L_5 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_3-(float)((float)((float)((float)((float)L_4*(float)L_5))*(float)(12.0f)))));
		V_0 = L_6;
		L_2->set_x_1(L_6);
		float L_7 = V_0;
		Vector3_t3525329789 * L_8 = __this->get_address_of__StartPos_3();
		float L_9 = L_8->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of__StartPos_3();
		float L_11 = L_10->get_z_3();
		Vector3_t3525329789  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, L_7, L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void sniperBulletFly::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppCodeGenString* _stringLiteral3721122659;
extern Il2CppCodeGenString* _stringLiteral67100520;
extern const uint32_t sniperBulletFly_OnTriggerEnter2D_m1115166415_MetadataUsageId;
extern "C"  void sniperBulletFly_OnTriggerEnter2D_m1115166415 (sniperBulletFly_t3858882530 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (sniperBulletFly_OnTriggerEnter2D_m1115166415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral3721122659, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		Collider2D_t1890038195 * L_2 = ___target;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m305486283(L_2, _stringLiteral67100520, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void SpawnCoins::.ctor()
extern "C"  void SpawnCoins__ctor_m3091868436 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::Start()
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1630969879;
extern Il2CppCodeGenString* _stringLiteral220500613;
extern const uint32_t SpawnCoins_Start_m2039006228_MetadataUsageId;
extern "C"  void SpawnCoins_Start_m2039006228 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_Start_m2039006228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t3518514978  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Bounds_t3518514978  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		BoxCollider2D_t262790558 * L_0 = Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var);
		__this->set_myBox_2(L_0);
		BoxCollider2D_t262790558 * L_1 = __this->get_myBox_2();
		NullCheck(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_y_2();
		BoxCollider2D_t262790558 * L_5 = __this->get_myBox_2();
		NullCheck(L_5);
		Bounds_t3518514978  L_6 = Collider2D_get_bounds_m1087503006(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t3525329789  L_7 = Bounds_get_size_m3666348432((&V_1), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_2();
		__this->set_x1_6(((float)((float)L_4-(float)((float)((float)L_8/(float)(2.0f))))));
		BoxCollider2D_t262790558 * L_9 = __this->get_myBox_2();
		NullCheck(L_9);
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_y_2();
		BoxCollider2D_t262790558 * L_13 = __this->get_myBox_2();
		NullCheck(L_13);
		Bounds_t3518514978  L_14 = Collider2D_get_bounds_m1087503006(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		Vector3_t3525329789  L_15 = Bounds_get_size_m3666348432((&V_4), /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_y_2();
		__this->set_x2_7(((float)((float)L_12+(float)((float)((float)L_16/(float)(2.0f))))));
		float L_17 = Random_Range_m3362417303(NULL /*static, unused*/, (1.0f), (2.0f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1630969879, L_17, /*hidden argument*/NULL);
		float L_18 = Random_Range_m3362417303(NULL /*static, unused*/, (10.0f), (20.0f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral220500613, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::Update()
extern "C"  void SpawnCoins_Update_m3085503097 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpawnCoins::SpawnThem()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2386450157;
extern Il2CppCodeGenString* _stringLiteral1630969879;
extern const uint32_t SpawnCoins_SpawnThem_m2285362729_MetadataUsageId;
extern "C"  void SpawnCoins_SpawnThem_m2285362729 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_SpawnThem_m2285362729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		float L_3 = __this->get_x1_6();
		float L_4 = __this->get_x2_7();
		float L_5 = Random_Range_m3362417303(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t3525329788  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m1517109030(&L_6, L_2, L_5, /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_pos_3(L_7);
		float L_8 = Random_Range_m3362417303(NULL /*static, unused*/, (2.5f), (5.5f), /*hidden argument*/NULL);
		__this->set_randTime_5(L_8);
		int32_t L_9 = Random_Range_m75452833(NULL /*static, unused*/, 1, 7, /*hidden argument*/NULL);
		__this->set_randPowerup_4(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_11 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2386450157, L_10, /*hidden argument*/NULL);
		Vector3_t3525329789  L_12 = __this->get_pos_3();
		Quaternion_t1891715979  L_13 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		float L_14 = __this->get_randTime_5();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1630969879, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::SpawnPowerup()
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2431756160;
extern Il2CppCodeGenString* _stringLiteral3597329061;
extern Il2CppCodeGenString* _stringLiteral3787356652;
extern Il2CppCodeGenString* _stringLiteral3485554945;
extern Il2CppCodeGenString* _stringLiteral3965443748;
extern Il2CppCodeGenString* _stringLiteral3507655134;
extern Il2CppCodeGenString* _stringLiteral220500613;
extern const uint32_t SpawnCoins_SpawnPowerup_m305981909_MetadataUsageId;
extern "C"  void SpawnCoins_SpawnPowerup_m305981909 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_SpawnPowerup_m305981909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_randPowerup_4();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_randPowerup_4();
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
		{
			goto IL_003c;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)1)) == 1)
		{
			goto IL_0051;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)1)) == 2)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)1)) == 3)
		{
			goto IL_007b;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)1)) == 4)
		{
			goto IL_0090;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)1)) == 5)
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00ba;
	}

IL_003c:
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral2431756160, (1.0f), /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_0051:
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3597329061, (1.0f), /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_0066:
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3787356652, (1.0f), /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_007b:
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3485554945, (1.0f), /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_0090:
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3965443748, (1.0f), /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_00a5:
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3507655134, (1.0f), /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_00ba:
	{
		goto IL_00bf;
	}

IL_00bf:
	{
		float L_5 = __this->get_randTime_5();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral220500613, ((float)((float)L_5*(float)(7.0f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::SpawnShockwave()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2359024282;
extern const uint32_t SpawnCoins_SpawnShockwave_m2782856272_MetadataUsageId;
extern "C"  void SpawnCoins_SpawnShockwave_m2782856272 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_SpawnShockwave_m2782856272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2359024282, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_pos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::SpawnFlurry()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2602228454;
extern const uint32_t SpawnCoins_SpawnFlurry_m2595767543_MetadataUsageId;
extern "C"  void SpawnCoins_SpawnFlurry_m2595767543 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_SpawnFlurry_m2595767543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2602228454, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_pos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::SpawnManiac()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral24977918;
extern const uint32_t SpawnCoins_SpawnManiac_m528688766_MetadataUsageId;
extern "C"  void SpawnCoins_SpawnManiac_m528688766 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_SpawnManiac_m528688766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral24977918, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_pos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::SpawnBottle()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2490454338;
extern const uint32_t SpawnCoins_SpawnBottle_m2555024467_MetadataUsageId;
extern "C"  void SpawnCoins_SpawnBottle_m2555024467 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_SpawnBottle_m2555024467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2490454338, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_pos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::SpawnShield()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2970343141;
extern const uint32_t SpawnCoins_SpawnShield_m4166663478_MetadataUsageId;
extern "C"  void SpawnCoins_SpawnShield_m4166663478 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_SpawnShield_m4166663478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2970343141, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_pos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnCoins::SpawnChrono()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4040243696;
extern const uint32_t SpawnCoins_SpawnChrono_m2318469616_MetadataUsageId;
extern "C"  void SpawnCoins_SpawnChrono_m2318469616 (SpawnCoins_t3299938663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnCoins_SpawnChrono_m2318469616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral4040243696, L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_pos_3();
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnEnemies::.ctor()
extern "C"  void SpawnEnemies__ctor_m2692862128 (SpawnEnemies_t3297838667 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnEnemies::Start()
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1630969879;
extern const uint32_t SpawnEnemies_Start_m1639999920_MetadataUsageId;
extern "C"  void SpawnEnemies_Start_m1639999920 (SpawnEnemies_t3297838667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnEnemies_Start_m1639999920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t3518514978  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Bounds_t3518514978  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		BoxCollider2D_t262790558 * L_0 = Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var);
		__this->set_myBox_2(L_0);
		BoxCollider2D_t262790558 * L_1 = __this->get_myBox_2();
		NullCheck(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_y_2();
		BoxCollider2D_t262790558 * L_5 = __this->get_myBox_2();
		NullCheck(L_5);
		Bounds_t3518514978  L_6 = Collider2D_get_bounds_m1087503006(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t3525329789  L_7 = Bounds_get_size_m3666348432((&V_1), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_2();
		__this->set_x1_6(((float)((float)L_4-(float)((float)((float)L_8/(float)(2.0f))))));
		BoxCollider2D_t262790558 * L_9 = __this->get_myBox_2();
		NullCheck(L_9);
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_y_2();
		BoxCollider2D_t262790558 * L_13 = __this->get_myBox_2();
		NullCheck(L_13);
		Bounds_t3518514978  L_14 = Collider2D_get_bounds_m1087503006(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		Vector3_t3525329789  L_15 = Bounds_get_size_m3666348432((&V_4), /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_y_2();
		__this->set_x2_7(((float)((float)L_12+(float)((float)((float)L_16/(float)(2.0f))))));
		float L_17 = Random_Range_m3362417303(NULL /*static, unused*/, (3.0f), (6.0f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1630969879, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnEnemies::SpawnThem()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t958209021_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern Il2CppCodeGenString* _stringLiteral2496330240;
extern Il2CppCodeGenString* _stringLiteral2381611212;
extern Il2CppCodeGenString* _stringLiteral82991;
extern Il2CppCodeGenString* _stringLiteral492622442;
extern Il2CppCodeGenString* _stringLiteral482684951;
extern Il2CppCodeGenString* _stringLiteral492864596;
extern Il2CppCodeGenString* _stringLiteral2427423810;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral1630969879;
extern const uint32_t SpawnEnemies_SpawnThem_m2594919877_MetadataUsageId;
extern "C"  void SpawnEnemies_SpawnThem_m2594919877 (SpawnEnemies_t3297838667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnEnemies_SpawnThem_m2594919877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		float L_3 = __this->get_x1_6();
		float L_4 = __this->get_x2_7();
		float L_5 = Random_Range_m3362417303(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t3525329788  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m1517109030(&L_6, L_2, L_5, /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_pos_3(L_7);
		Transform_t284553113 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3525329789  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_x_1();
		float L_11 = __this->get_x1_6();
		Vector2_t3525329788  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m1517109030(&L_12, L_10, L_11, /*hidden argument*/NULL);
		Vector3_t3525329789  L_13 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		__this->set_bottomLeft_8(L_13);
		GameObject_t4012695102 * L_14 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		NullCheck(L_14);
		Parameters_t2452200970 * L_15 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_14, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		NullCheck(L_15);
		float L_16 = L_15->get_spawnCoeficient_16();
		GameObject_t4012695102 * L_17 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		NullCheck(L_17);
		Parameters_t2452200970 * L_18 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_17, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		NullCheck(L_18);
		float L_19 = L_18->get_spawnCoeficient_16();
		float L_20 = Random_Range_m3362417303(NULL /*static, unused*/, ((float)((float)(2.5f)*(float)L_16)), ((float)((float)(5.5f)*(float)L_19)), /*hidden argument*/NULL);
		__this->set_randTime_5(L_20);
		int32_t L_21 = Random_Range_m75452833(NULL /*static, unused*/, 1, 5, /*hidden argument*/NULL);
		__this->set_rand_4(L_21);
		int32_t L_22 = __this->get_rand_4();
		int32_t L_23 = L_22;
		Il2CppObject * L_24 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2496330240, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_27 = Resources_Load_m3601699608(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		Vector3_t3525329789  L_28 = __this->get_pos_3();
		Quaternion_t1891715979  L_29 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		float L_30 = __this->get_randTime_5();
		if ((!(((float)L_30) > ((float)(4.0f)))))
		{
			goto IL_0113;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_31 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_32 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2381611212, L_31, /*hidden argument*/NULL);
		Vector3_t3525329789  L_33 = __this->get_bottomLeft_8();
		Quaternion_t1891715979  L_34 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/NULL);
	}

IL_0113:
	{
		String_t* L_35 = Application_get_loadedLevelName_m953500779(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_35, _stringLiteral82991, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_025d;
		}
	}
	{
		int32_t L_37 = __this->get_wave_9();
		if (((int32_t)((int32_t)L_37%(int32_t)((int32_t)14))))
		{
			goto IL_015f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_38 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_39 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral492622442, L_38, /*hidden argument*/NULL);
		Vector3_t3525329789  L_40 = __this->get_bottomLeft_8();
		Quaternion_t1891715979  L_41 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		goto IL_0202;
	}

IL_015f:
	{
		int32_t L_42 = __this->get_wave_9();
		if (((int32_t)((int32_t)L_42%(int32_t)((int32_t)17))))
		{
			goto IL_0197;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_44 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral482684951, L_43, /*hidden argument*/NULL);
		Vector3_t3525329789  L_45 = __this->get_pos_3();
		Quaternion_t1891715979  L_46 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_44, L_45, L_46, /*hidden argument*/NULL);
		goto IL_0202;
	}

IL_0197:
	{
		int32_t L_47 = __this->get_wave_9();
		if (((int32_t)((int32_t)L_47%(int32_t)((int32_t)12))))
		{
			goto IL_01cf;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_48 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_49 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral492864596, L_48, /*hidden argument*/NULL);
		Vector3_t3525329789  L_50 = __this->get_pos_3();
		Quaternion_t1891715979  L_51 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_49, L_50, L_51, /*hidden argument*/NULL);
		goto IL_0202;
	}

IL_01cf:
	{
		int32_t L_52 = __this->get_wave_9();
		if (((int32_t)((int32_t)L_52%(int32_t)((int32_t)9))))
		{
			goto IL_0202;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_54 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2427423810, L_53, /*hidden argument*/NULL);
		Vector3_t3525329789  L_55 = __this->get_pos_3();
		Quaternion_t1891715979  L_56 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_54, L_55, L_56, /*hidden argument*/NULL);
	}

IL_0202:
	{
		ObjectU5BU5D_t11523773* L_57 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_58 = Application_get_loadedLevelName_m953500779(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 0);
		ArrayElementTypeCheck (L_57, L_58);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_58);
		ObjectU5BU5D_t11523773* L_59 = L_57;
		float L_60 = __this->get_randTime_5();
		float L_61 = L_60;
		Il2CppObject * L_62 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_61);
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 1);
		ArrayElementTypeCheck (L_59, L_62);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_62);
		ObjectU5BU5D_t11523773* L_63 = L_59;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 2);
		ArrayElementTypeCheck (L_63, _stringLiteral32);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral32);
		ObjectU5BU5D_t11523773* L_64 = L_63;
		int32_t L_65 = __this->get_wave_9();
		int32_t L_66 = L_65;
		Il2CppObject * L_67 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_66);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 3);
		ArrayElementTypeCheck (L_64, L_67);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_67);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_68 = String_Concat_m3016520001(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		int32_t L_69 = __this->get_wave_9();
		__this->set_wave_9(((int32_t)((int32_t)L_69+(int32_t)1)));
		float L_70 = __this->get_randTime_5();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1630969879, L_70, /*hidden argument*/NULL);
	}

IL_025d:
	{
		return;
	}
}
// System.Void SpawnEnemies2::.ctor()
extern "C"  void SpawnEnemies2__ctor_m2610191876 (SpawnEnemies2_t3448750919 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnEnemies2::Start()
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1630969879;
extern const uint32_t SpawnEnemies2_Start_m1557329668_MetadataUsageId;
extern "C"  void SpawnEnemies2_Start_m1557329668 (SpawnEnemies2_t3448750919 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnEnemies2_Start_m1557329668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t3518514978  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Bounds_t3518514978  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		BoxCollider2D_t262790558 * L_0 = Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var);
		__this->set_myBox_2(L_0);
		BoxCollider2D_t262790558 * L_1 = __this->get_myBox_2();
		NullCheck(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_y_2();
		BoxCollider2D_t262790558 * L_5 = __this->get_myBox_2();
		NullCheck(L_5);
		Bounds_t3518514978  L_6 = Collider2D_get_bounds_m1087503006(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t3525329789  L_7 = Bounds_get_size_m3666348432((&V_1), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_2();
		__this->set_x1_6(((float)((float)L_4-(float)((float)((float)L_8/(float)(2.0f))))));
		BoxCollider2D_t262790558 * L_9 = __this->get_myBox_2();
		NullCheck(L_9);
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_y_2();
		BoxCollider2D_t262790558 * L_13 = __this->get_myBox_2();
		NullCheck(L_13);
		Bounds_t3518514978  L_14 = Collider2D_get_bounds_m1087503006(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		Vector3_t3525329789  L_15 = Bounds_get_size_m3666348432((&V_4), /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_y_2();
		__this->set_x2_7(((float)((float)L_12+(float)((float)((float)L_16/(float)(2.0f))))));
		float L_17 = Random_Range_m3362417303(NULL /*static, unused*/, (6.0f), (10.0f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1630969879, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnEnemies2::Update()
extern "C"  void SpawnEnemies2_Update_m1038431625 (SpawnEnemies2_t3448750919 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = Random_Range_m3362417303(NULL /*static, unused*/, (0.5f), (5.5f), /*hidden argument*/NULL);
		__this->set_randTime_5(L_0);
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		float L_4 = __this->get_x1_6();
		float L_5 = __this->get_x2_7();
		float L_6 = Random_Range_m3362417303(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Vector2_t3525329788  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m1517109030(&L_7, L_3, L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_pos_3(L_8);
		Transform_t284553113 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3525329789  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_x_1();
		float L_12 = __this->get_x1_6();
		Vector2_t3525329788  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector2__ctor_m1517109030(&L_13, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t3525329789  L_14 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		__this->set_bottomLeft_8(L_14);
		return;
	}
}
// System.Void SpawnEnemies2::SpawnThem()
extern const Il2CppType* GameObject_t4012695102_0_0_0_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2496330240;
extern Il2CppCodeGenString* _stringLiteral2381611212;
extern Il2CppCodeGenString* _stringLiteral1630969879;
extern const uint32_t SpawnEnemies2_SpawnThem_m2219776281_MetadataUsageId;
extern "C"  void SpawnEnemies2_SpawnThem_m2219776281 (SpawnEnemies2_t3448750919 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpawnEnemies2_SpawnThem_m2219776281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		float L_0 = Random_Range_m3362417303(NULL /*static, unused*/, (1.5f), (5.5f), /*hidden argument*/NULL);
		__this->set_randTime_5(L_0);
		int32_t L_1 = Random_Range_m75452833(NULL /*static, unused*/, 1, 5, /*hidden argument*/NULL);
		__this->set_rand_4(L_1);
		int32_t L_2 = __this->get_rand_4();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2496330240, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_7 = Resources_Load_m3601699608(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8 = __this->get_pos_3();
		Quaternion_t1891715979  L_9 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_10 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)CastclassSealed(L_10, GameObject_t4012695102_il2cpp_TypeInfo_var));
		float L_11 = __this->get_randTime_5();
		if ((!(((float)L_11) > ((float)(4.0f)))))
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t4012695102_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_13 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral2381611212, L_12, /*hidden argument*/NULL);
		Vector3_t3525329789  L_14 = __this->get_bottomLeft_8();
		Quaternion_t1891715979  L_15 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0091:
	{
		float L_16 = __this->get_randTime_5();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1630969879, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TestParticles__ctor_m83762608_MetadataUsageId;
extern "C"  void TestParticles__ctor_m83762608 (TestParticles_t3963318811 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TestParticles__ctor_m83762608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_CurrentElementIndex_10((-1));
		__this->set_m_CurrentParticleIndex_11((-1));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_ElementName_12(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_ParticleName_13(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::Start()
extern "C"  void TestParticles_Start_m3325867696 (TestParticles_t3963318811 * __this, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t3499186955* L_0 = __this->get_m_PrefabListFire_2();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_1 = __this->get_m_PrefabListWind_3();
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_2 = __this->get_m_PrefabListWater_4();
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_3 = __this->get_m_PrefabListEarth_5();
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_4 = __this->get_m_PrefabListIce_6();
		NullCheck(L_4);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_5 = __this->get_m_PrefabListThunder_7();
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_6 = __this->get_m_PrefabListLight_8();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_7 = __this->get_m_PrefabListDarkness_9();
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}

IL_0070:
	{
		__this->set_m_CurrentElementIndex_10(0);
		__this->set_m_CurrentParticleIndex_11(0);
		TestParticles_ShowParticle_m2655492727(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void TestParticles::Update()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t TestParticles_Update_m28535645_MetadataUsageId;
extern "C"  void TestParticles_Update_m28535645 (TestParticles_t3963318811 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TestParticles_Update_m28535645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_m_CurrentElementIndex_10();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_1 = __this->get_m_CurrentParticleIndex_11();
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyUp_m2739135306(NULL /*static, unused*/, ((int32_t)273), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_3 = __this->get_m_CurrentElementIndex_10();
		__this->set_m_CurrentElementIndex_10(((int32_t)((int32_t)L_3+(int32_t)1)));
		__this->set_m_CurrentParticleIndex_11(0);
		TestParticles_ShowParticle_m2655492727(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKeyUp_m2739135306(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_5 = __this->get_m_CurrentElementIndex_10();
		__this->set_m_CurrentElementIndex_10(((int32_t)((int32_t)L_5-(int32_t)1)));
		__this->set_m_CurrentParticleIndex_11(0);
		TestParticles_ShowParticle_m2655492727(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetKeyUp_m2739135306(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_7 = __this->get_m_CurrentParticleIndex_11();
		__this->set_m_CurrentParticleIndex_11(((int32_t)((int32_t)L_7-(int32_t)1)));
		TestParticles_ShowParticle_m2655492727(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_009e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetKeyUp_m2739135306(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_9 = __this->get_m_CurrentParticleIndex_11();
		__this->set_m_CurrentParticleIndex_11(((int32_t)((int32_t)L_9+(int32_t)1)));
		TestParticles_ShowParticle_m2655492727(__this, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		return;
	}
}
// System.Void TestParticles::OnGUI()
extern TypeInfo* WindowFunction_t999919624_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern const MethodInfo* TestParticles_InfoWindow_m1509106787_MethodInfo_var;
extern const MethodInfo* TestParticles_ParticleInformationWindow_m3377206901_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2283726;
extern Il2CppCodeGenString* _stringLiteral2245473;
extern const uint32_t TestParticles_OnGUI_m3874128554_MetadataUsageId;
extern "C"  void TestParticles_OnGUI_m3874128554 (TestParticles_t3963318811 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TestParticles_OnGUI_m3874128554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Rect__ctor_m3291325233(&L_1, (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)260)))))), (5.0f), (250.0f), (80.0f), /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)TestParticles_InfoWindow_m1509106787_MethodInfo_var);
		WindowFunction_t999919624 * L_3 = (WindowFunction_t999919624 *)il2cpp_codegen_object_new(WindowFunction_t999919624_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m732638321(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_Window_m2314976695(NULL /*static, unused*/, 1, L_1, L_3, _stringLiteral2283726, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, (((float)((float)((int32_t)((int32_t)L_4-(int32_t)((int32_t)260)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)85)))))), (250.0f), (80.0f), /*hidden argument*/NULL);
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)TestParticles_ParticleInformationWindow_m3377206901_MethodInfo_var);
		WindowFunction_t999919624 * L_8 = (WindowFunction_t999919624 *)il2cpp_codegen_object_new(WindowFunction_t999919624_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m732638321(L_8, __this, L_7, /*hidden argument*/NULL);
		GUI_Window_m2314976695(NULL /*static, unused*/, 2, L_6, L_8, _stringLiteral2245473, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::ShowParticle()
extern const MethodInfo* Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2158134;
extern Il2CppCodeGenString* _stringLiteral82365687;
extern Il2CppCodeGenString* _stringLiteral2664456;
extern Il2CppCodeGenString* _stringLiteral65740842;
extern Il2CppCodeGenString* _stringLiteral3678054596;
extern Il2CppCodeGenString* _stringLiteral72299;
extern Il2CppCodeGenString* _stringLiteral72432886;
extern Il2CppCodeGenString* _stringLiteral2524430029;
extern const uint32_t TestParticles_ShowParticle_m2655492727_MetadataUsageId;
extern "C"  void TestParticles_ShowParticle_m2655492727 (TestParticles_t3963318811 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TestParticles_ShowParticle_m2655492727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_m_CurrentElementIndex_10();
		if ((((int32_t)L_0) <= ((int32_t)7)))
		{
			goto IL_0018;
		}
	}
	{
		__this->set_m_CurrentElementIndex_10(0);
		goto IL_002b;
	}

IL_0018:
	{
		int32_t L_1 = __this->get_m_CurrentElementIndex_10();
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		__this->set_m_CurrentElementIndex_10(7);
	}

IL_002b:
	{
		int32_t L_2 = __this->get_m_CurrentElementIndex_10();
		if (L_2)
		{
			goto IL_0052;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_3 = __this->get_m_PrefabListFire_2();
		__this->set_m_CurrentElementList_14(L_3);
		__this->set_m_ElementName_12(_stringLiteral2158134);
		goto IL_0165;
	}

IL_0052:
	{
		int32_t L_4 = __this->get_m_CurrentElementIndex_10();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_5 = __this->get_m_PrefabListWater_4();
		__this->set_m_CurrentElementList_14(L_5);
		__this->set_m_ElementName_12(_stringLiteral82365687);
		goto IL_0165;
	}

IL_007a:
	{
		int32_t L_6 = __this->get_m_CurrentElementIndex_10();
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_00a2;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_7 = __this->get_m_PrefabListWind_3();
		__this->set_m_CurrentElementList_14(L_7);
		__this->set_m_ElementName_12(_stringLiteral2664456);
		goto IL_0165;
	}

IL_00a2:
	{
		int32_t L_8 = __this->get_m_CurrentElementIndex_10();
		if ((!(((uint32_t)L_8) == ((uint32_t)3))))
		{
			goto IL_00ca;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_9 = __this->get_m_PrefabListEarth_5();
		__this->set_m_CurrentElementList_14(L_9);
		__this->set_m_ElementName_12(_stringLiteral65740842);
		goto IL_0165;
	}

IL_00ca:
	{
		int32_t L_10 = __this->get_m_CurrentElementIndex_10();
		if ((!(((uint32_t)L_10) == ((uint32_t)4))))
		{
			goto IL_00f2;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_11 = __this->get_m_PrefabListThunder_7();
		__this->set_m_CurrentElementList_14(L_11);
		__this->set_m_ElementName_12(_stringLiteral3678054596);
		goto IL_0165;
	}

IL_00f2:
	{
		int32_t L_12 = __this->get_m_CurrentElementIndex_10();
		if ((!(((uint32_t)L_12) == ((uint32_t)5))))
		{
			goto IL_011a;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_13 = __this->get_m_PrefabListIce_6();
		__this->set_m_CurrentElementList_14(L_13);
		__this->set_m_ElementName_12(_stringLiteral72299);
		goto IL_0165;
	}

IL_011a:
	{
		int32_t L_14 = __this->get_m_CurrentElementIndex_10();
		if ((!(((uint32_t)L_14) == ((uint32_t)6))))
		{
			goto IL_0142;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_15 = __this->get_m_PrefabListLight_8();
		__this->set_m_CurrentElementList_14(L_15);
		__this->set_m_ElementName_12(_stringLiteral72432886);
		goto IL_0165;
	}

IL_0142:
	{
		int32_t L_16 = __this->get_m_CurrentElementIndex_10();
		if ((!(((uint32_t)L_16) == ((uint32_t)7))))
		{
			goto IL_0165;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_17 = __this->get_m_PrefabListDarkness_9();
		__this->set_m_CurrentElementList_14(L_17);
		__this->set_m_ElementName_12(_stringLiteral2524430029);
	}

IL_0165:
	{
		int32_t L_18 = __this->get_m_CurrentParticleIndex_11();
		GameObjectU5BU5D_t3499186955* L_19 = __this->get_m_CurrentElementList_14();
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0184;
		}
	}
	{
		__this->set_m_CurrentParticleIndex_11(0);
		goto IL_01a0;
	}

IL_0184:
	{
		int32_t L_20 = __this->get_m_CurrentParticleIndex_11();
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_01a0;
		}
	}
	{
		GameObjectU5BU5D_t3499186955* L_21 = __this->get_m_CurrentElementList_14();
		NullCheck(L_21);
		__this->set_m_CurrentParticleIndex_11(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length))))-(int32_t)1)));
	}

IL_01a0:
	{
		GameObjectU5BU5D_t3499186955* L_22 = __this->get_m_CurrentElementList_14();
		int32_t L_23 = __this->get_m_CurrentParticleIndex_11();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		NullCheck(((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24))));
		String_t* L_25 = Object_get_name_m3709440845(((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24))), /*hidden argument*/NULL);
		__this->set_m_ParticleName_13(L_25);
		GameObject_t4012695102 * L_26 = __this->get_m_CurrentParticle_15();
		bool L_27 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_26, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_01d4;
		}
	}
	{
		GameObject_t4012695102 * L_28 = __this->get_m_CurrentParticle_15();
		Object_DestroyObject_m3900253135(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_01d4:
	{
		GameObjectU5BU5D_t3499186955* L_29 = __this->get_m_CurrentElementList_14();
		int32_t L_30 = __this->get_m_CurrentParticleIndex_11();
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		GameObject_t4012695102 * L_32 = Object_Instantiate_TisGameObject_t4012695102_m3917608929(NULL /*static, unused*/, ((L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31))), /*hidden argument*/Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var);
		__this->set_m_CurrentParticle_15(L_32);
		return;
	}
}
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3722368316;
extern Il2CppCodeGenString* _stringLiteral1032;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral1214022714;
extern const uint32_t TestParticles_ParticleInformationWindow_m3377206901_MetadataUsageId;
extern "C"  void TestParticles_ParticleInformationWindow_m3377206901 (TestParticles_t3963318811 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TestParticles_ParticleInformationWindow_m3377206901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t1525428817  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m3291325233(&L_0, (12.0f), (25.0f), (280.0f), (20.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_1 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral3722368316);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3722368316);
		ObjectU5BU5D_t11523773* L_2 = L_1;
		String_t* L_3 = __this->get_m_ElementName_12();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_3);
		ObjectU5BU5D_t11523773* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral1032);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1032);
		ObjectU5BU5D_t11523773* L_5 = L_4;
		int32_t L_6 = __this->get_m_CurrentParticleIndex_11();
		int32_t L_7 = ((int32_t)((int32_t)L_6+(int32_t)1));
		Il2CppObject * L_8 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		ObjectU5BU5D_t11523773* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, _stringLiteral47);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral47);
		ObjectU5BU5D_t11523773* L_10 = L_9;
		GameObjectU5BU5D_t3499186955* L_11 = __this->get_m_CurrentElementList_14();
		NullCheck(L_11);
		int32_t L_12 = (((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))));
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_13);
		ObjectU5BU5D_t11523773* L_14 = L_10;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 6);
		ArrayElementTypeCheck (L_14, _stringLiteral41);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3016520001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_0, L_15, /*hidden argument*/NULL);
		Rect_t1525428817  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m3291325233(&L_16, (12.0f), (50.0f), (280.0f), (20.0f), /*hidden argument*/NULL);
		String_t* L_17 = __this->get_m_ParticleName_13();
		NullCheck(L_17);
		String_t* L_18 = String_ToUpper_m1841663596(L_17, /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1214022714, L_18, /*hidden argument*/NULL);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::InfoWindow(System.Int32)
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral294327971;
extern Il2CppCodeGenString* _stringLiteral2532620189;
extern const uint32_t TestParticles_InfoWindow_m1509106787_MetadataUsageId;
extern "C"  void TestParticles_InfoWindow_m1509106787 (TestParticles_t3963318811 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TestParticles_InfoWindow_m1509106787_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t1525428817  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m3291325233(&L_0, (15.0f), (25.0f), (240.0f), (20.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_0, _stringLiteral294327971, /*hidden argument*/NULL);
		Rect_t1525428817  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Rect__ctor_m3291325233(&L_1, (15.0f), (50.0f), (240.0f), (20.0f), /*hidden argument*/NULL);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_1, _stringLiteral2532620189, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TryAgain::.ctor()
extern "C"  void TryAgain__ctor_m2946171318 (TryAgain_t1990705797 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TryAgain::Start()
extern "C"  void TryAgain_Start_m1893309110 (TryAgain_t1990705797 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TryAgain::Update()
extern "C"  void TryAgain_Update_m2863859735 (TryAgain_t1990705797 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SmoothFollow::.ctor()
extern "C"  void SmoothFollow__ctor_m2621372001 (SmoothFollow_t1823522624 * __this, const MethodInfo* method)
{
	{
		__this->set_distance_3((10.0f));
		__this->set_height_4((5.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SmoothFollow::Start()
extern "C"  void SmoothFollow_Start_m1568509793 (SmoothFollow_t1823522624 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SmoothFollow::LateUpdate()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t SmoothFollow_LateUpdate_m1090922642_MetadataUsageId;
extern "C"  void SmoothFollow_LateUpdate_m1090922642 (SmoothFollow_t1823522624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothFollow_LateUpdate_m1090922642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Quaternion_t1891715979  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3525329789  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3525329789  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3525329789  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3525329789  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3525329789  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		Transform_t284553113 * L_0 = __this->get_target_2();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Transform_t284553113 * L_2 = __this->get_target_2();
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_eulerAngles_m1058084741(L_2, /*hidden argument*/NULL);
		V_5 = L_3;
		float L_4 = (&V_5)->get_y_2();
		V_0 = L_4;
		Transform_t284553113 * L_5 = __this->get_target_2();
		NullCheck(L_5);
		Vector3_t3525329789  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		V_6 = L_6;
		float L_7 = (&V_6)->get_y_2();
		float L_8 = __this->get_height_4();
		V_1 = ((float)((float)L_7+(float)L_8));
		Transform_t284553113 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3525329789  L_10 = Transform_get_eulerAngles_m1058084741(L_9, /*hidden argument*/NULL);
		V_7 = L_10;
		float L_11 = (&V_7)->get_y_2();
		V_2 = L_11;
		Transform_t284553113 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3525329789  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		V_8 = L_13;
		float L_14 = (&V_8)->get_y_2();
		V_3 = L_14;
		float L_15 = V_2;
		float L_16 = V_0;
		float L_17 = __this->get_rotationDamping_5();
		float L_18 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_19 = Mathf_LerpAngle_m1852538964(NULL /*static, unused*/, L_15, L_16, ((float)((float)L_17*(float)L_18)), /*hidden argument*/NULL);
		V_2 = L_19;
		float L_20 = V_3;
		float L_21 = V_1;
		float L_22 = __this->get_heightDamping_6();
		float L_23 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_20, L_21, ((float)((float)L_22*(float)L_23)), /*hidden argument*/NULL);
		V_3 = L_24;
		float L_25 = V_2;
		Quaternion_t1891715979  L_26 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (0.0f), L_25, (0.0f), /*hidden argument*/NULL);
		V_4 = L_26;
		Transform_t284553113 * L_27 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_28 = __this->get_target_2();
		NullCheck(L_28);
		Vector3_t3525329789  L_29 = Transform_get_position_m2211398607(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_m3111394108(L_27, L_29, /*hidden argument*/NULL);
		Transform_t284553113 * L_30 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_31 = L_30;
		NullCheck(L_31);
		Vector3_t3525329789  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_33 = V_4;
		Vector3_t3525329789  L_34 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_35 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		float L_36 = __this->get_distance_3();
		Vector3_t3525329789  L_37 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t3525329789  L_38 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_32, L_37, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_position_m3111394108(L_31, L_38, /*hidden argument*/NULL);
		Transform_t284553113 * L_39 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_40 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t3525329789  L_41 = Transform_get_position_m2211398607(L_40, /*hidden argument*/NULL);
		V_9 = L_41;
		float L_42 = (&V_9)->get_x_1();
		float L_43 = V_3;
		Transform_t284553113 * L_44 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t3525329789  L_45 = Transform_get_position_m2211398607(L_44, /*hidden argument*/NULL);
		V_10 = L_45;
		float L_46 = (&V_10)->get_z_3();
		Vector3_t3525329789  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m2926210380(&L_47, L_42, L_43, L_46, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_position_m3111394108(L_39, L_47, /*hidden argument*/NULL);
		Transform_t284553113 * L_48 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_49 = __this->get_target_2();
		NullCheck(L_48);
		Transform_LookAt_m2663225588(L_48, L_49, /*hidden argument*/NULL);
		return;
	}
}
// System.Void yyo::.ctor()
extern "C"  void yyo__ctor_m3943208700 (yyo_t120143 * __this, const MethodInfo* method)
{
	{
		__this->set_lerpTime_2((2.0f));
		__this->set_moveDistance_4((15.0f));
		__this->set_onlyOnceCheckpoint_14((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void yyo::Start()
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3959627309;
extern Il2CppCodeGenString* _stringLiteral2483298528;
extern const uint32_t yyo_Start_m2890346492_MetadataUsageId;
extern "C"  void yyo_Start_m2890346492 (yyo_t120143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (yyo_Start_m2890346492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3878351788 * L_0 = Resources_Load_m2187391845(NULL /*static, unused*/, _stringLiteral3959627309, /*hidden argument*/NULL);
		__this->set_house_18(((GameObject_t4012695102 *)IsInstSealed(L_0, GameObject_t4012695102_il2cpp_TypeInfo_var)));
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		AudioSource_t3628549054 * L_2 = GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151(L_1, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151_MethodInfo_var);
		__this->set_playSound_15(L_2);
		Transform_t284553113 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		__this->set_startPos_5(L_4);
		Transform_t284553113 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t3525329789  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		Transform_t284553113 * L_7 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_right_m2070836824(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_moveDistance_4();
		Vector3_t3525329789  L_10 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t3525329789  L_11 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_6, L_10, /*hidden argument*/NULL);
		__this->set_endPos_6(L_11);
		Vector3_t3525329789  L_12 = __this->get_endPos_6();
		Transform_t284553113 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3525329789  L_14 = Transform_get_up_m297874561(L_13, /*hidden argument*/NULL);
		float L_15 = __this->get_moveDistance_4();
		Vector3_t3525329789  L_16 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_14, ((float)((float)L_15+(float)(1.0f))), /*hidden argument*/NULL);
		Vector3_t3525329789  L_17 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_12, L_16, /*hidden argument*/NULL);
		__this->set_endPosOut_7(L_17);
		Animator_t792326996 * L_18 = Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var);
		__this->set_myAni_8(L_18);
		GameObject_t4012695102 * L_19 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2483298528, /*hidden argument*/NULL);
		__this->set_go_16(L_19);
		GameObject_t4012695102 * L_20 = __this->get_go_16();
		NullCheck(L_20);
		Parameters_t2452200970 * L_21 = GameObject_GetComponent_TisParameters_t2452200970_m1256535827(L_20, /*hidden argument*/GameObject_GetComponent_TisParameters_t2452200970_m1256535827_MethodInfo_var);
		__this->set_parameters_17(L_21);
		return;
	}
}
// System.Void yyo::Update()
extern Il2CppCodeGenString* _stringLiteral633075377;
extern const uint32_t yyo_Update_m3707247505_MetadataUsageId;
extern "C"  void yyo_Update_m3707247505 (yyo_t120143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (yyo_Update_m3707247505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral633075377, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void yyo::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppCodeGenString* _stringLiteral69916416;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral3785212358;
extern const uint32_t yyo_OnTriggerEnter2D_m3693893628_MetadataUsageId;
extern "C"  void yyo_OnTriggerEnter2D_m3693893628 (yyo_t120143 * __this, Collider2D_t1890038195 * ___trigger, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (yyo_OnTriggerEnter2D_m3693893628_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_CompareTag_m3153977471(L_0, _stringLiteral69916416, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007d;
		}
	}
	{
		Collider2D_t1890038195 * L_2 = ___trigger;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m305486283(L_2, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_007d;
		}
	}
	{
		Animator_t792326996 * L_4 = __this->get_myAni_8();
		NullCheck(L_4);
		Animator_SetBool_m2336836203(L_4, _stringLiteral3785212358, (bool)1, /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_5 = __this->get_playSound_15();
		NullCheck(L_5);
		AudioSource_Play_m1360558992(L_5, /*hidden argument*/NULL);
		bool L_6 = __this->get_onlyOnceCheckpoint_14();
		if (!L_6)
		{
			goto IL_007d;
		}
	}
	{
		Parameters_t2452200970 * L_7 = __this->get_parameters_17();
		Parameters_t2452200970 * L_8 = L_7;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_currentCheckPoint_15();
		NullCheck(L_8);
		L_8->set_currentCheckPoint_15(((int32_t)((int32_t)L_9+(int32_t)1)));
		GameObject_t4012695102 * L_10 = __this->get_house_18();
		Vector3_t3525329789  L_11 = __this->get_startPos_5();
		Quaternion_t1891715979  L_12 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
		__this->set_onlyOnceCheckpoint_14((bool)0);
	}

IL_007d:
	{
		return;
	}
}
// System.Void yyo::MoveToSight()
extern Il2CppCodeGenString* _stringLiteral3785212358;
extern Il2CppCodeGenString* _stringLiteral3799560617;
extern const uint32_t yyo_MoveToSight_m834429931_MetadataUsageId;
extern "C"  void yyo_MoveToSight_m834429931 (yyo_t120143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (yyo_MoveToSight_m834429931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_currentLerpTime_3();
		float L_1 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentLerpTime_3(((float)((float)L_0+(float)L_1)));
		float L_2 = __this->get_currentLerpTime_3();
		float L_3 = __this->get_lerpTime_2();
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_002f;
		}
	}
	{
		float L_4 = __this->get_lerpTime_2();
		__this->set_currentLerpTime_3(L_4);
	}

IL_002f:
	{
		float L_5 = __this->get_currentLerpTime_3();
		float L_6 = __this->get_lerpTime_2();
		__this->set_perc_13(((float)((float)L_5/(float)L_6)));
		Animator_t792326996 * L_7 = __this->get_myAni_8();
		NullCheck(L_7);
		bool L_8 = Animator_GetBool_m436748612(L_7, _stringLiteral3785212358, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_007e;
		}
	}
	{
		Transform_t284553113 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_10 = __this->get_startPos_5();
		Vector3_t3525329789  L_11 = __this->get_endPos_6();
		float L_12 = __this->get_perc_13();
		Vector3_t3525329789  L_13 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_position_m3111394108(L_9, L_13, /*hidden argument*/NULL);
		goto IL_008e;
	}

IL_007e:
	{
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral3799560617, (2.0f), /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// System.Void yyo::MoveOutOfSight()
extern "C"  void yyo_MoveOutOfSight_m3383009617 (yyo_t120143 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_you_12();
		float L_1 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_you_12(((float)((float)L_0+(float)((float)((float)L_1/(float)(20.0f))))));
		float L_2 = __this->get_you_12();
		float L_3 = __this->get_lerpTime_2();
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0035;
		}
	}
	{
		float L_4 = __this->get_lerpTime_2();
		__this->set_you_12(L_4);
	}

IL_0035:
	{
		Transform_t284553113 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3525329789  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8 = __this->get_endPosOut_7();
		float L_9 = __this->get_you_12();
		Vector3_t3525329789  L_10 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_position_m3111394108(L_5, L_10, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_11 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_11, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
