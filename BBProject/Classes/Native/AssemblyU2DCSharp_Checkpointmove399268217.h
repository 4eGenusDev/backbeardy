﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Checkpointmove
struct  Checkpointmove_t399268217  : public MonoBehaviour_t3012272455
{
public:
	// System.Single Checkpointmove::lerpTime
	float ___lerpTime_2;
	// System.Single Checkpointmove::currentLerpTime
	float ___currentLerpTime_3;
	// System.Single Checkpointmove::moveDistance
	float ___moveDistance_4;
	// UnityEngine.Vector3 Checkpointmove::startPos
	Vector3_t3525329789  ___startPos_5;
	// UnityEngine.Vector3 Checkpointmove::endPos
	Vector3_t3525329789  ___endPos_6;

public:
	inline static int32_t get_offset_of_lerpTime_2() { return static_cast<int32_t>(offsetof(Checkpointmove_t399268217, ___lerpTime_2)); }
	inline float get_lerpTime_2() const { return ___lerpTime_2; }
	inline float* get_address_of_lerpTime_2() { return &___lerpTime_2; }
	inline void set_lerpTime_2(float value)
	{
		___lerpTime_2 = value;
	}

	inline static int32_t get_offset_of_currentLerpTime_3() { return static_cast<int32_t>(offsetof(Checkpointmove_t399268217, ___currentLerpTime_3)); }
	inline float get_currentLerpTime_3() const { return ___currentLerpTime_3; }
	inline float* get_address_of_currentLerpTime_3() { return &___currentLerpTime_3; }
	inline void set_currentLerpTime_3(float value)
	{
		___currentLerpTime_3 = value;
	}

	inline static int32_t get_offset_of_moveDistance_4() { return static_cast<int32_t>(offsetof(Checkpointmove_t399268217, ___moveDistance_4)); }
	inline float get_moveDistance_4() const { return ___moveDistance_4; }
	inline float* get_address_of_moveDistance_4() { return &___moveDistance_4; }
	inline void set_moveDistance_4(float value)
	{
		___moveDistance_4 = value;
	}

	inline static int32_t get_offset_of_startPos_5() { return static_cast<int32_t>(offsetof(Checkpointmove_t399268217, ___startPos_5)); }
	inline Vector3_t3525329789  get_startPos_5() const { return ___startPos_5; }
	inline Vector3_t3525329789 * get_address_of_startPos_5() { return &___startPos_5; }
	inline void set_startPos_5(Vector3_t3525329789  value)
	{
		___startPos_5 = value;
	}

	inline static int32_t get_offset_of_endPos_6() { return static_cast<int32_t>(offsetof(Checkpointmove_t399268217, ___endPos_6)); }
	inline Vector3_t3525329789  get_endPos_6() const { return ___endPos_6; }
	inline Vector3_t3525329789 * get_address_of_endPos_6() { return &___endPos_6; }
	inline void set_endPos_6(Vector3_t3525329789  value)
	{
		___endPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
