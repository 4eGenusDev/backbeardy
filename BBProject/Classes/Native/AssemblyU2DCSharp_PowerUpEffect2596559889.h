﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUpEffect
struct  PowerUpEffect_t2596559889  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.SpriteRenderer PowerUpEffect::changeEffect
	SpriteRenderer_t2223784725 * ___changeEffect_2;
	// UnityEngine.Color PowerUpEffect::FadeColor
	Color_t1588175760  ___FadeColor_3;
	// UnityEngine.GameObject PowerUpEffect::Shine
	GameObject_t4012695102 * ___Shine_4;

public:
	inline static int32_t get_offset_of_changeEffect_2() { return static_cast<int32_t>(offsetof(PowerUpEffect_t2596559889, ___changeEffect_2)); }
	inline SpriteRenderer_t2223784725 * get_changeEffect_2() const { return ___changeEffect_2; }
	inline SpriteRenderer_t2223784725 ** get_address_of_changeEffect_2() { return &___changeEffect_2; }
	inline void set_changeEffect_2(SpriteRenderer_t2223784725 * value)
	{
		___changeEffect_2 = value;
		Il2CppCodeGenWriteBarrier(&___changeEffect_2, value);
	}

	inline static int32_t get_offset_of_FadeColor_3() { return static_cast<int32_t>(offsetof(PowerUpEffect_t2596559889, ___FadeColor_3)); }
	inline Color_t1588175760  get_FadeColor_3() const { return ___FadeColor_3; }
	inline Color_t1588175760 * get_address_of_FadeColor_3() { return &___FadeColor_3; }
	inline void set_FadeColor_3(Color_t1588175760  value)
	{
		___FadeColor_3 = value;
	}

	inline static int32_t get_offset_of_Shine_4() { return static_cast<int32_t>(offsetof(PowerUpEffect_t2596559889, ___Shine_4)); }
	inline GameObject_t4012695102 * get_Shine_4() const { return ___Shine_4; }
	inline GameObject_t4012695102 ** get_address_of_Shine_4() { return &___Shine_4; }
	inline void set_Shine_4(GameObject_t4012695102 * value)
	{
		___Shine_4 = value;
		Il2CppCodeGenWriteBarrier(&___Shine_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
