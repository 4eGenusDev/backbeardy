﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t3681339876;
// System.Single[]
struct SingleU5BU5D_t1219431280;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Parallaxing
struct  Parallaxing_t2299696843  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform[] Parallaxing::backgrounds
	TransformU5BU5D_t3681339876* ___backgrounds_2;
	// System.Single[] Parallaxing::speed
	SingleU5BU5D_t1219431280* ___speed_3;
	// System.Single Parallaxing::parameters
	float ___parameters_4;

public:
	inline static int32_t get_offset_of_backgrounds_2() { return static_cast<int32_t>(offsetof(Parallaxing_t2299696843, ___backgrounds_2)); }
	inline TransformU5BU5D_t3681339876* get_backgrounds_2() const { return ___backgrounds_2; }
	inline TransformU5BU5D_t3681339876** get_address_of_backgrounds_2() { return &___backgrounds_2; }
	inline void set_backgrounds_2(TransformU5BU5D_t3681339876* value)
	{
		___backgrounds_2 = value;
		Il2CppCodeGenWriteBarrier(&___backgrounds_2, value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(Parallaxing_t2299696843, ___speed_3)); }
	inline SingleU5BU5D_t1219431280* get_speed_3() const { return ___speed_3; }
	inline SingleU5BU5D_t1219431280** get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(SingleU5BU5D_t1219431280* value)
	{
		___speed_3 = value;
		Il2CppCodeGenWriteBarrier(&___speed_3, value);
	}

	inline static int32_t get_offset_of_parameters_4() { return static_cast<int32_t>(offsetof(Parallaxing_t2299696843, ___parameters_4)); }
	inline float get_parameters_4() const { return ___parameters_4; }
	inline float* get_address_of_parameters_4() { return &___parameters_4; }
	inline void set_parameters_4(float value)
	{
		___parameters_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
