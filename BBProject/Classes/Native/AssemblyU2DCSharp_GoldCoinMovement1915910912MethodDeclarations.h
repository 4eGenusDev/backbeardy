﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoldCoinMovement
struct GoldCoinMovement_t1915910912;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void GoldCoinMovement::.ctor()
extern "C"  void GoldCoinMovement__ctor_m398265243 (GoldCoinMovement_t1915910912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldCoinMovement::Start()
extern "C"  void GoldCoinMovement_Start_m3640370331 (GoldCoinMovement_t1915910912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldCoinMovement::Update()
extern "C"  void GoldCoinMovement_Update_m1188182738 (GoldCoinMovement_t1915910912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldCoinMovement::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void GoldCoinMovement_OnTriggerEnter2D_m1905341245 (GoldCoinMovement_t1915910912 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
