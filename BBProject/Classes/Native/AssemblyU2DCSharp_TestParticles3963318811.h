﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3499186955;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestParticles
struct  TestParticles_t3963318811  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject[] TestParticles::m_PrefabListFire
	GameObjectU5BU5D_t3499186955* ___m_PrefabListFire_2;
	// UnityEngine.GameObject[] TestParticles::m_PrefabListWind
	GameObjectU5BU5D_t3499186955* ___m_PrefabListWind_3;
	// UnityEngine.GameObject[] TestParticles::m_PrefabListWater
	GameObjectU5BU5D_t3499186955* ___m_PrefabListWater_4;
	// UnityEngine.GameObject[] TestParticles::m_PrefabListEarth
	GameObjectU5BU5D_t3499186955* ___m_PrefabListEarth_5;
	// UnityEngine.GameObject[] TestParticles::m_PrefabListIce
	GameObjectU5BU5D_t3499186955* ___m_PrefabListIce_6;
	// UnityEngine.GameObject[] TestParticles::m_PrefabListThunder
	GameObjectU5BU5D_t3499186955* ___m_PrefabListThunder_7;
	// UnityEngine.GameObject[] TestParticles::m_PrefabListLight
	GameObjectU5BU5D_t3499186955* ___m_PrefabListLight_8;
	// UnityEngine.GameObject[] TestParticles::m_PrefabListDarkness
	GameObjectU5BU5D_t3499186955* ___m_PrefabListDarkness_9;
	// System.Int32 TestParticles::m_CurrentElementIndex
	int32_t ___m_CurrentElementIndex_10;
	// System.Int32 TestParticles::m_CurrentParticleIndex
	int32_t ___m_CurrentParticleIndex_11;
	// System.String TestParticles::m_ElementName
	String_t* ___m_ElementName_12;
	// System.String TestParticles::m_ParticleName
	String_t* ___m_ParticleName_13;
	// UnityEngine.GameObject[] TestParticles::m_CurrentElementList
	GameObjectU5BU5D_t3499186955* ___m_CurrentElementList_14;
	// UnityEngine.GameObject TestParticles::m_CurrentParticle
	GameObject_t4012695102 * ___m_CurrentParticle_15;

public:
	inline static int32_t get_offset_of_m_PrefabListFire_2() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_PrefabListFire_2)); }
	inline GameObjectU5BU5D_t3499186955* get_m_PrefabListFire_2() const { return ___m_PrefabListFire_2; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_PrefabListFire_2() { return &___m_PrefabListFire_2; }
	inline void set_m_PrefabListFire_2(GameObjectU5BU5D_t3499186955* value)
	{
		___m_PrefabListFire_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrefabListFire_2, value);
	}

	inline static int32_t get_offset_of_m_PrefabListWind_3() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_PrefabListWind_3)); }
	inline GameObjectU5BU5D_t3499186955* get_m_PrefabListWind_3() const { return ___m_PrefabListWind_3; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_PrefabListWind_3() { return &___m_PrefabListWind_3; }
	inline void set_m_PrefabListWind_3(GameObjectU5BU5D_t3499186955* value)
	{
		___m_PrefabListWind_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrefabListWind_3, value);
	}

	inline static int32_t get_offset_of_m_PrefabListWater_4() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_PrefabListWater_4)); }
	inline GameObjectU5BU5D_t3499186955* get_m_PrefabListWater_4() const { return ___m_PrefabListWater_4; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_PrefabListWater_4() { return &___m_PrefabListWater_4; }
	inline void set_m_PrefabListWater_4(GameObjectU5BU5D_t3499186955* value)
	{
		___m_PrefabListWater_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrefabListWater_4, value);
	}

	inline static int32_t get_offset_of_m_PrefabListEarth_5() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_PrefabListEarth_5)); }
	inline GameObjectU5BU5D_t3499186955* get_m_PrefabListEarth_5() const { return ___m_PrefabListEarth_5; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_PrefabListEarth_5() { return &___m_PrefabListEarth_5; }
	inline void set_m_PrefabListEarth_5(GameObjectU5BU5D_t3499186955* value)
	{
		___m_PrefabListEarth_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrefabListEarth_5, value);
	}

	inline static int32_t get_offset_of_m_PrefabListIce_6() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_PrefabListIce_6)); }
	inline GameObjectU5BU5D_t3499186955* get_m_PrefabListIce_6() const { return ___m_PrefabListIce_6; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_PrefabListIce_6() { return &___m_PrefabListIce_6; }
	inline void set_m_PrefabListIce_6(GameObjectU5BU5D_t3499186955* value)
	{
		___m_PrefabListIce_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrefabListIce_6, value);
	}

	inline static int32_t get_offset_of_m_PrefabListThunder_7() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_PrefabListThunder_7)); }
	inline GameObjectU5BU5D_t3499186955* get_m_PrefabListThunder_7() const { return ___m_PrefabListThunder_7; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_PrefabListThunder_7() { return &___m_PrefabListThunder_7; }
	inline void set_m_PrefabListThunder_7(GameObjectU5BU5D_t3499186955* value)
	{
		___m_PrefabListThunder_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrefabListThunder_7, value);
	}

	inline static int32_t get_offset_of_m_PrefabListLight_8() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_PrefabListLight_8)); }
	inline GameObjectU5BU5D_t3499186955* get_m_PrefabListLight_8() const { return ___m_PrefabListLight_8; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_PrefabListLight_8() { return &___m_PrefabListLight_8; }
	inline void set_m_PrefabListLight_8(GameObjectU5BU5D_t3499186955* value)
	{
		___m_PrefabListLight_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrefabListLight_8, value);
	}

	inline static int32_t get_offset_of_m_PrefabListDarkness_9() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_PrefabListDarkness_9)); }
	inline GameObjectU5BU5D_t3499186955* get_m_PrefabListDarkness_9() const { return ___m_PrefabListDarkness_9; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_PrefabListDarkness_9() { return &___m_PrefabListDarkness_9; }
	inline void set_m_PrefabListDarkness_9(GameObjectU5BU5D_t3499186955* value)
	{
		___m_PrefabListDarkness_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrefabListDarkness_9, value);
	}

	inline static int32_t get_offset_of_m_CurrentElementIndex_10() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_CurrentElementIndex_10)); }
	inline int32_t get_m_CurrentElementIndex_10() const { return ___m_CurrentElementIndex_10; }
	inline int32_t* get_address_of_m_CurrentElementIndex_10() { return &___m_CurrentElementIndex_10; }
	inline void set_m_CurrentElementIndex_10(int32_t value)
	{
		___m_CurrentElementIndex_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentParticleIndex_11() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_CurrentParticleIndex_11)); }
	inline int32_t get_m_CurrentParticleIndex_11() const { return ___m_CurrentParticleIndex_11; }
	inline int32_t* get_address_of_m_CurrentParticleIndex_11() { return &___m_CurrentParticleIndex_11; }
	inline void set_m_CurrentParticleIndex_11(int32_t value)
	{
		___m_CurrentParticleIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_ElementName_12() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_ElementName_12)); }
	inline String_t* get_m_ElementName_12() const { return ___m_ElementName_12; }
	inline String_t** get_address_of_m_ElementName_12() { return &___m_ElementName_12; }
	inline void set_m_ElementName_12(String_t* value)
	{
		___m_ElementName_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_ElementName_12, value);
	}

	inline static int32_t get_offset_of_m_ParticleName_13() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_ParticleName_13)); }
	inline String_t* get_m_ParticleName_13() const { return ___m_ParticleName_13; }
	inline String_t** get_address_of_m_ParticleName_13() { return &___m_ParticleName_13; }
	inline void set_m_ParticleName_13(String_t* value)
	{
		___m_ParticleName_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_ParticleName_13, value);
	}

	inline static int32_t get_offset_of_m_CurrentElementList_14() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_CurrentElementList_14)); }
	inline GameObjectU5BU5D_t3499186955* get_m_CurrentElementList_14() const { return ___m_CurrentElementList_14; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_m_CurrentElementList_14() { return &___m_CurrentElementList_14; }
	inline void set_m_CurrentElementList_14(GameObjectU5BU5D_t3499186955* value)
	{
		___m_CurrentElementList_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentElementList_14, value);
	}

	inline static int32_t get_offset_of_m_CurrentParticle_15() { return static_cast<int32_t>(offsetof(TestParticles_t3963318811, ___m_CurrentParticle_15)); }
	inline GameObject_t4012695102 * get_m_CurrentParticle_15() const { return ___m_CurrentParticle_15; }
	inline GameObject_t4012695102 ** get_address_of_m_CurrentParticle_15() { return &___m_CurrentParticle_15; }
	inline void set_m_CurrentParticle_15(GameObject_t4012695102 * value)
	{
		___m_CurrentParticle_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentParticle_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
