﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PetsFollow
struct  PetsFollow_t690634789  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform PetsFollow::_trans
	Transform_t284553113 * ____trans_2;
	// UnityEngine.Vector3 PetsFollow::_StartPos
	Vector3_t3525329789  ____StartPos_3;
	// System.Single PetsFollow::moveSpeed
	float ___moveSpeed_4;
	// UnityEngine.GameObject PetsFollow::wayPoint
	GameObject_t4012695102 * ___wayPoint_5;
	// UnityEngine.Vector3 PetsFollow::wayPointPos
	Vector3_t3525329789  ___wayPointPos_6;
	// System.Single PetsFollow::speed
	float ___speed_7;
	// System.Single PetsFollow::randomPos
	float ___randomPos_8;

public:
	inline static int32_t get_offset_of__trans_2() { return static_cast<int32_t>(offsetof(PetsFollow_t690634789, ____trans_2)); }
	inline Transform_t284553113 * get__trans_2() const { return ____trans_2; }
	inline Transform_t284553113 ** get_address_of__trans_2() { return &____trans_2; }
	inline void set__trans_2(Transform_t284553113 * value)
	{
		____trans_2 = value;
		Il2CppCodeGenWriteBarrier(&____trans_2, value);
	}

	inline static int32_t get_offset_of__StartPos_3() { return static_cast<int32_t>(offsetof(PetsFollow_t690634789, ____StartPos_3)); }
	inline Vector3_t3525329789  get__StartPos_3() const { return ____StartPos_3; }
	inline Vector3_t3525329789 * get_address_of__StartPos_3() { return &____StartPos_3; }
	inline void set__StartPos_3(Vector3_t3525329789  value)
	{
		____StartPos_3 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(PetsFollow_t690634789, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_wayPoint_5() { return static_cast<int32_t>(offsetof(PetsFollow_t690634789, ___wayPoint_5)); }
	inline GameObject_t4012695102 * get_wayPoint_5() const { return ___wayPoint_5; }
	inline GameObject_t4012695102 ** get_address_of_wayPoint_5() { return &___wayPoint_5; }
	inline void set_wayPoint_5(GameObject_t4012695102 * value)
	{
		___wayPoint_5 = value;
		Il2CppCodeGenWriteBarrier(&___wayPoint_5, value);
	}

	inline static int32_t get_offset_of_wayPointPos_6() { return static_cast<int32_t>(offsetof(PetsFollow_t690634789, ___wayPointPos_6)); }
	inline Vector3_t3525329789  get_wayPointPos_6() const { return ___wayPointPos_6; }
	inline Vector3_t3525329789 * get_address_of_wayPointPos_6() { return &___wayPointPos_6; }
	inline void set_wayPointPos_6(Vector3_t3525329789  value)
	{
		___wayPointPos_6 = value;
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(PetsFollow_t690634789, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}

	inline static int32_t get_offset_of_randomPos_8() { return static_cast<int32_t>(offsetof(PetsFollow_t690634789, ___randomPos_8)); }
	inline float get_randomPos_8() const { return ___randomPos_8; }
	inline float* get_address_of_randomPos_8() { return &___randomPos_8; }
	inline void set_randomPos_8(float value)
	{
		___randomPos_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
