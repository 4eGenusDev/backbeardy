﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// Parameters
struct Parameters_t2452200970;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnCollisionDie
struct  OnCollisionDie_t1988392845  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Vector3 OnCollisionDie::pos
	Vector3_t3525329789  ___pos_2;
	// UnityEngine.Vector3 OnCollisionDie::behindRocks
	Vector3_t3525329789  ___behindRocks_3;
	// UnityEngine.GameObject OnCollisionDie::go
	GameObject_t4012695102 * ___go_4;
	// Parameters OnCollisionDie::parameters
	Parameters_t2452200970 * ___parameters_5;
	// UnityEngine.GameObject OnCollisionDie::Sphere
	GameObject_t4012695102 * ___Sphere_6;
	// System.Int32 OnCollisionDie::currentLevel
	int32_t ___currentLevel_7;

public:
	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(OnCollisionDie_t1988392845, ___pos_2)); }
	inline Vector3_t3525329789  get_pos_2() const { return ___pos_2; }
	inline Vector3_t3525329789 * get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(Vector3_t3525329789  value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_behindRocks_3() { return static_cast<int32_t>(offsetof(OnCollisionDie_t1988392845, ___behindRocks_3)); }
	inline Vector3_t3525329789  get_behindRocks_3() const { return ___behindRocks_3; }
	inline Vector3_t3525329789 * get_address_of_behindRocks_3() { return &___behindRocks_3; }
	inline void set_behindRocks_3(Vector3_t3525329789  value)
	{
		___behindRocks_3 = value;
	}

	inline static int32_t get_offset_of_go_4() { return static_cast<int32_t>(offsetof(OnCollisionDie_t1988392845, ___go_4)); }
	inline GameObject_t4012695102 * get_go_4() const { return ___go_4; }
	inline GameObject_t4012695102 ** get_address_of_go_4() { return &___go_4; }
	inline void set_go_4(GameObject_t4012695102 * value)
	{
		___go_4 = value;
		Il2CppCodeGenWriteBarrier(&___go_4, value);
	}

	inline static int32_t get_offset_of_parameters_5() { return static_cast<int32_t>(offsetof(OnCollisionDie_t1988392845, ___parameters_5)); }
	inline Parameters_t2452200970 * get_parameters_5() const { return ___parameters_5; }
	inline Parameters_t2452200970 ** get_address_of_parameters_5() { return &___parameters_5; }
	inline void set_parameters_5(Parameters_t2452200970 * value)
	{
		___parameters_5 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_5, value);
	}

	inline static int32_t get_offset_of_Sphere_6() { return static_cast<int32_t>(offsetof(OnCollisionDie_t1988392845, ___Sphere_6)); }
	inline GameObject_t4012695102 * get_Sphere_6() const { return ___Sphere_6; }
	inline GameObject_t4012695102 ** get_address_of_Sphere_6() { return &___Sphere_6; }
	inline void set_Sphere_6(GameObject_t4012695102 * value)
	{
		___Sphere_6 = value;
		Il2CppCodeGenWriteBarrier(&___Sphere_6, value);
	}

	inline static int32_t get_offset_of_currentLevel_7() { return static_cast<int32_t>(offsetof(OnCollisionDie_t1988392845, ___currentLevel_7)); }
	inline int32_t get_currentLevel_7() const { return ___currentLevel_7; }
	inline int32_t* get_address_of_currentLevel_7() { return &___currentLevel_7; }
	inline void set_currentLevel_7(int32_t value)
	{
		___currentLevel_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
