﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Parameters
struct Parameters_t2452200970;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUpMove
struct  PowerUpMove_t2161606225  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Vector3 PowerUpMove::_startingPos
	Vector3_t3525329789  ____startingPos_2;
	// UnityEngine.Transform PowerUpMove::_trans
	Transform_t284553113 * ____trans_3;
	// System.Single PowerUpMove::moveSpeed
	float ___moveSpeed_4;
	// System.Single PowerUpMove::randomPos
	float ___randomPos_5;
	// UnityEngine.Vector3 PowerUpMove::myScale
	Vector3_t3525329789  ___myScale_6;
	// UnityEngine.Vector3 PowerUpMove::centerOfScreen
	Vector3_t3525329789  ___centerOfScreen_7;
	// UnityEngine.GameObject PowerUpMove::go
	GameObject_t4012695102 * ___go_8;
	// Parameters PowerUpMove::parameters
	Parameters_t2452200970 * ___parameters_9;

public:
	inline static int32_t get_offset_of__startingPos_2() { return static_cast<int32_t>(offsetof(PowerUpMove_t2161606225, ____startingPos_2)); }
	inline Vector3_t3525329789  get__startingPos_2() const { return ____startingPos_2; }
	inline Vector3_t3525329789 * get_address_of__startingPos_2() { return &____startingPos_2; }
	inline void set__startingPos_2(Vector3_t3525329789  value)
	{
		____startingPos_2 = value;
	}

	inline static int32_t get_offset_of__trans_3() { return static_cast<int32_t>(offsetof(PowerUpMove_t2161606225, ____trans_3)); }
	inline Transform_t284553113 * get__trans_3() const { return ____trans_3; }
	inline Transform_t284553113 ** get_address_of__trans_3() { return &____trans_3; }
	inline void set__trans_3(Transform_t284553113 * value)
	{
		____trans_3 = value;
		Il2CppCodeGenWriteBarrier(&____trans_3, value);
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(PowerUpMove_t2161606225, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_randomPos_5() { return static_cast<int32_t>(offsetof(PowerUpMove_t2161606225, ___randomPos_5)); }
	inline float get_randomPos_5() const { return ___randomPos_5; }
	inline float* get_address_of_randomPos_5() { return &___randomPos_5; }
	inline void set_randomPos_5(float value)
	{
		___randomPos_5 = value;
	}

	inline static int32_t get_offset_of_myScale_6() { return static_cast<int32_t>(offsetof(PowerUpMove_t2161606225, ___myScale_6)); }
	inline Vector3_t3525329789  get_myScale_6() const { return ___myScale_6; }
	inline Vector3_t3525329789 * get_address_of_myScale_6() { return &___myScale_6; }
	inline void set_myScale_6(Vector3_t3525329789  value)
	{
		___myScale_6 = value;
	}

	inline static int32_t get_offset_of_centerOfScreen_7() { return static_cast<int32_t>(offsetof(PowerUpMove_t2161606225, ___centerOfScreen_7)); }
	inline Vector3_t3525329789  get_centerOfScreen_7() const { return ___centerOfScreen_7; }
	inline Vector3_t3525329789 * get_address_of_centerOfScreen_7() { return &___centerOfScreen_7; }
	inline void set_centerOfScreen_7(Vector3_t3525329789  value)
	{
		___centerOfScreen_7 = value;
	}

	inline static int32_t get_offset_of_go_8() { return static_cast<int32_t>(offsetof(PowerUpMove_t2161606225, ___go_8)); }
	inline GameObject_t4012695102 * get_go_8() const { return ___go_8; }
	inline GameObject_t4012695102 ** get_address_of_go_8() { return &___go_8; }
	inline void set_go_8(GameObject_t4012695102 * value)
	{
		___go_8 = value;
		Il2CppCodeGenWriteBarrier(&___go_8, value);
	}

	inline static int32_t get_offset_of_parameters_9() { return static_cast<int32_t>(offsetof(PowerUpMove_t2161606225, ___parameters_9)); }
	inline Parameters_t2452200970 * get_parameters_9() const { return ___parameters_9; }
	inline Parameters_t2452200970 ** get_address_of_parameters_9() { return &___parameters_9; }
	inline void set_parameters_9(Parameters_t2452200970 * value)
	{
		___parameters_9 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
