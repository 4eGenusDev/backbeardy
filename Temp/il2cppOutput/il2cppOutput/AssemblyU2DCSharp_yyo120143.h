﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t792326996;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Parameters
struct Parameters_t2452200970;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// yyo
struct  yyo_t120143  : public MonoBehaviour_t3012272455
{
public:
	// System.Single yyo::lerpTime
	float ___lerpTime_2;
	// System.Single yyo::currentLerpTime
	float ___currentLerpTime_3;
	// System.Single yyo::moveDistance
	float ___moveDistance_4;
	// UnityEngine.Vector3 yyo::startPos
	Vector3_t3525329789  ___startPos_5;
	// UnityEngine.Vector3 yyo::endPos
	Vector3_t3525329789  ___endPos_6;
	// UnityEngine.Vector3 yyo::endPosOut
	Vector3_t3525329789  ___endPosOut_7;
	// UnityEngine.Animator yyo::myAni
	Animator_t792326996 * ___myAni_8;
	// UnityEngine.Transform yyo::tran1
	Transform_t284553113 * ___tran1_9;
	// UnityEngine.Transform yyo::tran2
	Transform_t284553113 * ___tran2_10;
	// System.Single yyo::yoyo
	float ___yoyo_11;
	// System.Single yyo::you
	float ___you_12;
	// System.Single yyo::perc
	float ___perc_13;
	// System.Boolean yyo::onlyOnceCheckpoint
	bool ___onlyOnceCheckpoint_14;
	// UnityEngine.AudioSource yyo::playSound
	AudioSource_t3628549054 * ___playSound_15;
	// UnityEngine.GameObject yyo::go
	GameObject_t4012695102 * ___go_16;
	// Parameters yyo::parameters
	Parameters_t2452200970 * ___parameters_17;
	// UnityEngine.GameObject yyo::house
	GameObject_t4012695102 * ___house_18;

public:
	inline static int32_t get_offset_of_lerpTime_2() { return static_cast<int32_t>(offsetof(yyo_t120143, ___lerpTime_2)); }
	inline float get_lerpTime_2() const { return ___lerpTime_2; }
	inline float* get_address_of_lerpTime_2() { return &___lerpTime_2; }
	inline void set_lerpTime_2(float value)
	{
		___lerpTime_2 = value;
	}

	inline static int32_t get_offset_of_currentLerpTime_3() { return static_cast<int32_t>(offsetof(yyo_t120143, ___currentLerpTime_3)); }
	inline float get_currentLerpTime_3() const { return ___currentLerpTime_3; }
	inline float* get_address_of_currentLerpTime_3() { return &___currentLerpTime_3; }
	inline void set_currentLerpTime_3(float value)
	{
		___currentLerpTime_3 = value;
	}

	inline static int32_t get_offset_of_moveDistance_4() { return static_cast<int32_t>(offsetof(yyo_t120143, ___moveDistance_4)); }
	inline float get_moveDistance_4() const { return ___moveDistance_4; }
	inline float* get_address_of_moveDistance_4() { return &___moveDistance_4; }
	inline void set_moveDistance_4(float value)
	{
		___moveDistance_4 = value;
	}

	inline static int32_t get_offset_of_startPos_5() { return static_cast<int32_t>(offsetof(yyo_t120143, ___startPos_5)); }
	inline Vector3_t3525329789  get_startPos_5() const { return ___startPos_5; }
	inline Vector3_t3525329789 * get_address_of_startPos_5() { return &___startPos_5; }
	inline void set_startPos_5(Vector3_t3525329789  value)
	{
		___startPos_5 = value;
	}

	inline static int32_t get_offset_of_endPos_6() { return static_cast<int32_t>(offsetof(yyo_t120143, ___endPos_6)); }
	inline Vector3_t3525329789  get_endPos_6() const { return ___endPos_6; }
	inline Vector3_t3525329789 * get_address_of_endPos_6() { return &___endPos_6; }
	inline void set_endPos_6(Vector3_t3525329789  value)
	{
		___endPos_6 = value;
	}

	inline static int32_t get_offset_of_endPosOut_7() { return static_cast<int32_t>(offsetof(yyo_t120143, ___endPosOut_7)); }
	inline Vector3_t3525329789  get_endPosOut_7() const { return ___endPosOut_7; }
	inline Vector3_t3525329789 * get_address_of_endPosOut_7() { return &___endPosOut_7; }
	inline void set_endPosOut_7(Vector3_t3525329789  value)
	{
		___endPosOut_7 = value;
	}

	inline static int32_t get_offset_of_myAni_8() { return static_cast<int32_t>(offsetof(yyo_t120143, ___myAni_8)); }
	inline Animator_t792326996 * get_myAni_8() const { return ___myAni_8; }
	inline Animator_t792326996 ** get_address_of_myAni_8() { return &___myAni_8; }
	inline void set_myAni_8(Animator_t792326996 * value)
	{
		___myAni_8 = value;
		Il2CppCodeGenWriteBarrier(&___myAni_8, value);
	}

	inline static int32_t get_offset_of_tran1_9() { return static_cast<int32_t>(offsetof(yyo_t120143, ___tran1_9)); }
	inline Transform_t284553113 * get_tran1_9() const { return ___tran1_9; }
	inline Transform_t284553113 ** get_address_of_tran1_9() { return &___tran1_9; }
	inline void set_tran1_9(Transform_t284553113 * value)
	{
		___tran1_9 = value;
		Il2CppCodeGenWriteBarrier(&___tran1_9, value);
	}

	inline static int32_t get_offset_of_tran2_10() { return static_cast<int32_t>(offsetof(yyo_t120143, ___tran2_10)); }
	inline Transform_t284553113 * get_tran2_10() const { return ___tran2_10; }
	inline Transform_t284553113 ** get_address_of_tran2_10() { return &___tran2_10; }
	inline void set_tran2_10(Transform_t284553113 * value)
	{
		___tran2_10 = value;
		Il2CppCodeGenWriteBarrier(&___tran2_10, value);
	}

	inline static int32_t get_offset_of_yoyo_11() { return static_cast<int32_t>(offsetof(yyo_t120143, ___yoyo_11)); }
	inline float get_yoyo_11() const { return ___yoyo_11; }
	inline float* get_address_of_yoyo_11() { return &___yoyo_11; }
	inline void set_yoyo_11(float value)
	{
		___yoyo_11 = value;
	}

	inline static int32_t get_offset_of_you_12() { return static_cast<int32_t>(offsetof(yyo_t120143, ___you_12)); }
	inline float get_you_12() const { return ___you_12; }
	inline float* get_address_of_you_12() { return &___you_12; }
	inline void set_you_12(float value)
	{
		___you_12 = value;
	}

	inline static int32_t get_offset_of_perc_13() { return static_cast<int32_t>(offsetof(yyo_t120143, ___perc_13)); }
	inline float get_perc_13() const { return ___perc_13; }
	inline float* get_address_of_perc_13() { return &___perc_13; }
	inline void set_perc_13(float value)
	{
		___perc_13 = value;
	}

	inline static int32_t get_offset_of_onlyOnceCheckpoint_14() { return static_cast<int32_t>(offsetof(yyo_t120143, ___onlyOnceCheckpoint_14)); }
	inline bool get_onlyOnceCheckpoint_14() const { return ___onlyOnceCheckpoint_14; }
	inline bool* get_address_of_onlyOnceCheckpoint_14() { return &___onlyOnceCheckpoint_14; }
	inline void set_onlyOnceCheckpoint_14(bool value)
	{
		___onlyOnceCheckpoint_14 = value;
	}

	inline static int32_t get_offset_of_playSound_15() { return static_cast<int32_t>(offsetof(yyo_t120143, ___playSound_15)); }
	inline AudioSource_t3628549054 * get_playSound_15() const { return ___playSound_15; }
	inline AudioSource_t3628549054 ** get_address_of_playSound_15() { return &___playSound_15; }
	inline void set_playSound_15(AudioSource_t3628549054 * value)
	{
		___playSound_15 = value;
		Il2CppCodeGenWriteBarrier(&___playSound_15, value);
	}

	inline static int32_t get_offset_of_go_16() { return static_cast<int32_t>(offsetof(yyo_t120143, ___go_16)); }
	inline GameObject_t4012695102 * get_go_16() const { return ___go_16; }
	inline GameObject_t4012695102 ** get_address_of_go_16() { return &___go_16; }
	inline void set_go_16(GameObject_t4012695102 * value)
	{
		___go_16 = value;
		Il2CppCodeGenWriteBarrier(&___go_16, value);
	}

	inline static int32_t get_offset_of_parameters_17() { return static_cast<int32_t>(offsetof(yyo_t120143, ___parameters_17)); }
	inline Parameters_t2452200970 * get_parameters_17() const { return ___parameters_17; }
	inline Parameters_t2452200970 ** get_address_of_parameters_17() { return &___parameters_17; }
	inline void set_parameters_17(Parameters_t2452200970 * value)
	{
		___parameters_17 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_17, value);
	}

	inline static int32_t get_offset_of_house_18() { return static_cast<int32_t>(offsetof(yyo_t120143, ___house_18)); }
	inline GameObject_t4012695102 * get_house_18() const { return ___house_18; }
	inline GameObject_t4012695102 ** get_address_of_house_18() { return &___house_18; }
	inline void set_house_18(GameObject_t4012695102 * value)
	{
		___house_18 = value;
		Il2CppCodeGenWriteBarrier(&___house_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
