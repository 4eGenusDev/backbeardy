﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SeaSpikyMovement
struct SeaSpikyMovement_t463284986;

#include "codegen/il2cpp-codegen.h"

// System.Void SeaSpikyMovement::.ctor()
extern "C"  void SeaSpikyMovement__ctor_m698332385 (SeaSpikyMovement_t463284986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SeaSpikyMovement::Start()
extern "C"  void SeaSpikyMovement_Start_m3940437473 (SeaSpikyMovement_t463284986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
