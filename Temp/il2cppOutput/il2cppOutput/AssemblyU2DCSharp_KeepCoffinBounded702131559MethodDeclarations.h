﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KeepCoffinBounded
struct KeepCoffinBounded_t702131559;

#include "codegen/il2cpp-codegen.h"

// System.Void KeepCoffinBounded::.ctor()
extern "C"  void KeepCoffinBounded__ctor_m1561928676 (KeepCoffinBounded_t702131559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KeepCoffinBounded::Start()
extern "C"  void KeepCoffinBounded_Start_m509066468 (KeepCoffinBounded_t702131559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KeepCoffinBounded::Update()
extern "C"  void KeepCoffinBounded_Update_m2902010793 (KeepCoffinBounded_t702131559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
