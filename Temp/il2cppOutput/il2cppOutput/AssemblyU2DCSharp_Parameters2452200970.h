﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Parameters
struct  Parameters_t2452200970  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject Parameters::GoCoins
	GameObject_t4012695102 * ___GoCoins_2;
	// UnityEngine.GameObject Parameters::GoEnemies
	GameObject_t4012695102 * ___GoEnemies_3;
	// UnityEngine.UI.Text Parameters::scoreText
	Text_t3286458198 * ___scoreText_4;
	// UnityEngine.UI.Text Parameters::enemiesText
	Text_t3286458198 * ___enemiesText_5;
	// System.Single Parameters::enemyBulletSpeed
	float ___enemyBulletSpeed_6;
	// System.Single Parameters::PlayerBulletSpeed
	float ___PlayerBulletSpeed_7;
	// System.Single Parameters::enemySpeed
	float ___enemySpeed_8;
	// System.Single Parameters::playerSpeed
	float ___playerSpeed_9;
	// System.Single Parameters::playerShootSpeed
	float ___playerShootSpeed_10;
	// System.Single Parameters::enemyShootSpeed
	float ___enemyShootSpeed_11;
	// System.Single Parameters::goldCoinSpeed
	float ___goldCoinSpeed_12;
	// System.Int32 Parameters::Coins
	int32_t ___Coins_13;
	// System.Int32 Parameters::Enemies
	int32_t ___Enemies_14;
	// System.Int32 Parameters::currentCheckPoint
	int32_t ___currentCheckPoint_15;
	// System.Single Parameters::spawnCoeficient
	float ___spawnCoeficient_16;

public:
	inline static int32_t get_offset_of_GoCoins_2() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___GoCoins_2)); }
	inline GameObject_t4012695102 * get_GoCoins_2() const { return ___GoCoins_2; }
	inline GameObject_t4012695102 ** get_address_of_GoCoins_2() { return &___GoCoins_2; }
	inline void set_GoCoins_2(GameObject_t4012695102 * value)
	{
		___GoCoins_2 = value;
		Il2CppCodeGenWriteBarrier(&___GoCoins_2, value);
	}

	inline static int32_t get_offset_of_GoEnemies_3() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___GoEnemies_3)); }
	inline GameObject_t4012695102 * get_GoEnemies_3() const { return ___GoEnemies_3; }
	inline GameObject_t4012695102 ** get_address_of_GoEnemies_3() { return &___GoEnemies_3; }
	inline void set_GoEnemies_3(GameObject_t4012695102 * value)
	{
		___GoEnemies_3 = value;
		Il2CppCodeGenWriteBarrier(&___GoEnemies_3, value);
	}

	inline static int32_t get_offset_of_scoreText_4() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___scoreText_4)); }
	inline Text_t3286458198 * get_scoreText_4() const { return ___scoreText_4; }
	inline Text_t3286458198 ** get_address_of_scoreText_4() { return &___scoreText_4; }
	inline void set_scoreText_4(Text_t3286458198 * value)
	{
		___scoreText_4 = value;
		Il2CppCodeGenWriteBarrier(&___scoreText_4, value);
	}

	inline static int32_t get_offset_of_enemiesText_5() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___enemiesText_5)); }
	inline Text_t3286458198 * get_enemiesText_5() const { return ___enemiesText_5; }
	inline Text_t3286458198 ** get_address_of_enemiesText_5() { return &___enemiesText_5; }
	inline void set_enemiesText_5(Text_t3286458198 * value)
	{
		___enemiesText_5 = value;
		Il2CppCodeGenWriteBarrier(&___enemiesText_5, value);
	}

	inline static int32_t get_offset_of_enemyBulletSpeed_6() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___enemyBulletSpeed_6)); }
	inline float get_enemyBulletSpeed_6() const { return ___enemyBulletSpeed_6; }
	inline float* get_address_of_enemyBulletSpeed_6() { return &___enemyBulletSpeed_6; }
	inline void set_enemyBulletSpeed_6(float value)
	{
		___enemyBulletSpeed_6 = value;
	}

	inline static int32_t get_offset_of_PlayerBulletSpeed_7() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___PlayerBulletSpeed_7)); }
	inline float get_PlayerBulletSpeed_7() const { return ___PlayerBulletSpeed_7; }
	inline float* get_address_of_PlayerBulletSpeed_7() { return &___PlayerBulletSpeed_7; }
	inline void set_PlayerBulletSpeed_7(float value)
	{
		___PlayerBulletSpeed_7 = value;
	}

	inline static int32_t get_offset_of_enemySpeed_8() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___enemySpeed_8)); }
	inline float get_enemySpeed_8() const { return ___enemySpeed_8; }
	inline float* get_address_of_enemySpeed_8() { return &___enemySpeed_8; }
	inline void set_enemySpeed_8(float value)
	{
		___enemySpeed_8 = value;
	}

	inline static int32_t get_offset_of_playerSpeed_9() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___playerSpeed_9)); }
	inline float get_playerSpeed_9() const { return ___playerSpeed_9; }
	inline float* get_address_of_playerSpeed_9() { return &___playerSpeed_9; }
	inline void set_playerSpeed_9(float value)
	{
		___playerSpeed_9 = value;
	}

	inline static int32_t get_offset_of_playerShootSpeed_10() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___playerShootSpeed_10)); }
	inline float get_playerShootSpeed_10() const { return ___playerShootSpeed_10; }
	inline float* get_address_of_playerShootSpeed_10() { return &___playerShootSpeed_10; }
	inline void set_playerShootSpeed_10(float value)
	{
		___playerShootSpeed_10 = value;
	}

	inline static int32_t get_offset_of_enemyShootSpeed_11() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___enemyShootSpeed_11)); }
	inline float get_enemyShootSpeed_11() const { return ___enemyShootSpeed_11; }
	inline float* get_address_of_enemyShootSpeed_11() { return &___enemyShootSpeed_11; }
	inline void set_enemyShootSpeed_11(float value)
	{
		___enemyShootSpeed_11 = value;
	}

	inline static int32_t get_offset_of_goldCoinSpeed_12() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___goldCoinSpeed_12)); }
	inline float get_goldCoinSpeed_12() const { return ___goldCoinSpeed_12; }
	inline float* get_address_of_goldCoinSpeed_12() { return &___goldCoinSpeed_12; }
	inline void set_goldCoinSpeed_12(float value)
	{
		___goldCoinSpeed_12 = value;
	}

	inline static int32_t get_offset_of_Coins_13() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___Coins_13)); }
	inline int32_t get_Coins_13() const { return ___Coins_13; }
	inline int32_t* get_address_of_Coins_13() { return &___Coins_13; }
	inline void set_Coins_13(int32_t value)
	{
		___Coins_13 = value;
	}

	inline static int32_t get_offset_of_Enemies_14() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___Enemies_14)); }
	inline int32_t get_Enemies_14() const { return ___Enemies_14; }
	inline int32_t* get_address_of_Enemies_14() { return &___Enemies_14; }
	inline void set_Enemies_14(int32_t value)
	{
		___Enemies_14 = value;
	}

	inline static int32_t get_offset_of_currentCheckPoint_15() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___currentCheckPoint_15)); }
	inline int32_t get_currentCheckPoint_15() const { return ___currentCheckPoint_15; }
	inline int32_t* get_address_of_currentCheckPoint_15() { return &___currentCheckPoint_15; }
	inline void set_currentCheckPoint_15(int32_t value)
	{
		___currentCheckPoint_15 = value;
	}

	inline static int32_t get_offset_of_spawnCoeficient_16() { return static_cast<int32_t>(offsetof(Parameters_t2452200970, ___spawnCoeficient_16)); }
	inline float get_spawnCoeficient_16() const { return ___spawnCoeficient_16; }
	inline float* get_address_of_spawnCoeficient_16() { return &___spawnCoeficient_16; }
	inline void set_spawnCoeficient_16(float value)
	{
		___spawnCoeficient_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
