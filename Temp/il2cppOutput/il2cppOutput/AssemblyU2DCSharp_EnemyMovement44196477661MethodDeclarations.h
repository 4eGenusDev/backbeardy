﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyMovement4
struct EnemyMovement4_t4196477661;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyMovement4::.ctor()
extern "C"  void EnemyMovement4__ctor_m822774366 (EnemyMovement4_t4196477661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement4::Start()
extern "C"  void EnemyMovement4_Start_m4064879454 (EnemyMovement4_t4196477661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement4::Update()
extern "C"  void EnemyMovement4_Update_m1463063663 (EnemyMovement4_t4196477661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
