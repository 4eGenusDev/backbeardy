﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// Parameters
struct Parameters_t2452200970;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PShoot
struct  PShoot_t2370192815  : public MonoBehaviour_t3012272455
{
public:
	// System.Single PShoot::moveSpeed
	float ___moveSpeed_2;
	// UnityEngine.GameObject PShoot::go
	GameObject_t4012695102 * ___go_3;
	// Parameters PShoot::parameters
	Parameters_t2452200970 * ___parameters_4;

public:
	inline static int32_t get_offset_of_moveSpeed_2() { return static_cast<int32_t>(offsetof(PShoot_t2370192815, ___moveSpeed_2)); }
	inline float get_moveSpeed_2() const { return ___moveSpeed_2; }
	inline float* get_address_of_moveSpeed_2() { return &___moveSpeed_2; }
	inline void set_moveSpeed_2(float value)
	{
		___moveSpeed_2 = value;
	}

	inline static int32_t get_offset_of_go_3() { return static_cast<int32_t>(offsetof(PShoot_t2370192815, ___go_3)); }
	inline GameObject_t4012695102 * get_go_3() const { return ___go_3; }
	inline GameObject_t4012695102 ** get_address_of_go_3() { return &___go_3; }
	inline void set_go_3(GameObject_t4012695102 * value)
	{
		___go_3 = value;
		Il2CppCodeGenWriteBarrier(&___go_3, value);
	}

	inline static int32_t get_offset_of_parameters_4() { return static_cast<int32_t>(offsetof(PShoot_t2370192815, ___parameters_4)); }
	inline Parameters_t2452200970 * get_parameters_4() const { return ___parameters_4; }
	inline Parameters_t2452200970 ** get_address_of_parameters_4() { return &___parameters_4; }
	inline void set_parameters_4(Parameters_t2452200970 * value)
	{
		___parameters_4 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
