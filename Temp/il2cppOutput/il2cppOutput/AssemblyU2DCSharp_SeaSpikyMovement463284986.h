﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t792326996;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeaSpikyMovement
struct  SeaSpikyMovement_t463284986  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Animator SeaSpikyMovement::myAni
	Animator_t792326996 * ___myAni_2;
	// UnityEngine.GameObject SeaSpikyMovement::spikes
	GameObject_t4012695102 * ___spikes_3;

public:
	inline static int32_t get_offset_of_myAni_2() { return static_cast<int32_t>(offsetof(SeaSpikyMovement_t463284986, ___myAni_2)); }
	inline Animator_t792326996 * get_myAni_2() const { return ___myAni_2; }
	inline Animator_t792326996 ** get_address_of_myAni_2() { return &___myAni_2; }
	inline void set_myAni_2(Animator_t792326996 * value)
	{
		___myAni_2 = value;
		Il2CppCodeGenWriteBarrier(&___myAni_2, value);
	}

	inline static int32_t get_offset_of_spikes_3() { return static_cast<int32_t>(offsetof(SeaSpikyMovement_t463284986, ___spikes_3)); }
	inline GameObject_t4012695102 * get_spikes_3() const { return ___spikes_3; }
	inline GameObject_t4012695102 ** get_address_of_spikes_3() { return &___spikes_3; }
	inline void set_spikes_3(GameObject_t4012695102 * value)
	{
		___spikes_3 = value;
		Il2CppCodeGenWriteBarrier(&___spikes_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
