﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainMenu
struct MainMenu_t55996120;

#include "codegen/il2cpp-codegen.h"

// System.Void MainMenu::.ctor()
extern "C"  void MainMenu__ctor_m310215363 (MainMenu_t55996120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::Start()
extern "C"  void MainMenu_Start_m3552320451 (MainMenu_t55996120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::Update()
extern "C"  void MainMenu_Update_m2753603754 (MainMenu_t55996120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::doBlend()
extern "C"  void MainMenu_doBlend_m863553799 (MainMenu_t55996120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
