﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Animator
struct Animator_t792326996;
// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyMovement3
struct  EnemyMovement3_t4196477660  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform EnemyMovement3::_trans
	Transform_t284553113 * ____trans_2;
	// UnityEngine.Vector3 EnemyMovement3::_StartPos
	Vector3_t3525329789  ____StartPos_3;
	// System.Single EnemyMovement3::moveSpeed
	float ___moveSpeed_4;
	// UnityEngine.GameObject EnemyMovement3::player
	GameObject_t4012695102 * ___player_5;
	// UnityEngine.Animator EnemyMovement3::ani
	Animator_t792326996 * ___ani_6;
	// UnityEngine.Object EnemyMovement3::go
	Object_t3878351788 * ___go_7;
	// System.Boolean EnemyMovement3::explodeOnce
	bool ___explodeOnce_8;

public:
	inline static int32_t get_offset_of__trans_2() { return static_cast<int32_t>(offsetof(EnemyMovement3_t4196477660, ____trans_2)); }
	inline Transform_t284553113 * get__trans_2() const { return ____trans_2; }
	inline Transform_t284553113 ** get_address_of__trans_2() { return &____trans_2; }
	inline void set__trans_2(Transform_t284553113 * value)
	{
		____trans_2 = value;
		Il2CppCodeGenWriteBarrier(&____trans_2, value);
	}

	inline static int32_t get_offset_of__StartPos_3() { return static_cast<int32_t>(offsetof(EnemyMovement3_t4196477660, ____StartPos_3)); }
	inline Vector3_t3525329789  get__StartPos_3() const { return ____StartPos_3; }
	inline Vector3_t3525329789 * get_address_of__StartPos_3() { return &____StartPos_3; }
	inline void set__StartPos_3(Vector3_t3525329789  value)
	{
		____StartPos_3 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(EnemyMovement3_t4196477660, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(EnemyMovement3_t4196477660, ___player_5)); }
	inline GameObject_t4012695102 * get_player_5() const { return ___player_5; }
	inline GameObject_t4012695102 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(GameObject_t4012695102 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier(&___player_5, value);
	}

	inline static int32_t get_offset_of_ani_6() { return static_cast<int32_t>(offsetof(EnemyMovement3_t4196477660, ___ani_6)); }
	inline Animator_t792326996 * get_ani_6() const { return ___ani_6; }
	inline Animator_t792326996 ** get_address_of_ani_6() { return &___ani_6; }
	inline void set_ani_6(Animator_t792326996 * value)
	{
		___ani_6 = value;
		Il2CppCodeGenWriteBarrier(&___ani_6, value);
	}

	inline static int32_t get_offset_of_go_7() { return static_cast<int32_t>(offsetof(EnemyMovement3_t4196477660, ___go_7)); }
	inline Object_t3878351788 * get_go_7() const { return ___go_7; }
	inline Object_t3878351788 ** get_address_of_go_7() { return &___go_7; }
	inline void set_go_7(Object_t3878351788 * value)
	{
		___go_7 = value;
		Il2CppCodeGenWriteBarrier(&___go_7, value);
	}

	inline static int32_t get_offset_of_explodeOnce_8() { return static_cast<int32_t>(offsetof(EnemyMovement3_t4196477660, ___explodeOnce_8)); }
	inline bool get_explodeOnce_8() const { return ___explodeOnce_8; }
	inline bool* get_address_of_explodeOnce_8() { return &___explodeOnce_8; }
	inline void set_explodeOnce_8(bool value)
	{
		___explodeOnce_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
