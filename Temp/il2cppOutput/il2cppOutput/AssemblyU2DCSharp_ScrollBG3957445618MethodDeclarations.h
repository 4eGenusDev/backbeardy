﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollBG
struct ScrollBG_t3957445618;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollBG::.ctor()
extern "C"  void ScrollBG__ctor_m1600731881 (ScrollBG_t3957445618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollBG::Update()
extern "C"  void ScrollBG_Update_m4104910148 (ScrollBG_t3957445618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
