﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UnityEngine.Animator
struct Animator_t792326996;
// UnityEngine.UI.Slider
struct Slider_t1468074762;
// UnityEngine.UI.Toggle
struct Toggle_t1499417981;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainButtons
struct  MainButtons_t1024182120  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject MainButtons::Play
	GameObject_t4012695102 * ___Play_2;
	// UnityEngine.GameObject MainButtons::Settings
	GameObject_t4012695102 * ___Settings_3;
	// UnityEngine.GameObject MainButtons::Store
	GameObject_t4012695102 * ___Store_4;
	// UnityEngine.GameObject MainButtons::Share
	GameObject_t4012695102 * ___Share_5;
	// UnityEngine.GameObject MainButtons::Back
	GameObject_t4012695102 * ___Back_6;
	// UnityEngine.AudioSource MainButtons::audioPlay
	AudioSource_t3628549054 * ___audioPlay_7;
	// UnityEngine.AudioClip MainButtons::buttonPlay
	AudioClip_t3714538611 * ___buttonPlay_8;
	// UnityEngine.AudioClip MainButtons::buttonCancel
	AudioClip_t3714538611 * ___buttonCancel_9;
	// UnityEngine.UI.Text MainButtons::soundText
	Text_t3286458198 * ___soundText_10;
	// UnityEngine.UI.Text MainButtons::notificationsText
	Text_t3286458198 * ___notificationsText_11;
	// UnityEngine.Animator MainButtons::shareAni
	Animator_t792326996 * ___shareAni_12;
	// UnityEngine.Animator MainButtons::mainAni
	Animator_t792326996 * ___mainAni_13;
	// System.Int32 MainButtons::menuCounter
	int32_t ___menuCounter_14;
	// System.Int32 MainButtons::hasPlayed
	int32_t ___hasPlayed_15;
	// System.Single MainButtons::sensitivity
	float ___sensitivity_16;
	// UnityEngine.UI.Slider MainButtons::Slider
	Slider_t1468074762 * ___Slider_17;
	// System.Single MainButtons::sliderValue
	float ___sliderValue_18;
	// UnityEngine.UI.Toggle MainButtons::toggleSounds
	Toggle_t1499417981 * ___toggleSounds_19;

public:
	inline static int32_t get_offset_of_Play_2() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___Play_2)); }
	inline GameObject_t4012695102 * get_Play_2() const { return ___Play_2; }
	inline GameObject_t4012695102 ** get_address_of_Play_2() { return &___Play_2; }
	inline void set_Play_2(GameObject_t4012695102 * value)
	{
		___Play_2 = value;
		Il2CppCodeGenWriteBarrier(&___Play_2, value);
	}

	inline static int32_t get_offset_of_Settings_3() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___Settings_3)); }
	inline GameObject_t4012695102 * get_Settings_3() const { return ___Settings_3; }
	inline GameObject_t4012695102 ** get_address_of_Settings_3() { return &___Settings_3; }
	inline void set_Settings_3(GameObject_t4012695102 * value)
	{
		___Settings_3 = value;
		Il2CppCodeGenWriteBarrier(&___Settings_3, value);
	}

	inline static int32_t get_offset_of_Store_4() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___Store_4)); }
	inline GameObject_t4012695102 * get_Store_4() const { return ___Store_4; }
	inline GameObject_t4012695102 ** get_address_of_Store_4() { return &___Store_4; }
	inline void set_Store_4(GameObject_t4012695102 * value)
	{
		___Store_4 = value;
		Il2CppCodeGenWriteBarrier(&___Store_4, value);
	}

	inline static int32_t get_offset_of_Share_5() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___Share_5)); }
	inline GameObject_t4012695102 * get_Share_5() const { return ___Share_5; }
	inline GameObject_t4012695102 ** get_address_of_Share_5() { return &___Share_5; }
	inline void set_Share_5(GameObject_t4012695102 * value)
	{
		___Share_5 = value;
		Il2CppCodeGenWriteBarrier(&___Share_5, value);
	}

	inline static int32_t get_offset_of_Back_6() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___Back_6)); }
	inline GameObject_t4012695102 * get_Back_6() const { return ___Back_6; }
	inline GameObject_t4012695102 ** get_address_of_Back_6() { return &___Back_6; }
	inline void set_Back_6(GameObject_t4012695102 * value)
	{
		___Back_6 = value;
		Il2CppCodeGenWriteBarrier(&___Back_6, value);
	}

	inline static int32_t get_offset_of_audioPlay_7() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___audioPlay_7)); }
	inline AudioSource_t3628549054 * get_audioPlay_7() const { return ___audioPlay_7; }
	inline AudioSource_t3628549054 ** get_address_of_audioPlay_7() { return &___audioPlay_7; }
	inline void set_audioPlay_7(AudioSource_t3628549054 * value)
	{
		___audioPlay_7 = value;
		Il2CppCodeGenWriteBarrier(&___audioPlay_7, value);
	}

	inline static int32_t get_offset_of_buttonPlay_8() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___buttonPlay_8)); }
	inline AudioClip_t3714538611 * get_buttonPlay_8() const { return ___buttonPlay_8; }
	inline AudioClip_t3714538611 ** get_address_of_buttonPlay_8() { return &___buttonPlay_8; }
	inline void set_buttonPlay_8(AudioClip_t3714538611 * value)
	{
		___buttonPlay_8 = value;
		Il2CppCodeGenWriteBarrier(&___buttonPlay_8, value);
	}

	inline static int32_t get_offset_of_buttonCancel_9() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___buttonCancel_9)); }
	inline AudioClip_t3714538611 * get_buttonCancel_9() const { return ___buttonCancel_9; }
	inline AudioClip_t3714538611 ** get_address_of_buttonCancel_9() { return &___buttonCancel_9; }
	inline void set_buttonCancel_9(AudioClip_t3714538611 * value)
	{
		___buttonCancel_9 = value;
		Il2CppCodeGenWriteBarrier(&___buttonCancel_9, value);
	}

	inline static int32_t get_offset_of_soundText_10() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___soundText_10)); }
	inline Text_t3286458198 * get_soundText_10() const { return ___soundText_10; }
	inline Text_t3286458198 ** get_address_of_soundText_10() { return &___soundText_10; }
	inline void set_soundText_10(Text_t3286458198 * value)
	{
		___soundText_10 = value;
		Il2CppCodeGenWriteBarrier(&___soundText_10, value);
	}

	inline static int32_t get_offset_of_notificationsText_11() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___notificationsText_11)); }
	inline Text_t3286458198 * get_notificationsText_11() const { return ___notificationsText_11; }
	inline Text_t3286458198 ** get_address_of_notificationsText_11() { return &___notificationsText_11; }
	inline void set_notificationsText_11(Text_t3286458198 * value)
	{
		___notificationsText_11 = value;
		Il2CppCodeGenWriteBarrier(&___notificationsText_11, value);
	}

	inline static int32_t get_offset_of_shareAni_12() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___shareAni_12)); }
	inline Animator_t792326996 * get_shareAni_12() const { return ___shareAni_12; }
	inline Animator_t792326996 ** get_address_of_shareAni_12() { return &___shareAni_12; }
	inline void set_shareAni_12(Animator_t792326996 * value)
	{
		___shareAni_12 = value;
		Il2CppCodeGenWriteBarrier(&___shareAni_12, value);
	}

	inline static int32_t get_offset_of_mainAni_13() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___mainAni_13)); }
	inline Animator_t792326996 * get_mainAni_13() const { return ___mainAni_13; }
	inline Animator_t792326996 ** get_address_of_mainAni_13() { return &___mainAni_13; }
	inline void set_mainAni_13(Animator_t792326996 * value)
	{
		___mainAni_13 = value;
		Il2CppCodeGenWriteBarrier(&___mainAni_13, value);
	}

	inline static int32_t get_offset_of_menuCounter_14() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___menuCounter_14)); }
	inline int32_t get_menuCounter_14() const { return ___menuCounter_14; }
	inline int32_t* get_address_of_menuCounter_14() { return &___menuCounter_14; }
	inline void set_menuCounter_14(int32_t value)
	{
		___menuCounter_14 = value;
	}

	inline static int32_t get_offset_of_hasPlayed_15() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___hasPlayed_15)); }
	inline int32_t get_hasPlayed_15() const { return ___hasPlayed_15; }
	inline int32_t* get_address_of_hasPlayed_15() { return &___hasPlayed_15; }
	inline void set_hasPlayed_15(int32_t value)
	{
		___hasPlayed_15 = value;
	}

	inline static int32_t get_offset_of_sensitivity_16() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___sensitivity_16)); }
	inline float get_sensitivity_16() const { return ___sensitivity_16; }
	inline float* get_address_of_sensitivity_16() { return &___sensitivity_16; }
	inline void set_sensitivity_16(float value)
	{
		___sensitivity_16 = value;
	}

	inline static int32_t get_offset_of_Slider_17() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___Slider_17)); }
	inline Slider_t1468074762 * get_Slider_17() const { return ___Slider_17; }
	inline Slider_t1468074762 ** get_address_of_Slider_17() { return &___Slider_17; }
	inline void set_Slider_17(Slider_t1468074762 * value)
	{
		___Slider_17 = value;
		Il2CppCodeGenWriteBarrier(&___Slider_17, value);
	}

	inline static int32_t get_offset_of_sliderValue_18() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___sliderValue_18)); }
	inline float get_sliderValue_18() const { return ___sliderValue_18; }
	inline float* get_address_of_sliderValue_18() { return &___sliderValue_18; }
	inline void set_sliderValue_18(float value)
	{
		___sliderValue_18 = value;
	}

	inline static int32_t get_offset_of_toggleSounds_19() { return static_cast<int32_t>(offsetof(MainButtons_t1024182120, ___toggleSounds_19)); }
	inline Toggle_t1499417981 * get_toggleSounds_19() const { return ___toggleSounds_19; }
	inline Toggle_t1499417981 ** get_address_of_toggleSounds_19() { return &___toggleSounds_19; }
	inline void set_toggleSounds_19(Toggle_t1499417981 * value)
	{
		___toggleSounds_19 = value;
		Il2CppCodeGenWriteBarrier(&___toggleSounds_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
