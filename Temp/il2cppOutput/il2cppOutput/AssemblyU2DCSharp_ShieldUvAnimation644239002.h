﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Material
struct Material_t1886596500;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldUvAnimation
struct  ShieldUvAnimation_t644239002  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject ShieldUvAnimation::iShield
	GameObject_t4012695102 * ___iShield_2;
	// System.Single ShieldUvAnimation::iSpeed
	float ___iSpeed_3;
	// UnityEngine.Material ShieldUvAnimation::mMaterial
	Material_t1886596500 * ___mMaterial_4;
	// System.Single ShieldUvAnimation::mTime
	float ___mTime_5;

public:
	inline static int32_t get_offset_of_iShield_2() { return static_cast<int32_t>(offsetof(ShieldUvAnimation_t644239002, ___iShield_2)); }
	inline GameObject_t4012695102 * get_iShield_2() const { return ___iShield_2; }
	inline GameObject_t4012695102 ** get_address_of_iShield_2() { return &___iShield_2; }
	inline void set_iShield_2(GameObject_t4012695102 * value)
	{
		___iShield_2 = value;
		Il2CppCodeGenWriteBarrier(&___iShield_2, value);
	}

	inline static int32_t get_offset_of_iSpeed_3() { return static_cast<int32_t>(offsetof(ShieldUvAnimation_t644239002, ___iSpeed_3)); }
	inline float get_iSpeed_3() const { return ___iSpeed_3; }
	inline float* get_address_of_iSpeed_3() { return &___iSpeed_3; }
	inline void set_iSpeed_3(float value)
	{
		___iSpeed_3 = value;
	}

	inline static int32_t get_offset_of_mMaterial_4() { return static_cast<int32_t>(offsetof(ShieldUvAnimation_t644239002, ___mMaterial_4)); }
	inline Material_t1886596500 * get_mMaterial_4() const { return ___mMaterial_4; }
	inline Material_t1886596500 ** get_address_of_mMaterial_4() { return &___mMaterial_4; }
	inline void set_mMaterial_4(Material_t1886596500 * value)
	{
		___mMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&___mMaterial_4, value);
	}

	inline static int32_t get_offset_of_mTime_5() { return static_cast<int32_t>(offsetof(ShieldUvAnimation_t644239002, ___mTime_5)); }
	inline float get_mTime_5() const { return ___mTime_5; }
	inline float* get_address_of_mTime_5() { return &___mTime_5; }
	inline void set_mTime_5(float value)
	{
		___mTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
