﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DestroyOutOFBounds
struct DestroyOutOFBounds_t2076988544;

#include "codegen/il2cpp-codegen.h"

// System.Void DestroyOutOFBounds::.ctor()
extern "C"  void DestroyOutOFBounds__ctor_m531676187 (DestroyOutOFBounds_t2076988544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOutOFBounds::Start()
extern "C"  void DestroyOutOFBounds_Start_m3773781275 (DestroyOutOFBounds_t2076988544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOutOFBounds::Update()
extern "C"  void DestroyOutOFBounds_Update_m1028954706 (DestroyOutOFBounds_t2076988544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOutOFBounds::DieOutOfBounds()
extern "C"  void DestroyOutOFBounds_DieOutOfBounds_m3512348035 (DestroyOutOFBounds_t2076988544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
