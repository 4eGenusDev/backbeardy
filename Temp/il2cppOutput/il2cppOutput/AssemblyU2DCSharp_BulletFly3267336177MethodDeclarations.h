﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BulletFly
struct BulletFly_t3267336177;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void BulletFly::.ctor()
extern "C"  void BulletFly__ctor_m2887310490 (BulletFly_t3267336177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BulletFly::Start()
extern "C"  void BulletFly_Start_m1834448282 (BulletFly_t3267336177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BulletFly::Update()
extern "C"  void BulletFly_Update_m1039174067 (BulletFly_t3267336177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BulletFly::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void BulletFly_OnTriggerEnter2D_m3331571230 (BulletFly_t3267336177 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
