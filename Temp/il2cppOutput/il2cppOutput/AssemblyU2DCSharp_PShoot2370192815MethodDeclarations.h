﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PShoot
struct PShoot_t2370192815;

#include "codegen/il2cpp-codegen.h"

// System.Void PShoot::.ctor()
extern "C"  void PShoot__ctor_m393417676 (PShoot_t2370192815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PShoot::Start()
extern "C"  void PShoot_Start_m3635522764 (PShoot_t2370192815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PShoot::Update()
extern "C"  void PShoot_Update_m1037908161 (PShoot_t2370192815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PShoot::Shoot()
extern "C"  void PShoot_Shoot_m3304812873 (PShoot_t2370192815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
