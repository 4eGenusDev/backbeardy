﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider2D
struct BoxCollider2D_t262790558;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpawnCoins
struct  SpawnCoins_t3299938663  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.BoxCollider2D SpawnCoins::myBox
	BoxCollider2D_t262790558 * ___myBox_2;
	// UnityEngine.Vector3 SpawnCoins::pos
	Vector3_t3525329789  ___pos_3;
	// System.Int32 SpawnCoins::randPowerup
	int32_t ___randPowerup_4;
	// System.Single SpawnCoins::randTime
	float ___randTime_5;
	// System.Single SpawnCoins::x1
	float ___x1_6;
	// System.Single SpawnCoins::x2
	float ___x2_7;

public:
	inline static int32_t get_offset_of_myBox_2() { return static_cast<int32_t>(offsetof(SpawnCoins_t3299938663, ___myBox_2)); }
	inline BoxCollider2D_t262790558 * get_myBox_2() const { return ___myBox_2; }
	inline BoxCollider2D_t262790558 ** get_address_of_myBox_2() { return &___myBox_2; }
	inline void set_myBox_2(BoxCollider2D_t262790558 * value)
	{
		___myBox_2 = value;
		Il2CppCodeGenWriteBarrier(&___myBox_2, value);
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(SpawnCoins_t3299938663, ___pos_3)); }
	inline Vector3_t3525329789  get_pos_3() const { return ___pos_3; }
	inline Vector3_t3525329789 * get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(Vector3_t3525329789  value)
	{
		___pos_3 = value;
	}

	inline static int32_t get_offset_of_randPowerup_4() { return static_cast<int32_t>(offsetof(SpawnCoins_t3299938663, ___randPowerup_4)); }
	inline int32_t get_randPowerup_4() const { return ___randPowerup_4; }
	inline int32_t* get_address_of_randPowerup_4() { return &___randPowerup_4; }
	inline void set_randPowerup_4(int32_t value)
	{
		___randPowerup_4 = value;
	}

	inline static int32_t get_offset_of_randTime_5() { return static_cast<int32_t>(offsetof(SpawnCoins_t3299938663, ___randTime_5)); }
	inline float get_randTime_5() const { return ___randTime_5; }
	inline float* get_address_of_randTime_5() { return &___randTime_5; }
	inline void set_randTime_5(float value)
	{
		___randTime_5 = value;
	}

	inline static int32_t get_offset_of_x1_6() { return static_cast<int32_t>(offsetof(SpawnCoins_t3299938663, ___x1_6)); }
	inline float get_x1_6() const { return ___x1_6; }
	inline float* get_address_of_x1_6() { return &___x1_6; }
	inline void set_x1_6(float value)
	{
		___x1_6 = value;
	}

	inline static int32_t get_offset_of_x2_7() { return static_cast<int32_t>(offsetof(SpawnCoins_t3299938663, ___x2_7)); }
	inline float get_x2_7() const { return ___x2_7; }
	inline float* get_address_of_x2_7() { return &___x2_7; }
	inline void set_x2_7(float value)
	{
		___x2_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
