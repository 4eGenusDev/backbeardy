﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FrostEffect
struct FrostEffect_t3874425173;
// UnityEngine.RenderTexture
struct RenderTexture_t12905170;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture12905170.h"

// System.Void FrostEffect::.ctor()
extern "C"  void FrostEffect__ctor_m2054758838 (FrostEffect_t3874425173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrostEffect::Awake()
extern "C"  void FrostEffect_Awake_m2292364057 (FrostEffect_t3874425173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrostEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void FrostEffect_OnRenderImage_m2495992040 (FrostEffect_t3874425173 * __this, RenderTexture_t12905170 * ___source, RenderTexture_t12905170 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
