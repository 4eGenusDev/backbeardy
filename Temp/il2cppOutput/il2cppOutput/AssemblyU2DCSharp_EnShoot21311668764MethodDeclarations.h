﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnShoot2
struct EnShoot2_t1311668764;

#include "codegen/il2cpp-codegen.h"

// System.Void EnShoot2::.ctor()
extern "C"  void EnShoot2__ctor_m2248401919 (EnShoot2_t1311668764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot2::Start()
extern "C"  void EnShoot2_Start_m1195539711 (EnShoot2_t1311668764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot2::Update()
extern "C"  void EnShoot2_Update_m2707844846 (EnShoot2_t1311668764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot2::Shoot()
extern "C"  void EnShoot2_Shoot_m864829820 (EnShoot2_t1311668764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
