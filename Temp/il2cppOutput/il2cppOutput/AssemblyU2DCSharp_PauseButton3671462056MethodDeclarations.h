﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PauseButton
struct PauseButton_t3671462056;

#include "codegen/il2cpp-codegen.h"

// System.Void PauseButton::.ctor()
extern "C"  void PauseButton__ctor_m811182019 (PauseButton_t3671462056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseButton::Start()
extern "C"  void PauseButton_Start_m4053287107 (PauseButton_t3671462056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseButton::OnPause()
extern "C"  void PauseButton_OnPause_m2350323320 (PauseButton_t3671462056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseButton::OnResume()
extern "C"  void PauseButton_OnResume_m2531132685 (PauseButton_t3671462056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseButton::TryAgain()
extern "C"  void PauseButton_TryAgain_m4206283334 (PauseButton_t3671462056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseButton::GoToMenu()
extern "C"  void PauseButton_GoToMenu_m668902947 (PauseButton_t3671462056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
