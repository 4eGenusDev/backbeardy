﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Parameters
struct Parameters_t2452200970;

#include "codegen/il2cpp-codegen.h"

// System.Void Parameters::.ctor()
extern "C"  void Parameters__ctor_m1181885905 (Parameters_t2452200970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Parameters::Start()
extern "C"  void Parameters_Start_m129023697 (Parameters_t2452200970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Parameters::Update()
extern "C"  void Parameters_Update_m4005586780 (Parameters_t2452200970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Parameters::goToNextScene()
extern "C"  void Parameters_goToNextScene_m1511013029 (Parameters_t2452200970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
