﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainButtons
struct MainButtons_t1024182120;
// UnityEngine.UI.Slider
struct Slider_t1468074762;
// UnityEngine.UI.Toggle
struct Toggle_t1499417981;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider1468074762.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle1499417981.h"

// System.Void MainButtons::.ctor()
extern "C"  void MainButtons__ctor_m3880093955 (MainButtons_t1024182120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainButtons::Start()
extern "C"  void MainButtons_Start_m2827231747 (MainButtons_t1024182120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainButtons::OnPlay()
extern "C"  void MainButtons_OnPlay_m1840751828 (MainButtons_t1024182120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainButtons::OnShare()
extern "C"  void MainButtons_OnShare_m3776576353 (MainButtons_t1024182120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainButtons::onSettings()
extern "C"  void MainButtons_onSettings_m2990676579 (MainButtons_t1024182120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainButtons::onBack()
extern "C"  void MainButtons_onBack_m1365186983 (MainButtons_t1024182120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainButtons::AdjustSpeed(UnityEngine.UI.Slider)
extern "C"  void MainButtons_AdjustSpeed_m993147477 (MainButtons_t1024182120 * __this, Slider_t1468074762 * ___slider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainButtons::toggleSound(UnityEngine.UI.Toggle)
extern "C"  void MainButtons_toggleSound_m1727625887 (MainButtons_t1024182120 * __this, Toggle_t1499417981 * ___toggleSounds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
