﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Parameters
struct Parameters_t2452200970;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoldCoinMovement
struct  GoldCoinMovement_t1915910912  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Vector3 GoldCoinMovement::_startingPos
	Vector3_t3525329789  ____startingPos_2;
	// UnityEngine.Transform GoldCoinMovement::_trans
	Transform_t284553113 * ____trans_3;
	// System.Single GoldCoinMovement::moveSpeed
	float ___moveSpeed_4;
	// UnityEngine.GameObject GoldCoinMovement::go
	GameObject_t4012695102 * ___go_5;
	// Parameters GoldCoinMovement::parameters
	Parameters_t2452200970 * ___parameters_6;
	// UnityEngine.GameObject GoldCoinMovement::sparkles
	GameObject_t4012695102 * ___sparkles_7;

public:
	inline static int32_t get_offset_of__startingPos_2() { return static_cast<int32_t>(offsetof(GoldCoinMovement_t1915910912, ____startingPos_2)); }
	inline Vector3_t3525329789  get__startingPos_2() const { return ____startingPos_2; }
	inline Vector3_t3525329789 * get_address_of__startingPos_2() { return &____startingPos_2; }
	inline void set__startingPos_2(Vector3_t3525329789  value)
	{
		____startingPos_2 = value;
	}

	inline static int32_t get_offset_of__trans_3() { return static_cast<int32_t>(offsetof(GoldCoinMovement_t1915910912, ____trans_3)); }
	inline Transform_t284553113 * get__trans_3() const { return ____trans_3; }
	inline Transform_t284553113 ** get_address_of__trans_3() { return &____trans_3; }
	inline void set__trans_3(Transform_t284553113 * value)
	{
		____trans_3 = value;
		Il2CppCodeGenWriteBarrier(&____trans_3, value);
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(GoldCoinMovement_t1915910912, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_go_5() { return static_cast<int32_t>(offsetof(GoldCoinMovement_t1915910912, ___go_5)); }
	inline GameObject_t4012695102 * get_go_5() const { return ___go_5; }
	inline GameObject_t4012695102 ** get_address_of_go_5() { return &___go_5; }
	inline void set_go_5(GameObject_t4012695102 * value)
	{
		___go_5 = value;
		Il2CppCodeGenWriteBarrier(&___go_5, value);
	}

	inline static int32_t get_offset_of_parameters_6() { return static_cast<int32_t>(offsetof(GoldCoinMovement_t1915910912, ___parameters_6)); }
	inline Parameters_t2452200970 * get_parameters_6() const { return ___parameters_6; }
	inline Parameters_t2452200970 ** get_address_of_parameters_6() { return &___parameters_6; }
	inline void set_parameters_6(Parameters_t2452200970 * value)
	{
		___parameters_6 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_6, value);
	}

	inline static int32_t get_offset_of_sparkles_7() { return static_cast<int32_t>(offsetof(GoldCoinMovement_t1915910912, ___sparkles_7)); }
	inline GameObject_t4012695102 * get_sparkles_7() const { return ___sparkles_7; }
	inline GameObject_t4012695102 ** get_address_of_sparkles_7() { return &___sparkles_7; }
	inline void set_sparkles_7(GameObject_t4012695102 * value)
	{
		___sparkles_7 = value;
		Il2CppCodeGenWriteBarrier(&___sparkles_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
