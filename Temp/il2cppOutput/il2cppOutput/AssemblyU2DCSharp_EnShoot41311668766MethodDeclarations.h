﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnShoot4
struct EnShoot4_t1311668766;

#include "codegen/il2cpp-codegen.h"

// System.Void EnShoot4::.ctor()
extern "C"  void EnShoot4__ctor_m1855374909 (EnShoot4_t1311668766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot4::Start()
extern "C"  void EnShoot4_Start_m802512701 (EnShoot4_t1311668766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot4::Update()
extern "C"  void EnShoot4_Update_m3408909424 (EnShoot4_t1311668766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnShoot4::Shoot()
extern "C"  void EnShoot4_Shoot_m471802810 (EnShoot4_t1311668766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
