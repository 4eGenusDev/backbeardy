﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PetsFollow
struct PetsFollow_t690634789;

#include "codegen/il2cpp-codegen.h"

// System.Void PetsFollow::.ctor()
extern "C"  void PetsFollow__ctor_m3435573782 (PetsFollow_t690634789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PetsFollow::Start()
extern "C"  void PetsFollow_Start_m2382711574 (PetsFollow_t690634789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PetsFollow::Update()
extern "C"  void PetsFollow_Update_m855466935 (PetsFollow_t690634789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
