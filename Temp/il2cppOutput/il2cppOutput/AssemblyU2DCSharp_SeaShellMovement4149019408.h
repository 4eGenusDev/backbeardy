﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t594472611;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeaShellMovement
struct  SeaShellMovement_t4149019408  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform SeaShellMovement::_trans
	Transform_t284553113 * ____trans_2;
	// UnityEngine.Vector3 SeaShellMovement::_StartPos
	Vector3_t3525329789  ____StartPos_3;
	// System.Single SeaShellMovement::moveSpeed
	float ___moveSpeed_4;
	// UnityEngine.SpriteRenderer SeaShellMovement::spriteName
	SpriteRenderer_t2223784725 * ___spriteName_5;
	// UnityEngine.CircleCollider2D SeaShellMovement::collider
	CircleCollider2D_t594472611 * ___collider_6;

public:
	inline static int32_t get_offset_of__trans_2() { return static_cast<int32_t>(offsetof(SeaShellMovement_t4149019408, ____trans_2)); }
	inline Transform_t284553113 * get__trans_2() const { return ____trans_2; }
	inline Transform_t284553113 ** get_address_of__trans_2() { return &____trans_2; }
	inline void set__trans_2(Transform_t284553113 * value)
	{
		____trans_2 = value;
		Il2CppCodeGenWriteBarrier(&____trans_2, value);
	}

	inline static int32_t get_offset_of__StartPos_3() { return static_cast<int32_t>(offsetof(SeaShellMovement_t4149019408, ____StartPos_3)); }
	inline Vector3_t3525329789  get__StartPos_3() const { return ____StartPos_3; }
	inline Vector3_t3525329789 * get_address_of__StartPos_3() { return &____StartPos_3; }
	inline void set__StartPos_3(Vector3_t3525329789  value)
	{
		____StartPos_3 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(SeaShellMovement_t4149019408, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_spriteName_5() { return static_cast<int32_t>(offsetof(SeaShellMovement_t4149019408, ___spriteName_5)); }
	inline SpriteRenderer_t2223784725 * get_spriteName_5() const { return ___spriteName_5; }
	inline SpriteRenderer_t2223784725 ** get_address_of_spriteName_5() { return &___spriteName_5; }
	inline void set_spriteName_5(SpriteRenderer_t2223784725 * value)
	{
		___spriteName_5 = value;
		Il2CppCodeGenWriteBarrier(&___spriteName_5, value);
	}

	inline static int32_t get_offset_of_collider_6() { return static_cast<int32_t>(offsetof(SeaShellMovement_t4149019408, ___collider_6)); }
	inline CircleCollider2D_t594472611 * get_collider_6() const { return ___collider_6; }
	inline CircleCollider2D_t594472611 ** get_address_of_collider_6() { return &___collider_6; }
	inline void set_collider_6(CircleCollider2D_t594472611 * value)
	{
		___collider_6 = value;
		Il2CppCodeGenWriteBarrier(&___collider_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
