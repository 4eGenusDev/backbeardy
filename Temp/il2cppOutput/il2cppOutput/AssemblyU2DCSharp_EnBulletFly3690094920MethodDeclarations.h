﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnBulletFly
struct EnBulletFly_t3690094920;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void EnBulletFly::.ctor()
extern "C"  void EnBulletFly__ctor_m1342049059 (EnBulletFly_t3690094920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly::Start()
extern "C"  void EnBulletFly_Start_m289186851 (EnBulletFly_t3690094920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly::Update()
extern "C"  void EnBulletFly_Update_m380709962 (EnBulletFly_t3690094920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnBulletFly::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void EnBulletFly_OnTriggerEnter2D_m1586250933 (EnBulletFly_t3690094920 * __this, Collider2D_t1890038195 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
