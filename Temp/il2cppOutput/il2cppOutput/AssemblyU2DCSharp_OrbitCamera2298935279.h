﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OrbitCamera
struct  OrbitCamera_t2298935279  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform OrbitCamera::TargetLookAt
	Transform_t284553113 * ___TargetLookAt_2;
	// System.Single OrbitCamera::Distance
	float ___Distance_3;
	// System.Single OrbitCamera::DistanceMin
	float ___DistanceMin_4;
	// System.Single OrbitCamera::DistanceMax
	float ___DistanceMax_5;
	// System.Single OrbitCamera::startingDistance
	float ___startingDistance_6;
	// System.Single OrbitCamera::desiredDistance
	float ___desiredDistance_7;
	// System.Single OrbitCamera::mouseX
	float ___mouseX_8;
	// System.Single OrbitCamera::mouseY
	float ___mouseY_9;
	// System.Single OrbitCamera::X_MouseSensitivity
	float ___X_MouseSensitivity_10;
	// System.Single OrbitCamera::Y_MouseSensitivity
	float ___Y_MouseSensitivity_11;
	// System.Single OrbitCamera::MouseWheelSensitivity
	float ___MouseWheelSensitivity_12;
	// System.Single OrbitCamera::Y_MinLimit
	float ___Y_MinLimit_13;
	// System.Single OrbitCamera::Y_MaxLimit
	float ___Y_MaxLimit_14;
	// System.Single OrbitCamera::DistanceSmooth
	float ___DistanceSmooth_15;
	// System.Single OrbitCamera::velocityDistance
	float ___velocityDistance_16;
	// UnityEngine.Vector3 OrbitCamera::desiredPosition
	Vector3_t3525329789  ___desiredPosition_17;
	// System.Single OrbitCamera::X_Smooth
	float ___X_Smooth_18;
	// System.Single OrbitCamera::Y_Smooth
	float ___Y_Smooth_19;
	// System.Single OrbitCamera::velX
	float ___velX_20;
	// System.Single OrbitCamera::velY
	float ___velY_21;
	// System.Single OrbitCamera::velZ
	float ___velZ_22;
	// UnityEngine.Vector3 OrbitCamera::position
	Vector3_t3525329789  ___position_23;

public:
	inline static int32_t get_offset_of_TargetLookAt_2() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___TargetLookAt_2)); }
	inline Transform_t284553113 * get_TargetLookAt_2() const { return ___TargetLookAt_2; }
	inline Transform_t284553113 ** get_address_of_TargetLookAt_2() { return &___TargetLookAt_2; }
	inline void set_TargetLookAt_2(Transform_t284553113 * value)
	{
		___TargetLookAt_2 = value;
		Il2CppCodeGenWriteBarrier(&___TargetLookAt_2, value);
	}

	inline static int32_t get_offset_of_Distance_3() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___Distance_3)); }
	inline float get_Distance_3() const { return ___Distance_3; }
	inline float* get_address_of_Distance_3() { return &___Distance_3; }
	inline void set_Distance_3(float value)
	{
		___Distance_3 = value;
	}

	inline static int32_t get_offset_of_DistanceMin_4() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___DistanceMin_4)); }
	inline float get_DistanceMin_4() const { return ___DistanceMin_4; }
	inline float* get_address_of_DistanceMin_4() { return &___DistanceMin_4; }
	inline void set_DistanceMin_4(float value)
	{
		___DistanceMin_4 = value;
	}

	inline static int32_t get_offset_of_DistanceMax_5() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___DistanceMax_5)); }
	inline float get_DistanceMax_5() const { return ___DistanceMax_5; }
	inline float* get_address_of_DistanceMax_5() { return &___DistanceMax_5; }
	inline void set_DistanceMax_5(float value)
	{
		___DistanceMax_5 = value;
	}

	inline static int32_t get_offset_of_startingDistance_6() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___startingDistance_6)); }
	inline float get_startingDistance_6() const { return ___startingDistance_6; }
	inline float* get_address_of_startingDistance_6() { return &___startingDistance_6; }
	inline void set_startingDistance_6(float value)
	{
		___startingDistance_6 = value;
	}

	inline static int32_t get_offset_of_desiredDistance_7() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___desiredDistance_7)); }
	inline float get_desiredDistance_7() const { return ___desiredDistance_7; }
	inline float* get_address_of_desiredDistance_7() { return &___desiredDistance_7; }
	inline void set_desiredDistance_7(float value)
	{
		___desiredDistance_7 = value;
	}

	inline static int32_t get_offset_of_mouseX_8() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___mouseX_8)); }
	inline float get_mouseX_8() const { return ___mouseX_8; }
	inline float* get_address_of_mouseX_8() { return &___mouseX_8; }
	inline void set_mouseX_8(float value)
	{
		___mouseX_8 = value;
	}

	inline static int32_t get_offset_of_mouseY_9() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___mouseY_9)); }
	inline float get_mouseY_9() const { return ___mouseY_9; }
	inline float* get_address_of_mouseY_9() { return &___mouseY_9; }
	inline void set_mouseY_9(float value)
	{
		___mouseY_9 = value;
	}

	inline static int32_t get_offset_of_X_MouseSensitivity_10() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___X_MouseSensitivity_10)); }
	inline float get_X_MouseSensitivity_10() const { return ___X_MouseSensitivity_10; }
	inline float* get_address_of_X_MouseSensitivity_10() { return &___X_MouseSensitivity_10; }
	inline void set_X_MouseSensitivity_10(float value)
	{
		___X_MouseSensitivity_10 = value;
	}

	inline static int32_t get_offset_of_Y_MouseSensitivity_11() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___Y_MouseSensitivity_11)); }
	inline float get_Y_MouseSensitivity_11() const { return ___Y_MouseSensitivity_11; }
	inline float* get_address_of_Y_MouseSensitivity_11() { return &___Y_MouseSensitivity_11; }
	inline void set_Y_MouseSensitivity_11(float value)
	{
		___Y_MouseSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_MouseWheelSensitivity_12() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___MouseWheelSensitivity_12)); }
	inline float get_MouseWheelSensitivity_12() const { return ___MouseWheelSensitivity_12; }
	inline float* get_address_of_MouseWheelSensitivity_12() { return &___MouseWheelSensitivity_12; }
	inline void set_MouseWheelSensitivity_12(float value)
	{
		___MouseWheelSensitivity_12 = value;
	}

	inline static int32_t get_offset_of_Y_MinLimit_13() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___Y_MinLimit_13)); }
	inline float get_Y_MinLimit_13() const { return ___Y_MinLimit_13; }
	inline float* get_address_of_Y_MinLimit_13() { return &___Y_MinLimit_13; }
	inline void set_Y_MinLimit_13(float value)
	{
		___Y_MinLimit_13 = value;
	}

	inline static int32_t get_offset_of_Y_MaxLimit_14() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___Y_MaxLimit_14)); }
	inline float get_Y_MaxLimit_14() const { return ___Y_MaxLimit_14; }
	inline float* get_address_of_Y_MaxLimit_14() { return &___Y_MaxLimit_14; }
	inline void set_Y_MaxLimit_14(float value)
	{
		___Y_MaxLimit_14 = value;
	}

	inline static int32_t get_offset_of_DistanceSmooth_15() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___DistanceSmooth_15)); }
	inline float get_DistanceSmooth_15() const { return ___DistanceSmooth_15; }
	inline float* get_address_of_DistanceSmooth_15() { return &___DistanceSmooth_15; }
	inline void set_DistanceSmooth_15(float value)
	{
		___DistanceSmooth_15 = value;
	}

	inline static int32_t get_offset_of_velocityDistance_16() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___velocityDistance_16)); }
	inline float get_velocityDistance_16() const { return ___velocityDistance_16; }
	inline float* get_address_of_velocityDistance_16() { return &___velocityDistance_16; }
	inline void set_velocityDistance_16(float value)
	{
		___velocityDistance_16 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_17() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___desiredPosition_17)); }
	inline Vector3_t3525329789  get_desiredPosition_17() const { return ___desiredPosition_17; }
	inline Vector3_t3525329789 * get_address_of_desiredPosition_17() { return &___desiredPosition_17; }
	inline void set_desiredPosition_17(Vector3_t3525329789  value)
	{
		___desiredPosition_17 = value;
	}

	inline static int32_t get_offset_of_X_Smooth_18() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___X_Smooth_18)); }
	inline float get_X_Smooth_18() const { return ___X_Smooth_18; }
	inline float* get_address_of_X_Smooth_18() { return &___X_Smooth_18; }
	inline void set_X_Smooth_18(float value)
	{
		___X_Smooth_18 = value;
	}

	inline static int32_t get_offset_of_Y_Smooth_19() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___Y_Smooth_19)); }
	inline float get_Y_Smooth_19() const { return ___Y_Smooth_19; }
	inline float* get_address_of_Y_Smooth_19() { return &___Y_Smooth_19; }
	inline void set_Y_Smooth_19(float value)
	{
		___Y_Smooth_19 = value;
	}

	inline static int32_t get_offset_of_velX_20() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___velX_20)); }
	inline float get_velX_20() const { return ___velX_20; }
	inline float* get_address_of_velX_20() { return &___velX_20; }
	inline void set_velX_20(float value)
	{
		___velX_20 = value;
	}

	inline static int32_t get_offset_of_velY_21() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___velY_21)); }
	inline float get_velY_21() const { return ___velY_21; }
	inline float* get_address_of_velY_21() { return &___velY_21; }
	inline void set_velY_21(float value)
	{
		___velY_21 = value;
	}

	inline static int32_t get_offset_of_velZ_22() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___velZ_22)); }
	inline float get_velZ_22() const { return ___velZ_22; }
	inline float* get_address_of_velZ_22() { return &___velZ_22; }
	inline void set_velZ_22(float value)
	{
		___velZ_22 = value;
	}

	inline static int32_t get_offset_of_position_23() { return static_cast<int32_t>(offsetof(OrbitCamera_t2298935279, ___position_23)); }
	inline Vector3_t3525329789  get_position_23() const { return ___position_23; }
	inline Vector3_t3525329789 * get_address_of_position_23() { return &___position_23; }
	inline void set_position_23(Vector3_t3525329789  value)
	{
		___position_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
