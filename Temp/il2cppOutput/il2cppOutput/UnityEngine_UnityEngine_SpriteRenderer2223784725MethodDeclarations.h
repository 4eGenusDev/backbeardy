﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern "C"  Sprite_t4006040370 * SpriteRenderer_get_sprite_m3481747968 (SpriteRenderer_t2223784725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.SpriteRenderer::GetSprite_INTERNAL()
extern "C"  Sprite_t4006040370 * SpriteRenderer_GetSprite_INTERNAL_m2175346003 (SpriteRenderer_t2223784725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C"  void SpriteRenderer_set_color_m2701854973 (SpriteRenderer_t2223784725 * __this, Color_t1588175760  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void SpriteRenderer_INTERNAL_set_color_m1427138729 (SpriteRenderer_t2223784725 * __this, Color_t1588175760 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_flipX(System.Boolean)
extern "C"  void SpriteRenderer_set_flipX_m4285205544 (SpriteRenderer_t2223784725 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
