﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OrbitCamera
struct OrbitCamera_t2298935279;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void OrbitCamera::.ctor()
extern "C"  void OrbitCamera__ctor_m2107310172 (OrbitCamera_t2298935279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::Start()
extern "C"  void OrbitCamera_Start_m1054447964 (OrbitCamera_t2298935279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::Update()
extern "C"  void OrbitCamera_Update_m2628967985 (OrbitCamera_t2298935279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::LateUpdate()
extern "C"  void OrbitCamera_LateUpdate_m3776455543 (OrbitCamera_t2298935279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::HandlePlayerInput()
extern "C"  void OrbitCamera_HandlePlayerInput_m790181339 (OrbitCamera_t2298935279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::CalculateDesiredPosition()
extern "C"  void OrbitCamera_CalculateDesiredPosition_m641278533 (OrbitCamera_t2298935279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
extern "C"  Vector3_t3525329789  OrbitCamera_CalculatePosition_m2715746098 (OrbitCamera_t2298935279 * __this, float ___rotationX, float ___rotationY, float ___distance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::UpdatePosition()
extern "C"  void OrbitCamera_UpdatePosition_m1656847674 (OrbitCamera_t2298935279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::Reset()
extern "C"  void OrbitCamera_Reset_m4048710409 (OrbitCamera_t2298935279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float OrbitCamera_ClampAngle_m1631653633 (OrbitCamera_t2298935279 * __this, float ___angle, float ___min, float ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
