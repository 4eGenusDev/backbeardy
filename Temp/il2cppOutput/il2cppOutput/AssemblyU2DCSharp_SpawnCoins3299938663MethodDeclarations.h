﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpawnCoins
struct SpawnCoins_t3299938663;

#include "codegen/il2cpp-codegen.h"

// System.Void SpawnCoins::.ctor()
extern "C"  void SpawnCoins__ctor_m3091868436 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::Start()
extern "C"  void SpawnCoins_Start_m2039006228 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::Update()
extern "C"  void SpawnCoins_Update_m3085503097 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::SpawnThem()
extern "C"  void SpawnCoins_SpawnThem_m2285362729 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::SpawnPowerup()
extern "C"  void SpawnCoins_SpawnPowerup_m305981909 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::SpawnShockwave()
extern "C"  void SpawnCoins_SpawnShockwave_m2782856272 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::SpawnFlurry()
extern "C"  void SpawnCoins_SpawnFlurry_m2595767543 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::SpawnManiac()
extern "C"  void SpawnCoins_SpawnManiac_m528688766 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::SpawnBottle()
extern "C"  void SpawnCoins_SpawnBottle_m2555024467 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::SpawnShield()
extern "C"  void SpawnCoins_SpawnShield_m4166663478 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnCoins::SpawnChrono()
extern "C"  void SpawnCoins_SpawnChrono_m2318469616 (SpawnCoins_t3299938663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
