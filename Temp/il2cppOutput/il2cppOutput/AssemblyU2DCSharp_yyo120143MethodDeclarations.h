﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// yyo
struct yyo_t120143;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void yyo::.ctor()
extern "C"  void yyo__ctor_m3943208700 (yyo_t120143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void yyo::Start()
extern "C"  void yyo_Start_m2890346492 (yyo_t120143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void yyo::Update()
extern "C"  void yyo_Update_m3707247505 (yyo_t120143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void yyo::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void yyo_OnTriggerEnter2D_m3693893628 (yyo_t120143 * __this, Collider2D_t1890038195 * ___trigger, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void yyo::MoveToSight()
extern "C"  void yyo_MoveToSight_m834429931 (yyo_t120143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void yyo::MoveOutOfSight()
extern "C"  void yyo_MoveOutOfSight_m3383009617 (yyo_t120143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
