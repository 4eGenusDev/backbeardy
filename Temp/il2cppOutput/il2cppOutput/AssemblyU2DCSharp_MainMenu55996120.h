﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3499186955;
// UnityEngine.CanvasRenderer[]
struct CanvasRendererU5BU5D_t2901501369;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu
struct  MainMenu_t55996120  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.Image MainMenu::meshR
	Image_t3354615620 * ___meshR_2;
	// System.Single MainMenu::lerp
	float ___lerp_3;
	// System.Boolean MainMenu::isLerped
	bool ___isLerped_4;
	// UnityEngine.GameObject[] MainMenu::ButtonsFade
	GameObjectU5BU5D_t3499186955* ___ButtonsFade_5;
	// UnityEngine.CanvasRenderer[] MainMenu::allRend
	CanvasRendererU5BU5D_t2901501369* ___allRend_6;

public:
	inline static int32_t get_offset_of_meshR_2() { return static_cast<int32_t>(offsetof(MainMenu_t55996120, ___meshR_2)); }
	inline Image_t3354615620 * get_meshR_2() const { return ___meshR_2; }
	inline Image_t3354615620 ** get_address_of_meshR_2() { return &___meshR_2; }
	inline void set_meshR_2(Image_t3354615620 * value)
	{
		___meshR_2 = value;
		Il2CppCodeGenWriteBarrier(&___meshR_2, value);
	}

	inline static int32_t get_offset_of_lerp_3() { return static_cast<int32_t>(offsetof(MainMenu_t55996120, ___lerp_3)); }
	inline float get_lerp_3() const { return ___lerp_3; }
	inline float* get_address_of_lerp_3() { return &___lerp_3; }
	inline void set_lerp_3(float value)
	{
		___lerp_3 = value;
	}

	inline static int32_t get_offset_of_isLerped_4() { return static_cast<int32_t>(offsetof(MainMenu_t55996120, ___isLerped_4)); }
	inline bool get_isLerped_4() const { return ___isLerped_4; }
	inline bool* get_address_of_isLerped_4() { return &___isLerped_4; }
	inline void set_isLerped_4(bool value)
	{
		___isLerped_4 = value;
	}

	inline static int32_t get_offset_of_ButtonsFade_5() { return static_cast<int32_t>(offsetof(MainMenu_t55996120, ___ButtonsFade_5)); }
	inline GameObjectU5BU5D_t3499186955* get_ButtonsFade_5() const { return ___ButtonsFade_5; }
	inline GameObjectU5BU5D_t3499186955** get_address_of_ButtonsFade_5() { return &___ButtonsFade_5; }
	inline void set_ButtonsFade_5(GameObjectU5BU5D_t3499186955* value)
	{
		___ButtonsFade_5 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonsFade_5, value);
	}

	inline static int32_t get_offset_of_allRend_6() { return static_cast<int32_t>(offsetof(MainMenu_t55996120, ___allRend_6)); }
	inline CanvasRendererU5BU5D_t2901501369* get_allRend_6() const { return ___allRend_6; }
	inline CanvasRendererU5BU5D_t2901501369** get_address_of_allRend_6() { return &___allRend_6; }
	inline void set_allRend_6(CanvasRendererU5BU5D_t2901501369* value)
	{
		___allRend_6 = value;
		Il2CppCodeGenWriteBarrier(&___allRend_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
