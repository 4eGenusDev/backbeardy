﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeepWithinBounds
struct  KeepWithinBounds_t1755115333  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Vector3 KeepWithinBounds::bottomLeft
	Vector3_t3525329789  ___bottomLeft_2;
	// UnityEngine.Vector3 KeepWithinBounds::topRight
	Vector3_t3525329789  ___topRight_3;

public:
	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(KeepWithinBounds_t1755115333, ___bottomLeft_2)); }
	inline Vector3_t3525329789  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Vector3_t3525329789 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Vector3_t3525329789  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_topRight_3() { return static_cast<int32_t>(offsetof(KeepWithinBounds_t1755115333, ___topRight_3)); }
	inline Vector3_t3525329789  get_topRight_3() const { return ___topRight_3; }
	inline Vector3_t3525329789 * get_address_of_topRight_3() { return &___topRight_3; }
	inline void set_topRight_3(Vector3_t3525329789  value)
	{
		___topRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
