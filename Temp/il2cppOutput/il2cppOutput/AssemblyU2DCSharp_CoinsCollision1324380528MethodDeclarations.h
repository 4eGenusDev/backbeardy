﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoinsCollision
struct CoinsCollision_t1324380528;

#include "codegen/il2cpp-codegen.h"

// System.Void CoinsCollision::.ctor()
extern "C"  void CoinsCollision__ctor_m808843563 (CoinsCollision_t1324380528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoinsCollision::Start()
extern "C"  void CoinsCollision_Start_m4050948651 (CoinsCollision_t1324380528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
