﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestParticles
struct TestParticles_t3963318811;

#include "codegen/il2cpp-codegen.h"

// System.Void TestParticles::.ctor()
extern "C"  void TestParticles__ctor_m83762608 (TestParticles_t3963318811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::Start()
extern "C"  void TestParticles_Start_m3325867696 (TestParticles_t3963318811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::Update()
extern "C"  void TestParticles_Update_m28535645 (TestParticles_t3963318811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::OnGUI()
extern "C"  void TestParticles_OnGUI_m3874128554 (TestParticles_t3963318811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::ShowParticle()
extern "C"  void TestParticles_ShowParticle_m2655492727 (TestParticles_t3963318811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
extern "C"  void TestParticles_ParticleInformationWindow_m3377206901 (TestParticles_t3963318811 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::InfoWindow(System.Int32)
extern "C"  void TestParticles_InfoWindow_m1509106787 (TestParticles_t3963318811 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
