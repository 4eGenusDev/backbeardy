﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyMovement2
struct  EnemyMovement2_t4196477659  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform EnemyMovement2::_trans
	Transform_t284553113 * ____trans_2;
	// UnityEngine.Vector3 EnemyMovement2::_StartPos
	Vector3_t3525329789  ____StartPos_3;
	// System.Single EnemyMovement2::moveSpeed
	float ___moveSpeed_4;

public:
	inline static int32_t get_offset_of__trans_2() { return static_cast<int32_t>(offsetof(EnemyMovement2_t4196477659, ____trans_2)); }
	inline Transform_t284553113 * get__trans_2() const { return ____trans_2; }
	inline Transform_t284553113 ** get_address_of__trans_2() { return &____trans_2; }
	inline void set__trans_2(Transform_t284553113 * value)
	{
		____trans_2 = value;
		Il2CppCodeGenWriteBarrier(&____trans_2, value);
	}

	inline static int32_t get_offset_of__StartPos_3() { return static_cast<int32_t>(offsetof(EnemyMovement2_t4196477659, ____StartPos_3)); }
	inline Vector3_t3525329789  get__StartPos_3() const { return ____StartPos_3; }
	inline Vector3_t3525329789 * get_address_of__StartPos_3() { return &____StartPos_3; }
	inline void set__StartPos_3(Vector3_t3525329789  value)
	{
		____StartPos_3 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(EnemyMovement2_t4196477659, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
